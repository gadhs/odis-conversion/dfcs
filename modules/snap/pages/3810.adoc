= 3810 Issuance
:chapter-number: 3800
:effective-date: January 2023
:mt: MT-70
:policy-number: 3810
:policy-title: Issuance
:previous-policy-number: MT-69

include::ROOT:partial$policy-header.adoc[]

== Requirements

Food Stamp (SNAP) benefits are issued in the form of an electronic funds transfer (EFT) using an electronic benefits transfer (EBT) card and personal identification number (PIN).

== Basic Considerations

Food Stamp (SNAP) benefits are issued as follows:

* initial benefits
* ongoing monthly benefits
* restorations
* supplemental benefits
* replacements for the loss of destroyed food
* manually issued benefits (Refer to Section 3805)

Food Stamp (SNAP) benefits [.underline]#may# be used to purchase any food or food product intended for human consumption such as:

* breads and cereals
* fruits and vegetables
* meats, fish and poultry
* dairy products
* seeds and plants, which produce food for the AU to eat
* meals prepared and delivered by an authorized meal delivery to AUs eligible for delivered meals
* meals served by an authorized communal dining facility for elderly AUs and SSI AUs
* meals prepared and served by a drug or alcohol treatment facility for AUs receiving treatment
* meals prepared and served by a group living arrangement for its residents
* meals prepared and served by a shelter for battered women and children to its residents

Food Stamp (SNAP) benefits [.underline]#*may not*# be used to purchase:

* beer and wine
* liquor
* cigarettes or tobacco
* pet foods
* soaps, paper products
* household supplies

Food Stamp (SNAP) benefits [.underline]#*may not*# be:

* sold, traded or given away
* used to allow retailers to buy benefits in exchange for cash
* used to pay credit accounts

=== Initial Month Issuance

Approving the application authorizes the initial month's benefits.
Gateway will do the following:

* authorize the initial benefits in the nightly processing cycle in which the case is brought to final

* post benefits to the EBT account after processing

* include all eligible benefit amounts, prorated from the date of application, and for any intervening months, in the issuance in the month of finalization.

=== Prorating Initial Month's Benefits

Prorated initial benefits are:

* rounded down to the next lower even dollar
* not issued if the amount is less than $23
+
NOTE: Eligible one and two person AUs are not issued the minimum benefit amount for the initial month of application when:

* The AU's normal ongoing benefit amount is less than the minimum benefit, but the AU will receive a monthly ongoing benefit of the minimum amount.

To determine the amount of benefits to issue for the initial application month, use the chart and follow the procedures below:

* Find the application date in Column I
* Identify the appropriate multiplication factor in Column II
* Apply the appropriate multiplication factor to the AU's full monthly allotment, rounding the amount down to the nearest whole dollar
* The amount is the prorated amount for the initial month of application

NOTE: The AU's full monthly allotment is determined by the system or manually after determining the AU's net income and comparing it to the Basis of Issuance Tables (BOI) for AU sizes 1-10 or BOI Tables for AU sizes 11-20.

.CHART 3810-1 PRORATING THE INITIAL MONTH'S BENEFITS
|===
| Column I  Date of Application | Column II  Multiplication Factor

^| 1
^| 1.000

^| 2
^| .9667

^| 3
^| .9334

^| 4
^| .9000

^| 5
^| .8667

^| 6
^| .8334

^| 7
^| .8000

^| 8
^| .7667

^| 9
^| .7334

^| 10
^| .7000

^| 11
^| .6667

^| 12
^| .6334

^| 13
^| .6000

^| 14
^| .5667

^| 15
^| .5334

^| 16
^| .5000

^| 17
^| .4667

^| 18
^| .4334

^| 19
^| .4000

^| 20
^| .3667

^| 21
^| .3334

^| 22
^| .3000

^| 23
^| .2667

^| 24
^| .2334

^| 25
^| .2000

^| 26
^| .1667

^| 27
^| .1334

^| 28
^| .1000

^| 29
^| .0667

^| 30
^| .0334

^| 31
^| .0334
|===

=== Regular Monthly Benefits

AUs with one or two members who are eligible for an allotment of less than the minimum benefit amount will receive the minimum benefit amount.

In the case where the AU's net income is less than the maximum, but the AU is eligible for zero benefits, or eligible for less than the minimum allotment, the Gateway system will automatically close the case.

Ongoing monthly FS benefits are posted to EBT accounts according to the last two digits of the head of household's client ID number.

The benefit schedule is as follows:

|===
| Client ID #s End in | Benefits Available on

^| 00-09
^| 5^th^ of the month

^| 10-19
^| 7^th^ of the month

^| 20-29
^| 9^th^ of the month

^| 30-39
^| 11^th^ of the month

^| 40-49
^| 13^th^ of the month

^| 50-59
^| 15^th^ of the month

^| 60-69
^| 17^th^ of the month

^| 70-79
^| 19^th^ of the month

^| 80-89
^| 21^st^ of the month

^| 90-99
^| 23^rd^ of the month
|===

=== Restoration

Authorize a restoration to correct an under issuance in the following situations:

* agency errors in benefit calculations
* reversal of an IPV disqualification
* specific policy requiring restored benefits
* restoration ordered by an administrative law judge

Benefits will be restored for not more than twelve months prior to whichever of the following occurred first:

* The date the State agency receives a request for restoration from a household

or

* The date the State agency is notified or otherwise discovers that a loss to a household has occurred.

Restore to households benefits which were found by any judicial action to have been wrongfully withheld.
If the judicial action is the first action the recipient has taken to +
obtain restoration of lost benefits, then benefits will be restored for a period of not more than twelve months from the date the court action was initiated.
When the judicial action is a review of an agency action, the benefits will be restored for a period of not more than twelve months from the first of the following dates:

* The date the agency receives a request for restoration:
* If no request for restoration is received, the date the fair hearing action was initiated;
but
* Never more than one year from when the agency is notified of, or discovers, the loss.

Benefits will be restored even if the household is currently ineligible. +
Restorations will be offset to pay on active unpaid claims *but* do not apply a restoration against an unpaid claim when:

* an initial allotment is issued
* benefits are reinstated
* benefits are replaced as a result of loss through a disaster
* initial benefits are expunged
* supplemental benefits are issued as a result of an expedited change (i.e., addition of an AU member or a decrease in gross monthly income of $50 or more)

Issue a notification form to inform the AU of all of the following:

* the amount of benefits restored
* the reason for the restoration
* the amount offset against the unpaid claim, if any
* the 90-day period allowed requesting a hearing to contest the amount or the denial of restored benefits.

If one AU becomes two or more AUs before a restoration is paid, issue benefits according to the following guidelines:

* to the AU with the majority of the members from the original AU
* if each current AU has an equal number of members from the original AU, to the AU which contains the head of the original AU
* if unable to locate the head of the original AU, prorate the benefits equally between the AUs containing the members of the original AU

=== Supplemental Benefits

Issue supplemental benefits for either of the following reasons:

* The addition of a new AU member causes an increase in the benefit amount.
* A decrease in income of $50.00 or more occurs in a prospectively budgeted AU.

If a reported change results in an increase in benefits, the change is made effective no later than the first allotment issued 10 days after the date the change was reported.

Supplemental allotments will be posted to EBT accounts after system processing.

=== Replacing Food Destroyed in a Disaster When Food Was Purchased with Food Stamp (SNAP) Benefits

Follow the steps below when an AU reports a loss of food destroyed in a disaster, which was purchased with FS benefits within 10 days of the occurrence:

. Verify that a disaster, such as a fire or flood, occurred.
*NOTE*: Replace benefits for loss of food purchased with FS benefits when a household experiences a power outage of 4 or more hours due to a disaster and it is reported within 10 days of the occurrence.

. Have the AU to sign a statement or file an affidavit attesting to the loss of food.
+
NOTE: The AU may mail the statement to the county office.

[start=3]
. Provide a replacement (as a claim underpayment in Gateway) within 10 days of the reported loss.

. Replace benefits in the amount of the loss, up to a maximum of one month's allotment.

. If restored benefits were part of the month's allotment, replace the amount of the restoration.

. There is no limit to the number of replacement benefits for food destroyed in a disaster.
