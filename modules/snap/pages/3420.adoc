= 3420 Income
:chapter-number: 3400
:effective-date: June 2023
:mt: MT-72
:policy-number: 3420
:policy-title: Income
:previous-policy-number: MT-68

include::ROOT:partial$policy-header.adoc[]

== Requirements

All money received from any source by the assistance unit (AU) is considered in determining eligibility and the benefit level.

== Basic Considerations

Money received is considered to be one of the following:

* Earned – money received from wages, salaries, or commissions in exchange for services rendered.

* Unearned – money received from any source other than those listed above.

Eligibility based on income is determined by resolving the following series of questions:

* What is the income limit for the AU?
* Whose income is considered?
* What is the source of the income?
* Is the income available to the AU to meet its needs?
* Is the income included or excluded?
* How often is the income received?

=== Income to be Considered

The countable income of the following individuals is considered when determining eligibility:

* eligible AU members
* disqualified individuals
* deemed income of sponsors of aliens
* ineligible aliens
* lawbreakers
* ineligible Able-Bodied Adults Without Dependents
* enumeration sanctioned individuals
* work sanctioned individuals

=== Income Limit Sources

The source of the income must be identified to determine if the income is included or excluded.
Refer to Chart 3420.2, Types of Income.

=== Income Limit

The countable income of the AU member is applied to the income limit(s) for the AU size.

[caption=Exception]
NOTE: Refer to Section 3210, Categorically Eligible Assistance Units.

=== Income Available to the AU

Income is included if it is accessible to the AU for daily use because the AU has the legal ability to use it.

=== Jointly Received Income

If an AU member receives income jointly with another person or a group of persons, the portion that belongs to the AU member is determined as follows:

* If there is an agreement between the parties that specifies how they will divide the income, this agreement is used to determine the amount of income to consider.

* If there is no agreement, a pro rata share of the income is counted to the member whose income is being considered.

=== Bankruptcy

Bankruptcy is a condition whereas a debtor, either voluntarily or invoked by a creditor, is judged legally insolvent, and the debtor's remaining debt(s) is administered and distributed to his/her creditors.

Money directed to pay creditors in a bankruptcy is not deducted from gross income in a FS budget, unless the income is otherwise exempt by policy.

=== Garnishment

Garnishment is a condition whereas a debtor has wages/monies withheld by an employer/entity to pay a debt owed to a third party.

Money directed to pay creditors via garnishment is not deducted from gross income in a FS budget, unless the income is otherwise exempt by policy.
Refer to Chart 3420.2 in this section.

=== Verification

Verification of income is obtained in the following order:

* The A/R should provide verification from the payment source.
If the A/R cannot obtain the verification, the agency must request it directly from the payment source.
* Verification can be obtained from a collateral source, a person who has knowledge of the income, if verification cannot be provided by the payment source.

The statement of the A/R may be accepted if all other attempts to verify income are unsuccessful and the A/R has cooperated with previous attempts to obtain verification.

=== Means of Verification

Verification of income can be provided in a variety of ways, including:

* The Work Number
* pay stubs
* award letter
* copy of check reflecting gross income
* written statement from payment source
* computer match
* Form 809 – Verification of Earned Income Form

=== Income from a Terminated Source at Application

Terminated income during the application or subsequent months should be verified.
The last date of employment, date and amount of final payment, if appropriate, and the reason for separation/termination, is verified if the income is terminated within 30 days of the application date or thereafter.
Income terminated prior to the application month may be verified if questionable.

If an eligibility determination cannot be made for the application or intervening months because of insufficient information or verification, but ongoing eligibility can be established, then deny the applicable month/(s) and approve the case ongoing.
Document all efforts and attempts made to verify eligibility for these months.

The following chart provides an alphabetic listing of the following:

* source or type of income
* whether the income is earned or unearned
* a description of the income
* whether the income is included (I) or excluded (E) in the Food Stamp budget.

.CHART 3420.2 – TYPES OF INCOME
|===
| SOURCE/TYPE | DESCRIPTION | FS

| ADOPTION ASSISTANCE

(TITLE IV-E AND STATE)
| UNEARNED-Payment received for the adoption of certain children.
^| I

| ADVANCE PAYMENT
| UNEARNED- A payment received for future expenses that does not represent a gain to the AU.

EARNED-A prepayment of wages or salaries.
| E

I

| AGENT ORANGE PAYMENT
| UNEARNED-A payment made to a Vietnam Veteran who was exposed to Agent Orange.
The payment is made to the surviving spouse and/or children of a deceased Vietnam Veteran who was exposed to agent orange.
^| E

| ALASKA NATIVE CLAIM
| UNEARNED-A payment made under the Alaska Native Claims Settlement Act.
^| E

| ALIMONY
| UNEARNED-A court-ordered payment from an estranged spouse or former spouse to the other spouse.
Refer to Vendor Payments in this chart.
^| I

| AMERICORPS
a|
Income from the AMERICORPS Network of programs, which encompasses AMERICORPS USA, AMERICORPS VISTA, and AMERICORPS NCCC. +
EARNED INCOME - Living Allowance Stipend

EARNED INCOME - On-The-Job Training

* Children under age 19
* Adults age 19 & up

_Childcare allowance_ - Payments are exempt to the extent that the funds are not a gain or benefit to the AU. +
_Basic health insurance policy_

_Auxiliary Aid & Service To Individuals With Disabilities_

_Educational Money_
^| E

E

I

E

E

E

E
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

| ANNUITY
| UNEARNED-A recurring payment received from an investment plan.
^| I

| BLACK LUNG BENEFITS
| UNEARNED-Benefits paid to miners and their survivors under the provisions of the Federal Mine Safety and Health Act.
^| I

| BLOOD (sale of)
| EARNED-Money received from the sale of blood.
^| I

| BOARDER INCOME
a|
EARNED-Direct payments for food and shelter, less the cost of doing business.

Refer to Section 3205, Food Stamp Assistance Units, for treatment of boarders.

NOTE: Refer to Section 3425, Self-Employment Income.
^| I

| BONUS
| EARNED-Wages paid in addition to the usual or expected wages.
Refer to _Wages_ in this chart.
^| I

| CAFETERIA PLANS
| EARNED-Credits made available to an employee to buy health insurance, life insurance, leave, spending accounts or other benefits.
Also known as a flexible benefits plan.
The employee cannot elect to receive cash for the credits.
^| E

| CAPITAL GAINS
| EARNED or UNEARNED-Profits gained from the sale of capital assets.

Capital assets are resources, such as real estate, stocks, securities, and equipment that are typically held for long periods of time.

A capital gain is realized when the asset sold has appreciated in value from the original purchase price.
Refer to Section 3425, Self-Employment Income.
^| I

.2+| CHARITABLE DONATIONS FROM PRIVATE, NON-PROFIT ORGANIZATIONS (NOT STATE OR FEDERALLY FUNDED)
| UNEARNED–Charitable donations paid to the AU that total $300 or less in a federal fiscal quarter.
^| E

| UNEARNED-Donations given to the AU exceeding

$300 in a federal fiscal quarter.
^| I
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

| CHARITABLE DONATIONS FROM FEDERALLY OR STATE FUNDED ORGANIZATIONS
| UNEARNED-Donations paid to the AU from organizations receiving state or federal funds.
For example: Salvation Army, United Way, Catholic Charities, and Lutheran Social Services Agencies.
^| I

| CHILDCARE ATTENDANT
| EARNED-Income received for providing childcare services.

If the attendant has a self-employment business and provides childcare services in his/her home or place of residence, treat the income as self-employment income.

If the attendant provides services in the home of the child and an employee/employer relationship exists, treat the income as wages.
Refer to Section 3425, Self-employment Income.
^| I

| CHILD NUTRITION PAYMENTS
a|
UNEARNED-The value of meals provided to a child in daycare through the Child Nutrition Amendment of 1978.

If the payment is for a child of the childcare provider, budget the entire amount as unearned income.

If the payment is for any other child, treat as self-employment income, using the following guidelines:

* If more than one child is included in the payment, allow each child a prorated share of the total payment.

* When determining the cost of doing business, do not exceed the food stamp allotment for one person for each child.
^| I

I

| CHILD NUTRITION SUBSIDY
| UNEARNED-Payments made under Title IV of the Social Security Act to a childcare provider on behalf of the AU.
These payments include Transitional Child Care, At-Risk block grants, and childcare payments made under Section 5801 of the Social Security Act.
^| E
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

| CHILD SUPPORT
a|
UNEARNED-Income received from an absent parent for the support of his/her children.

Payments are made to the AU as follows:

* from the probation office
* directly from the absent parent
* from the Division of Child Support Services (DCSS)

Refer to Military Allotment and Vendor Payments in this chart.
| I

I

I

I

| COMMISSION
| EARNED-A payment, usually a set fee or percentage, made to an employee for his/her service in facilitating a transaction such as buying or selling goods.
A commission may be paid in lieu of or in addition to a regular salary.
Refer to W_ages_ in this chart.

If the payment is reoccurring, include it when determining representative pay.
If not, do not include the pay.
Refer to Section 3605, Prospective Budgeting.
^| I

| CONTRACTED EMPLOYMENT INCOME
| EARNED-Income received from jobs in which there is a contract or payment agreement.
Determine the gross monthly amount by dividing the total amount during the life of the contract by the number of months specified in the contract.

Refer to Section 3605, Prospective Budgeting and Section 3640, Contract Employment Budgeting.
^| I

| CONTRIBUTION (CASH), GIFT, PRIZE, OR REWARD
| UNEARNED-Money given to the AU as a gift from individuals or organizations.
Refer to Charitable Donations in this chart.
^| I

| CRIME VICTIM COMPENSATION PROGRAM
| UNEARNED-Money paid through federal or federally funded state or local programs that cover costs incurred by victims of crimes.
^| E

| DEFERRED COMPENSATION PLAN
| UNEARNED-Money paid regularly from a deferred compensation plan.
The money is usually available upon the owner's employment retirement or the owner attains a certain age.
^| I
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

| DEEMED INCOME FROM AN ALIEN'S SPONSOR
| Refer to Section 3630, Budget for an AU with Sponsored Alien.
^| I

| DISABILITY PAYMENT
| UNEARNED-Money paid to an employee by an insurance company or a source other than the employer.
Refer to Sick Pay in this chart.
^| I

.3+| DISASTER RELIEF ACT OF 1974 AND EMERGENCY ASSISTANCE ACT OF 1988
| UNEARNED-Funds paid for disaster relief, to save lives, protect property, for public health and safety because of a major disaster or natural catastrophe.
^| E

| Federal Emergency Management Assistance (FEMA) payments for a major disaster or natural catastrophe.
^| E

| FEMA payments for rent, mortgage, food and utility assistance that are made to homeless people when there is no major disaster or natural catastrophe.
^| I

| DISASTER UNEMPLOYMENT ASSISTANCE
| UNEARNED-Unemployment benefits paid to an AU member during a major disaster or catastrophe.
Public Law 100-707 authorizes the President under the Robert T. Stafford Disaster Relief and Emergency Assistance Act to pay unemployment assistance to any individual unemployed as a result of a major disaster for the weeks of unemployment for which the individual is not entitled to any other unemployment compensation.
^| E

| DIVIDEND
| UNEARNED-A share of company profits received by a policyholder or shareholder.

Divide the amount of income anticipated to be received during the POE by the number of months in the POE.
Count the prorated amount in each month of the POE.
Accept the A/R's statement as verification for amounts equal to or less than $10 per month or $120 per year.
^| I
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

| EARNED INCOME TAX CREDIT (EITC)
| EARNED**-**A special tax credit which reduces the federal tax liability of certain low-income working taxpayers.
This tax credit may or may not result in a payment.

EITC payments can be received as an advance from an employer or as a refund from the IRS.
EITC given as a tax credit (no payment) is not income.

Refer to Chart 3405.1, Types of Resources for treatment of EITC received as an income tax refund.
^| E

| EDUCATIONAL GRANT, LOAN, SCHOLARSHIP, WORK STUDY PROGRAM
a|
UNEARNED-Payments for the educational assistance of an AU member enrolled at a recognized institution of post-secondary education, school for the handicapped, vocational program or a program that provides for completion of a secondary school diploma /degree or GED.
Refer to Section 3245, Students.

EARNED-Work study program wages are earnings from a program operated by a secondary or post-secondary school in which the student works and earns money during the year.

NOTE: Exclusions apply only to monies from an educational source and not to educational payments made from income available to the AU such as earnings, contributions from parents, TANF, etc.
| E

E

| EMPLOYMENT AND TRAINING (E&T) /EMPLOYMENT SERVICES (ES) FUNDS
| UNEARNED or EARNED- Payments made to an AU member for reimbursement of costs that are related to participation in the program.
These costs include, but are not limited to, transportation, tuition, registration, education testing fees, equipment, books, required wearing apparel, tools, supplies, and occupational license fees.
^| E
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

| ENERGY ASSISTANCE OTHER THAN LIHEAA
a|
UNEARNED-Payments or allowances made under federal, state or local law for the purpose of assisting the AU with the cost of heating and/or cooling its home.
Payments can be one of the following:

* Federal or state assistance paid only once for weatherization, emergency repair, or replacement of heating or cooling devices.

* Energy assistance payments made under a state law.
| E

I

| ENHANCED RELATIVE RATE (ERR) PAYMENTS
| UNEARNED-Payments that provide financial support in assisting relative caregivers with the basic needs of a related child.
The child must be in the custody of DFCS and placed with the relative caregiver.

If the person for whom the payments are intended is included in the FS AU, count the income in the budget.
^| I

| FAMILY SUBSISTENCE SUPPLEMENTAL ALLOWANCE (FSSA)
| EARNED-Cash benefit up to $500 to supplement the income of low pay grade military families.
Refer to Section 3430, Military Pay.
^| I

| FARM ALLOTMENT
| UNEARNED-Payments received from government-sponsored programs, such as Agricultural Stabilization and Conservation Services, which are a gain or a benefit
^| I

| FARMING
| EARNED-Income received from agricultural labor.
Refer to Section 3425, Self-Employment Income.
^| I

| FOSTER CARE PAYMENTS

(IV-B, IV-E or Title XX)
| UNEARNED-Per diem payments received on behalf of a foster child or foster family.

If the person for whom the payments are intended is included in the FS AU
^| I

| GARNISHMENT
| UNEARNED/EARNED-A set amount of wages or monies withheld by an employer/entity to pay a debt owed to a third party.
^| I

| GENERAL ASSISTANCE
| UNEARNED – Payments received by the AU from county funds administered by a local DFCS Office.
^| I
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

| HOUSING AND URBAN DEVELOPMENT (HUD) RENTAL SUBSIDY
| UNEARNED-Payment received by an AU for rent.
Payments are sometimes distributed by the Georgia Residential Financial Authority (GRFA).

Payments can be made directly to the AU, by a two-party check or directly to the landlord on behalf of the AU.
Refer to Vendor Payments.
^| E

| INCOME TAX REFUND
| Refer to Section 3405, Resources.
|

| INDIAN JUDGEMENT GAMBLING PUBLIC LAW 101-277
| UNEARNED-A payment of up to $2,000 per calendar year per individual made to an American Indian from his/her interest in tribal trusts or restricted lands.
^| E

| INDIAN LAND GRANTS
| UNEARNED-Payments to native Americans based on federal statutes.
^| E

.3+| INDIVIDUAL DEVELOPMENT ACCOUNT (IDA) AND INTEREST
| UNEARNED-Funds withdrawn from an account established by or on behalf of a TANF A/R to pay for post-secondary educational expenses, a first home purchase, or to start up a new business, if the funds are used to pay for a qualified expense.
^| E

| UNEARNED-Interest income accumulated in an IDA account.
^| E

| UNEARNED-Funds withdrawn from an IDA by a non-TANF individual.
^| I

| INHERITANCE
a|
UNEARNED– Cash, a right or non-cash item(s) received as a result of someone's death.
Consider accessibility.

NOTE: Until an item or right has a value or is accessible, it is neither income nor a resource.
^| I

| IN-KIND BENEFIT (SUPPORT AND MAINTENANCE)
| UNEARNED-Any gain or benefit, such as food, clothing, or housing, which is not in the form of money payable directly to the AU.
^| E

| IN-KIND BENEFIT (IN LIEU OF WAGES)
| EARNED-Wages that may include the value of food, clothing, shelter, or other items provided in lieu of cash wages.
^| E

| INSURANCE BENEFITS DUE TO LOSS OF INCOME
| UNEARNED-Benefits paid from an insurance policy due to loss of income.
^| I
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

| INSTALLMENT CONTACTS/ AGREEMENTS
| EARNED OR UNEARNED-Monies given due to a written agreement with specific stipulations for the sale, rental, or lease of property real or personal.
^| I

| INTEREST
a|
UNEARNED-Income received on investments.

Divide the amount of interest income anticipated to be received during the POE by the number of months in the POE and count the prorated amount in each month of the POE.

Accept A/R's statement as verification for amounts equal to or less than $10 per month or $120 per year.

* Refer to Individual Development Account and PASS Account in this chart.
^| I

| IRREGULAR UNEARNED INCOME
a|
UNEARNED- Unearned income that is received irregularly is treated as follows:

* income of $30 or more received in a calendar quarter
* income of less than $30 received in a calendar quarter.
^| I

E

.3+| JOB CORPS
a|
UNEARNED-The following types of income received from Job Corps are budgeted as follows:

* Living allowance.
^| E

a|
* Readjustment allowance.
Payments made to those participating in Job Corps for at least 6 months.
The money is placed into an account and paid to the participant upon leaving Job Corps.
^| E

a|
* Allotments.
Money sent to a dependent child from the Job Corps participant.
This money is an allotment, not child support.
^| E

| JURY DUTY
| EARNED-Compensation received for serving on a jury.
^| I
|===

|===
| | SOURCE/TYPE 3+| DESCRIPTION | FS

|
| LOANS (PERSONAL OR BUSINESS)
3+| UNEARNED-Money received that the borrower must repay to the lender.
A written repayment agreement (signed by both parties) is required in order for the money received to be considered a loan.
There must be an understanding between both parties that the money is a loan and there must be an accounting of the loan activity/balance.
^| E

|
| LOTTERY WINNINGS, GAMBLING WINNINGS, PRIZES, AWARDS AND/OR WINDFALLS
3+a|
UNEARNED-A sum of money received as a result of a winning ticket or receiving other monetary gain in a game of chance.

Budget as income in the first ongoing month of receipt.

NOTE: Refer to Lottery Winnings, Gambling Winnings, Prizes, Awards and/or Windfalls in Chart 3405.1, Types of Resources for ongoing budgeting rules
^| I

|
| LOW-INCOME HOME ENERGY ASSISTANCE ACT (LIHEAA)
3+| UNEARNED-Payments or allowance for home energy provided to, or indirectly on behalf of an AU.
^| E

|
| LUMP SUMS
3+| Refer to Lump Sums in Chart 3405.1, Types of Resources.
^| E

|
| MANAGED INCOME
3+| UNEARNED-Money received and used for the care of a third party who is not a member of the AU.

All or any portion of the money used for the care of the third party.

All or any portion of the money used by the AU.

UNEARNED or EARNED-Money that belongs to the AU that is under the control of a third party in order to pay debts owed by an AU member.
^| E

I

I

|
| MEDICARE PREMIUM PAYMENTS
3+| UNEARNED-Payments made by DMA to pay for Medicare premiums on behalf of Medicaid recipients.
^| E

|
| MILITARY ALLOTMENT
3+| UNEARNED-Payments received by an AU or BG member who is a spouse and/or dependent child of military personnel.

Consider the income as child support if it is for a dependent child.

Refer to Section 3430, Military Pay.
^| I

3+^| *SOURCE/TYPE*
^| *DESCRIPTION*
2+^| *FS*

3+| MILITARY

COMBAT PAY
| EARNED-Payments received by a member of the U.S Armed Forces due to deployment to a designated combat zone for the duration of the member's deployment to or service in the combat zone.

Determine if any of the allotment/monies made available to the food stamp AU is combat pay.
If yes, exclude as income, that portion of the allotment /money.
The additional or special pay may be identified as “incentive pay for hazardous duty” or “special pay for duty” or “special pay for duty subject to hostile fire or imminent danger”.
Refer to Section 3430, Military Pay and Family Subsistence Supplemental Allowance.
2+^| E

3+| MILITARY, NATIONAL GUARD AND RESERVE PAY
| Payments received by military personnel.
Refer to Section 3430, Military Pay and Family Subsistence Supplemental Allowance in this chart.
2+^| I

3+| MILITARY RETIREMENT
| UNEARNED-Income received by military retirees and survivors.
Beneficiaries who may be entitled to receive military payments include the retiree, his/her surviving spouse and children.
2+^| I

3+| MONTGOMERY GI BILL PAYMENTS
a|
UNEARNED- VA payments for individuals enrolled in Active Duty or the Selected Reserve of the Army, Navy, Air Force, Marine Corps, Coast Guard, or Air National Guard for up to 36 months of education assistance.

NOTE: Any portion of funds that come from the individual's earnings is counted as income.
2+^| E

3+| NATIONAL EMERGENCY GRANT (DISASTER RELIEF EMPLOYMENT)
| UNEARNED-Grants funded by FEMA, used to provide disaster relief employment on projects that provide food, clothing, shelter and other humanitarian assistance for disaster victims.
2+^| E

3+| NATIONAL FLOOD INSURANCE PAYMENTS (NFIP)
| UNEARNED-Payments made for flood mitigation activities under the National Flood Insurance Act of 1968.
2+^| E

3+| NOISE ABATEMENT PAYMENTS
| UNEARNED-A non-recurring payment designated for noise abatement work on a dwelling.
2+^| E

3+| OVERTIME PAY
| EARNED-Extra income paid to employees who work in excess of 40 hours in a week.
Refer to _Wages_ in this chart.
2+^| I

3+| PENSION
| UNEARNED-A payment received regularly as a retirement benefit.
2+^| I
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

| PUBLIC LAW 103-286
| UNEARNED-Payments to individuals received as a result of their status of Nazi persecution.
^| E

| REFUGEE CASH ASSISTANCE (RCA)
a|
UNEARNED-Cash payments provided to new entrant refugees, asylums, Amerasians, Cubans/Haitians, and victims of human trafficking (or other refugee eligible status) who are ineligible for TANF benefits.
Payments are provided to these immigrants for a period, up to 8 months on a one-time basis.

* ** If the Date of Entry for an individual is on or after 10/1/2021, payments are provided to these immigrants for a period, up to 12 months, from the month the immigrant entered the USA or the month refugee status eligibility started, on a one-time basis.
^| I

| REIMBURSEMENT
| UNEARNED-Payment for past expenses that do not represent a gain or benefit to the AU.
^| E

| RELATIVE CARE SUBSIDY
| UNEARNED-Monthly per diem paid out of redirected TANF funds for children placed with relatives whose parents have agreed to non-reunification.

If the person for whom the payments are intended is included in the AU.

If the person for whom the payments are intended are not included in the AU.
^| I

E

| RENTAL INCOME
| Money received on property owned by an AU member and rented to others.

EARNED-must be engaged in management of property an average of 20 hrs./week.

UNEARNED-if not involved in management more than 20 hrs./week.
Refer to Section 3425, Self-Employment.
^| I
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

| REPAYMENT FOR OVERPAYMENT OF BENEFITS
a|
UNEARNED-Money withheld from an income source (earned or unearned) or money received from any income source, which are voluntarily or involuntarily returned to repay a previous overpayment received from that source.
Do not count the repayment amount.
Ex: An AU receives Social Security Administration (SSA) income of $534/mon.
SSA discovers that it overpaid the AU in benefits because the AU failed to report wages.
SSA must now withhold $100/mon from the AU's income to repay an overpayment of benefits.
The overpayment amount is excluded as income.
$434 ($534 - $100) is counted in the budget. +
Wages or monies withheld by an employer/entity to pay a debt owed to a third party are garnishments.
Money withheld from assistance from another program is included as income.
Refer to Garnishments in this section.
Ex: An AU receives Social Security Administration (SSA) Income of $680/mon.
SSA discovers that the AU owes the IRS in taxes.
SSA must now withhold 80/mon to pay the IRS.
This overpayment amount is included as income.
$680 is counted in the budget. +
[.underline]#Non-Means Tested Income#

For a non-means-tested program (e.g., RSDI, UCB), do not count the repayment amount.
Count the gross minus the repayment amount.

[.underline]#Means Tested Income# +
For a means-tested program (e.g., TANF) count the income as follows:

* if fraud/IPV, count the gross income.
* if AU error, count the gross income.
* if agency error, do not count the repayment amount.
* if the reason for the overpayment is unknown, do not count.

Exclude the repayment amount.
Document the reason for the overpayment.
If the worker is unable to verify the reason through third party verification, document the attempt and exclude the repayment amount.
^| E

I
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

| RESTITUTION FOR WORLD WAR II INTERNMENT OF JAPANESE-AMERICANS AND ALEUTS;
PL 100-383
| UNEARNED – Payment of $20,000 made to U.S. citizens of Japanese ancestry and to lawful permanent resident Japanese aliens or their survivors, and to Aleutian Island residents, as a consequence of their evacuation, relocation, and internment during World War II.
^| E

| RETIREMENT
| UNEARNED-A sum of money paid regularly as a retirement benefit.
^| I

| ROOMER
| EARNED-Payments received for a room only.
Refer to Section 3425, Self-Employment Income.
^| I

| SALARY
| EARNED-Fixed compensation for services paid to a person on a regular basis.
Most salaries are considered overtime exempt.
Refer to _wages_ in this chart.
^| I

| SCHOOL LUNCH PROGRAM
| UNEARNED-The cash value of assistance provided to children under the National School Lunch Program, Child Nutrition Act, Special Milk Program, School Breakfast Program.
Refer to Child Nutrition Payments in this chart.
^| E

| SELF-EMPLOYMENT
| EARNED-Income received from a self-employment enterprise, including rental property, roomers and boarders.

Refer to Section 3425, Self-Employment.
^| I

| SENIOR COMMUNITY SERVICE EMPLOYMENT PROGRAM
a|
EARNED-Payments made to individuals age 55 or older under Title V of the Older Americans Act for part-time employment.

Agencies that receive funds for this program include:

* U.S.D.A. Forest Service/ Forest Service
* National Able Network/ABLE
* National Council on Aging, Inc./NCOA
* National Caucus & Center on Black Aged, Inc/NCBA
* Mature Services, Inc/Mature Services
* SER-Jobs for Progress National, Inc/SER
* Senior Service America, Inc
* National Council of Senior Citizens
* American Association of Retired Persons/AARP
* Experience Works Inc/Experience Works
* National Indian Council on Aging/NICOA
* National Asian Pacific Center on Aging/NAPCA
* Asociacion Nacional Pro Personas Mayores
* Easter Seals, Inc.

* DHS Division of Aging Services

Also refer to Volunteer Payments in this chart.
^| E
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

| SEVERANCE PAY
| EARNED-Payments received from an employer [.underline]#upon# termination of employment.

UNEARNED-Payments received from a former employer [.underline]#after# termination of employment.
| I

I

| SHARED HOUSEHOLD EXPENSES
| UNEARNED-Payments made to an AU by a person who shares household expenses, and which do not represent a gain or benefit to the AU.
^| E

| SICK PAY
| Payments made to an individual when out of work because of illness.

EARNED-If paid from the employer's payroll.

UNEARNED-If paid by another source (e.g., insurance).
^| I

| SOCIAL SECURITY BENEFITS (RSDI)
| UNEARNED-Retirement, Survivors, Disability Insurance (RSDI) benefits received from the Social Security Administration (SSA).

Do not use the check as the sole source of verification of the entitlement amount.
Include the amount paid for Part B Medicare premium in the total gross amount if paid by the individual or DMA.
^| I

| SPENDING ACCOUNT
| EARNED-Pre-taxed earnings that are deducted from an employee's gross wages and placed in an account to pay AU expenses such as childcare and medical costs.

*Refer to Wages/Salary.
^| ***

| STRIKE BENEFITS
| UNEARNED-Income received by individuals on strike.

Refer to Section, 3240, Strikers.
^| I

| SUBSIDIZED EMPLOYMENT (GRANT DIVERSION)
a|
EARNED-Full-time employment in the private for-profit, private non-profit, or public sector in which the recipient's wages are subsidized through TANF (grant diversion) and the TANF benefits are reduced to zero.

NOTE: Exclude the TANF grant amount from the gross wages received.
^| I

| SUPPLEMENTAL SECURITY INCOME (SSI)
| UNEARNED-Benefits paid by the Social Security Administration for Aged, Blind, or Disabled individuals.

Refer to Lump Sum/SSI Back Payments in Chart 3405.1, Types of Resources.
^| I
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

| TANF
| UNEARNED-Benefits received from Temporary Assistance for Needy Families (TANF), including under issuance received in the month it is intended to cover.
^| I

| TANF CORRECTIVE
| UNEARNED-TANF benefits for a previous month owed to the AU because of agency error.
^| E

| TANF EMPLOYMENT INTERVENTION SERVICES (EIS)
| UNEARNED- A non-recurrent lump sum payment that is equivalent to four times the maximum TANF grant amount for the AU size.
The income is treated as a lump sum payment and is not counted in the food stamp budget.

Refer to Lump Sum Payments in Chart 3405.1, Types of Resources.
^| E

| TANF FROM ANOTHER STATE
| UNEARNED-TANF benefits received from another state.
Budget for month of receipt only.
^| I

| TANF TRANSITIONAL SUPPORT SERVICES (TSS)
| UNEARNED-TANF support payment used to pay for or reimburse the cost of childcare, transportation, and incidental expenses to an applicant or a recipient.
TSS is available to former TANF clients for a twelve-month period of WSP eligibility.
The twelve-month count begins the first month of TANF ineligibility.
TSS should run concurrently with the AU's twelve-months of WSP.

TSS is not counted in the food stamp budget.
^| E

| TANF WORK SUPPORT PAYMENT (WSP)
a|
UNEARNED-Work support payments provide time limited cash supplements to former TANF recipients who are transitioning from TANF dependency to self-sufficiency.

WSP payments are available to former TANF AUs for a period of twelve months and twice in their lifetime receipt of TANF.

The former TANF AU receives a cash supplement of $200 per month.
If the AU is eligible for and/or receives TFS benefits, the income is not counted in the food stamp budget during the first five (5) months of TFS eligibility.

NOTE: If the AU is not receiving TFS benefits, then the income is counted in the food stamp budget.

If the AU reapplies for/ receives regular food stamp benefits, the income is counted in the food stamp budget.
| E

I
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

| TIPS
| EARNED-Voluntary payments above the stated cost of a product or service given in appreciation for the service rendered.
Refer to _Wages_ in this chart.
^| I

| TRADE READJUSTMENT ALLOWANCE (TRA)
| UNEARNED-Weekly payment available for up to 52 weeks after an individual's UCB is exhausted and during a period in which the individual is participating in a full-time training program approved in accordance with the Trade Act.
^| I

| TRAINING ALLOWANCE/ STIPEND
| EARNED– Payment received from vocational/ rehabilitation programs recognized by federal, state, or local governments, to the extent the payments are not a reimbursement or specifically excluded
^| I

| TRUST FUND PROCEEDS
a|
UNEARNED-Payments from a trust.

* payments distributed to the AU
* payments that the AU has access to but are reinvested
* income earned by the trust to which the AU has access.
^| I

| UNEMPLOYMENT COMPENSATION BENEFITS (UCB)
| UNEARNED-Benefits received from the Department of Labor (DOL) by unemployed individuals.

Refer to Trade Readjustment Allowance in this chart.
^| I

| UNIFORM RELOCATION ASSISTANCE AND REAL PROPERTY ACQUISITION OF 1970 PAYMENTS
| UNEARNED-Reimbursements received under Public Law 91-646, Section 210.
^| E

| UTILITY REIMBURSEMENT HOUSING AND URBAN DEVELOPMENT (HUD) OR FARMERS HOME ADMINISTRATION (FMHA) UTILITY REIMBURSEMENT
| UNEARNED-A utility reimbursement paid by HUD or FMHA to an AU that receives housing assistance and is responsible for paying its utilities separately from its rent.

Refer to Vendor Payments.
^| E

| VACATION PAY
| EARNED-Any amount paid to employees for a regular scheduled period spent away from work or regular duty.
It includes amounts paid even if the employee chooses not to take a vacation.
Refer to _Wages_ in this chart.
^| I
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

| VENDOR PAYMENTS OTHER THAN GENERAL ASSISTANCE VENDOR PAYMENTS
a|
UNEARNED-Money paid by an outside source to a third party on behalf of the AU for an expense.

Vendor payments are treated as follows:

* Monies deducted or diverted by the AU or BG to go directly to a third party to pay AU or BG expenses.

* HUD vendor payment for rent or mortgage

* Reimbursements made in the form of vendor payments

* Vendor payments from monies that are legally obligated and otherwise payable to the AU

* Vendor payments from money that is not owed to the AU

* Monies deducted or diverted by court order or other legally binding agreement, such as child support and alimony, to go directly to a third party to pay an AU expense

Refer to General Assistance, Child Care Subsidy in this chart.
| I

E

E

I

E

E

| VETERANS ADMINISTRATION (VA) BENEFITS EDUCATIONAL

(NON-EDUCATIONAL)
| UNEARNED-Benefits received from the VA by a veteran or a veteran's dependent(s) for educational purposes.

UNEARNED-Disability and/or survivors benefits including Aid and Attendance Care, received from the VA by a veteran or a spouse or a dependent of a veteran.
Also included are stipends paid for participation in a study of Vietnam-era veterans' psychological problems.

Refer to Agent Orange Payment in this chart.
^| E

I
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

| VETERANS ADMINISTRATION (VA) PAYMENTS FOR CHILDREN OF VIETNAM VETERANS WITH BIRTH DEFECTS
| UNEARNED-Benefits paid by the VA to children of Vietnam veterans born with congenital spinal bifida and certain other defects.
^| E

| VICTIM RESTITUTION
a|
UNEARNED-Money received by a victim of a crime from a crime victim restitution program, usually as a reimbursement for financial losses.

* The value of the payment does not exceed the value of the loss.
* The value of the payment exceeds the value of the loss.
Count the excess value.

* The payment is a set monthly amount based on a court ruling.
^| E

E

I

I

| VISTA VOLUNTEER PAYMENT
| EARNED-Income received by VISTA volunteers under Title I of the Domestic Volunteer Services Act.
Included are payments from the Urban Crime Prevention Program.

Exclude income only for individuals who were receiving TANF or FS when they originally joined VISTA, even if there is a break in participation.
^| I

| VOLUNTEER PAYMENTS
| UNEARNED-Payment received under Title II of the Domestic Volunteer Services Act of 1973, including the Retired Senior Volunteer Program, Foster Grandparents Program and Senior Companion Program.
^| E

| WAGES
| EARNED- Payment given in return for labor, goods, and services rendered.
Wages may be paid on an hourly, weekly, or daily basis.

Include commissions, tips, overtime, vacation pay, bonus pay, flex benefits, and the employee's share of FICA when paid by the employer.
^| I
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

.2+| WAGES OF A CHILD
| EARNED-Wages, including self-employment wages of a child under age 18 who is attending GED classes, in elementary or secondary school at least ½ time and who is under parental control of an AU member.
Exclusions apply during school breaks as long as the intent is for the child to return to school.

Wages of a child under age 18 not under parental control or not in school.
^| E

I

| EARNED-Wages, including self-employment wages of a child who turns 18 are included effective the month following the month in which the 18^th^ birthday falls, unless the birthday falls on the first day of the month.
^| I

| WORKER'S COMPENSATION
| EARNED or UNEARNED-Payments received by an employee injured on the job.

EARNED-If the AU member remains employed during recuperation and expects to return to work.

UNEARNED-If the AU member does not remain employed during recuperation.

Include the full amount of the award.
Do not exclude any amount withheld for legal expenses.
| I

I

| WORKFORCE INVESTMENT ACT (WIA)
a|
UNEARNED-Payments, such as training allowances and grants, that are not designated for services rendered.

EARNED-On-the-job training payments for:

* Children under age 19 who are 1) under the parental control of another adult AU member, and 2) in a year-round program

* Children under 19 who are not under the parental control of another adult AU member and/or are not in a year-round program

* Adults, age 19 and up
| E

E

I

I
|===

|===
| SOURCE/TYPE | DESCRIPTION | FS

| YOUTH BUILD PROGRAM PAYMENTS
| EARNED-Payments made through the Youth Build Program.

*Treat as WIA income.
^| *

| YOUTH PROJECT PAYMENTS
a|
UNEARNED-Payments made through projects developed to assist youth in acquiring work skills, including the following:

* youth incentive entitlement pilot project
* youth community conservation and improvement projects
* youth employment

*Treat as WIA income.
^| *
|===
