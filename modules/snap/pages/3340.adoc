= 3340 Residency
:chapter-number: 3300
:effective-date: September 2020
:mt: MT-61
:policy-number: 3340
:policy-title: Residency
:previous-policy-number: MT-57

include::ROOT:partial$policy-header.adoc[]

== Requirements

Assistance unit (AU) members who receive benefits must live or intend to live in Georgia.

== Basic Considerations

[.text-center]
*Georgia Residency*

There is no specific durational requirement for residency to participate in the Food Stamp Program.
The place of residence need not be a fixed dwelling.
The AU may be homeless.

[.text-center]
*County Residency*

An AU may apply for assistance in any county but is encouraged to apply in the county in which he/she may reside.
The AU may be certified for benefits in any county within the state.
Refer to xref:3105.adoc[Section 3105], Application Processing.

== Verification/Documentation

Verify residency prior to certification of _initial applications._

Residency may be verified by one of the following sources:

* Georgia Department of Driver Services (DDS) interface if the results from the interface verify that the customer has a current and valid Georgia Driver's License or Identification card
* mortgage statement or lease
* rent or utility company receipts
* school records
* written statement of responsible reference
* TANF/SSI and TCOS eligibility
* any other document proving residency

This list is not all inclusive.

At _review_ accept and document the AU's statement of residency unless information known to the agency conflicts with the AU's statement.
