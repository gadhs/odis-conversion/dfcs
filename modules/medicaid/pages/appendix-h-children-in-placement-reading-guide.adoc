= Medicaid CAR Guide and SUCCESS Instructions Revenue Maximization Unit
:mt: MT-31

== ADDR/NARR/PREV

. Application/review form/change form
 ** For applications, is a signed application in the CR?
For reviews, is the review document in the CR?
For changes, is the change form in the CR?
 ** NARR documented for all case actions and indicate type of contact and/or action taken?

. Correct forms in case record?
 ** All appropriate forms in case record?
 ** Is the case record organized per established standards?

. Address correct?
 ** Is the placement name and address correct in ADDR Residential Address?
 ** Is the custody county name and address correct in ADDR Mailing Address?

== AREP/Stat

[start=4]
. Adoptive placement/relative care established and documented?
 ** Are adoptive parents documented on AREP screen?
 ** For a new relative care placement for a child in DFCS custody, is the relative name, address and relationship entered on AREP screen?
Documented?

. Denial/closure completed timely, accurately and documented?
 ** If case was denied, was correct denial reason (500 code) used, and was denial reason thoroughly documented in remarks?
 ** If case was closed, was correct closure reason entered?
Closure reasons thoroughly documented in remarks?

. Timely and correct notice for action taken?
 ** Is a notice sent to the SSCM, by the SOP of the application, review, change explaining the AU's eligibility?

. Retroactive Medicaid documented and correctly coded?
 ** Retroactive months correctly determined, documented and processed?

. Period of eligibility addressed and documented?
 ** Correct month of eligibility?
 ** Historical changes
 ** Denial of removal month explained

. CMD completed?
 ** Was a CMD completed, if appropriate?
Circumstances and outcome of CMD?

. Alerts addressed?
 ** Have all alerts been completed and documented?

== DEM1/DEM2/DEM3/DEM4 Screens

[start=11]
. CSS referral correct and documented?
 ** Was a referral made to CSS, if applicable?
Was 122 completed or mailed and documented?
If referral was not made, was REMA screen behind FCAR documented with explanation?

. Deprivation established, coded and documented?
 ** Was deprivation correctly determined and documented?
 ** If SUCCESS work around code entered, was code explained and documented?

. Pregnancy verified and documented?
 ** Pregnancy verified?
Was correct date of termination entered on DEM1?

. Citizenship and identity documented and verified?
 ** Citizenship addressed?
Any verification provided?
Is copy in record?
 ** If alien, WEB-1 VIS/CPS verified and documented as to DHS status and date of entry?
Alien status documented?

. TPR/Immunization and Health Check addressed and documented?
 ** Were TPR and Health Check addressed and documented?

. DEM screens coded and documented?
 ** Is SSN entered and if there is no SSN or a matching problem, is there documentation to resolve discrepancy in record?
 ** Are DOB, sex, race, marital status and living arrangement coded correctly?
 ** Is SSI documented correctly for AU member who receives SSI benefits?

== Alas/FSME

[start=17]
. EMA verified and documented?
 ** Are EMA dates of service keyed correctly?
 ** Is correct, signed form 526 in record?

. School information documented, if known?

== FCAR

[start=19]
. Each case action documented in detail with date, type of action, case manger's last name, first initial and case load number, Revenue Maximization location and phone number?
 ** Initial placement documented in detail with date entered custody, date of application and SOP, SSCM's name, county and contact number, court order type, date and required language, date court order received from SSCM, [.underline]#each# AFDC Relatedness criteria addressed, removal from/ living with specified relative, removal home information, OCSS referral, current placement and reimbursability, COA determination and/or CMD?
 ** 962 requests, who requested, sent to, date and address?
 ** Citizenship/Alienage for the eligibility month?
 ** Was AFDC Relatedness AU/BG composition correctly determined and documented?
 ** Was relationship correctly established for all AFDC Relatedness AU/BG members?
 ** Were all possible AU/BG combinations addressed to provide the most advantageous BG?
 ** Date Form 529 sent to county and to accounting
 ** Prior month's MAO addressed and action taken
 ** New relative placement: name, address, relationship, DFCS custody?
Relative Care Option?
 ** All subsequent case actions, placement episodes and relevant information documented in detail?

== RES1

[start=21]
. Document all resources and value of the resource of a Child in Placement at REMA without entering amount in screen.
SUCCESS is not programmed for Children in Placement Medicaid.

== ERN1/ERN2/Care/Deal

[start=21]
. Verification obtained and documented?

. Clearinghouse checked and all discrepancies in information researched and documented?

== UINC

[start=21]
. SDX/BENDEX/UCB discrepancies addressed and documented?
 ** Are the SDX, BENDEX, and UCB screen results documented and addressed?
 ** Unearned income entered for RSM COA?
 ** If member receives SSI or RSDI, is SDX/BENDEX result documented and verified with the SSCM's statement to verify income?

. Verification obtained and documented?
 ** Is the type of income received verified and documented on the UINC REMA screen?
 ** Child support coded correctly?
CS/CD/GP
  *** If child support is received through OCSS, is the income coded as (CS) on the child's line number who receives the income?
  *** If the county receives direct child support from an absent parent, is the income coded as (CD) and on the child's line number who receives the income?

* Is there a contribution statement or statement from a third party stating the amount contributed to the AU, how often contributed to the AU, and if the income will continue?

== Misc

[start=21]
. Document OSOP reason?
