= 2334 Savings and Checking Accounts
:chapter-number: 2300
:effective-date: December 2019
:mt: MT-57
:policy-number: 2334
:policy-title: Savings and Checking Accounts
:previous-policy-number: MT 54

include::ROOT:partial$policy-header.adoc[]

== Requirements

The resource value of a savings instrument with a financial institution or NH is the balance of the account, less any early withdrawal penalty.

== Basic Considerations

The term savings encompasses the following:

* cash savings on hand
* checking and savings accounts
* certificates of deposit (CDs)
* credit union accounts
* time deposits
* NH patient fund accounts
* NH prepayments/deposits if to be returned to A/R
* individual retirement accounts (IRAs)
* Keoghs.

Any cash on hand received in a prior month and retained after the month of receipt is a resource.

NOTE: For the treatment of retirement funds, including IRAs and Keoghs, see Section 2332, Retirement Funds

=== Verification and Documentation Family Medicaid

Accept the A/R's statement of account balance(s) unless questionable.
If the total of all liquid and non-liquid resources exceeds 75% of the resource limit, the account balances must be verified.
Refer to Section 2301, Family Medicaid Resources Overview.

*Verification and Documentation ABD Medicaid*

At application obtain financial statements that will verify account balance(s) as of the first moment of the first day of each month Medicaid eligibility is requested.
For A/Rs alleging to have accounts with financial institutions, obtain copies of statements and/or updated passbooks.

If the A/R's past financial history is questionable, it is reasonable at initial application to request financial statements for a minimum of three months prior to the first month that Medicaid eligibility is requested in order to establish a history of financial transactions.

NOTE: A/R's with a *Direct Express Debit card* do not receive monthly statements.
The resource type is cash [on hand] as Direct Express debit card is not a savings or checking instrument with a financial institution.
This is not a checking account, the only monies that can be direct deposited are Social Security, Supplemental Security Income (SSI), VA Compensation or Pension, RRB Annuity and OPM (Federal Retirement).
Count Direct Express deposits as income in the month received.
The balance is considered a resource the first day of the following month.

For NH A/Rs, verify ownership and balance of patient fund accounts by telephone or Form 958 on every application and redetermination, regardless of whether the A/R alleges having a patient fund account.

If the above records are available, and if they appear accurate and complete, calculate the balance(s) as of the first moment of the first day of the verification month.
Take into consideration any deposits, withdrawals or checks that have been written that are reflected on check stubs/passbooks that are not reflected on the account statement.

If the accuracy, reliability or completeness of the account statement and the A/R's personal records is questionable, a Form 957, MAO Resource Clearance Form, must be completed by the institution.
Form 957 must indicate the balance as of the first moment of the first day of the month.

Inquire as to the disposition of previously owned accounts and develop as necessary.
Check available IEVS IRS matches on each review for unreported accounts.

== Special Considerations

=== Joint Accounts Family Medicaid

When an AU of Family Medicaid Budget Group member is named on a joint bank account with a non-AU or BG individual solely for the convenience or emergency, exclude the joint account as a resource to the Family Medicaid AU or BG member if the other individual, or someone who is in a position to know, verifies that s/he has deposited all the money in the account and all withdrawals are used for the non-AU or non-BG individual's benefit.

=== Joint Accounts ABD Medicaid

If an A/R's or deemor' s name appears on any checking or savings account, including an account under several names, assume that the A/R is an owner of the account unless the A/R verifies otherwise through the rebuttal process.
Refer to _Joint Account Ownership Rebuttal_ in this section for rebuttal procedures. +
If an account is owned jointly by one or more Medicaid A/Rs and one or more non-Medicaid individuals, count all of the funds in the account as a resource to the Medicaid A/Rs in equal shares.
Do _not_ allow a share of the funds to the non-Medicaid individuals.

Assume that an A/R with ownership interest in a checking or savings account has unrestricted access to the account unless the A/R verifies restricted access through the rebuttal process.
Refer to Unrestricted Access Rebuttal in this section for rebuttal procedures.

=== Joint Account Ownership Rebuttal

If an individual wishes to rebut the applicable ownership assumption, obtain his or her statement regarding the following:

* who owns the funds
* why there is a joint account
* who has made deposits to and withdrawals from the account
* how withdrawals have been spent.

In addition, inform the individual that he or she must submit the following evidence within 30 days:

* a corroborating statement from each other account holder
+
NOTE: If the only other account holder is incompetent or a minor, have the individual submit a corroborating statement from anyone aware of the circumstances surrounding establishment of the account.

* account records showing a history of deposits, withdrawals and interest payments.
* if the individual owns none of the funds, evidence showing that he or she can no longer withdraw funds from the account, such as removal of the individual's name from the account.
* if the individual owns only a portion of the funds, evidence showing removal from the account of such funds, or removal of the funds owned by the other account of such funds, or removal of the funds owned by the other account holder(s) and re-designation of the account.

Exclude from the A/R's resources any funds that the evidence establishes were owned by the other account holder(s) and can no longer be withdrawn from the account by the A/R.

[caption=Exception]
NOTE: Such funds are a countable resource in determining the A/R's resource eligibility if the account holder to whom they belong is a deemor.

=== Withdrawals by other Account Holder(s)

Develop a transfer of resources penalty for any withdrawals made on or after 8-11-93 by the other account holder if the A/R has not successfully rebutted ownership.
Refer to Section 2342, Transfer of Resources.

[caption=Exception]
NOTE: In Family Medicaid there is no penalty for transferring resources.
Consider only resources owned by the AU at the time of the eligibility determination.

=== Unrestricted Access Rebuttal

If an A/R verifies through the financial institution that s/he cannot withdraw funds from a checking or savings account without the signature of the other joint owner(s), consider the A/R to have restricted access to the account, and exclude the account from the A/R's countable resources.

[caption=Exception]
NOTE: If a _restricted_ account is owned jointly by an A/R and deemor only, the account is a countable resource.

=== Georgia STABLE Account

The Achieving Better Life Experiences Act (ABLE) was passed by the United States Congress in December 2014.
The ABLE Act amends Section 529 of the Internal Revenue Code to create tax advantaged savings accounts for individuals with disabilities.
This federal legislation allows eligible individuals with disabilities the opportunity save and fund a variety of qualified disability expenses without impacting their ability to receive Supplemental Security Income (SSI), Medicaid, and other federal programs.
The Georgia ABLE program is named Georgia STABLE.

The purpose of the ABLE Act is to permit people with disabilities and their families to save money in and withdraw funds from their ABLE accounts which can be used to help maintain health, independence, and quality of life.

Individuals with disabilities and their families can save up to a maximum amount annually in a tax-advantaged account for disability-related expenses.
The maximum amount may change yearly.

*Maximum amount:*

*2017* 14,000.00 +
*2018* 15,000.00

NOTE: Effective January 1, 2019, the designated beneficiary of an ABLE account is temporarily allowed to contribute additional monies above the maximum annual limit towards savings.
The balance within a single ABLE account for an individual, including contributions and earnings, is an excluded resource.

An ABLE account is an account established and owned by an eligible individual and maintained under a qualified ABLE program. +
The eligible individual is the designated beneficiary of the ABLE account.
An individual is permitted to have only one ABLE account. +
The individual may open the account in their state of residence, or in another state's ABLE program.

A parent, legal guardian, or the holder of a Power of Attorney can also open, set up, and manage an ABLE account for their loved one.

Whenever a Medicaid applicant or recipient reports they are the owner of an ABLE account, obtain verification of documentation of the following:

* The name of the designated beneficiary or owner of the account
* The state ABLE program administering the account
* The name of the person who has signature authority (if different from the owner)
* The unique account number assigned by the state to the ABLE account
* The date the account was opened
* The first of the month account balance

=== Treatment of Funds in an ABLE Account

All funds in an ABLE account are disregarded including earning on the account (e.g. interest) when determining eligibility of Medicaid applicants and beneficiaries who are subject to a resource test. +
The balance within a single ABLE account for an individual, including contributions and earnings, is excluded as a resource.

=== Qualified Disability Expenses

The funds in an ABLE account are intended to cover an individual's Qualified Disability Expenses (QDEs) related to his or her blindness or disability. +
QDEs include, but are not limited to education, housing and basic living expenses, transportation, employment training and support, assistive technology and personal support services, health, prevention and wellness, financial management, and administrative services, legal fees, and funeral and burial expenses.

=== Treatment of Distributions

Distributions from ABLE accounts are excluded if used or intended to be used for QDEs as long as the distributions are identifiable.

* Distributions from an ABLE account are excluded if used or intended to be used for QDEs
* Distributions from an ABLE account used for non-qualified expenses are excluded if spent in the month of receipt

Distributions from an ABLE account are countable when:

* Distributions are retained past the month of receipt and are used for or intended to be used for non-QDE or
* Distributions are retained past the month of receipt and were previously excluded because it was intended for a QDE but was used for a non-qualified expense.
Count the amount of funds used as a resource the first of the month in which funds were spent;
or
* Distributions are retained past the month of receipt, have not been spent, and the intent to use funds as a QDE has changed.
Count the retained funds as a resource the first of the following month.

=== Estate Recovery

Estate Recovery applies to funds in an ABLE account that have become part of an estate subject to recovery.
