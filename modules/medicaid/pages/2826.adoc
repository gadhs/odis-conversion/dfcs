= 2826 AFDC Deprivation- Unemployed Parent
:chapter-number: 2800
:effective-date: November 2020
:mt: MT-62
:policy-number: 2826
:policy-title: AFDC Deprivation- Unemployed Parent
:previous-policy-number: MT 45

include::ROOT:partial$policy-header.adoc[]

== Requirements

When both parents are in the home, the unemployment of the parent who is the principal earner (PE) deprives a child of parental support.

AFDC-UP is a type of deprivation and not a special type of AFDC.
An AFDC-UP AU is subject to all processing requirements of the AFDC program

== Basic Considerations

For AFDC-UP purposes, unemployment of the PE is defined as one of the following:

* Being out of work for 30 consecutive days

[.text-center]
*OR*

* Working fewer than 100 hours in the 30 consecutive calendar days prior to approval

[.text-center]
*AND*

* Working fewer than 100 hours in a calendar month after approval

=== Deprivation Requirements

To establish deprivation because of the unemployment of the PE, the PE must meet each of the criteria below:

* Be unemployed for at least 30 consecutive calendar days prior to the AFDC- Relatedness budget month

* Have a recent connection to the workforce

Parents must live together, but are not required to be married to each other at the time of the budget month.

If the PE does not meet the citizenship/alien status requirement and does not have INS authorization to work, the family is ineligible for AFDCUP, and the child cannot be considered deprived due to the unemployment of the parent.

If the PE has INS authorization to work, but does not meet the citizenship/alien status requirement, the PE is not included in the AU.
However, other family members are potentially eligible for AFDC-UP.

Unemployment compensation benefits (UCB) received by the PE are budgeted uniquely.
A family is not eligible for AFDC-UP if the PE is unemployed because s/he is on strike.

== Procedures

Determine who lives in the home and their relationship to each other.

Explore eligibility for AFDC-UP when a blended family applies and one parent is unemployed as follows:

* When the PE is designated and meets the AFDC-UP definition of unemployed, include the parents, the mutual child(ren), and the child(ren) of each parent in one AU.

* If the AU does not meet the AFDC-UP deprivation requirements, deny AFDC-UP and explore the eligibility of the child in other AU compositions.

=== Initial Determination

Determine eligibility using the steps on the following pages.

=== Determine the Principal Earner

Determine the Principal Earner.
The Principal Earner (PE) is the parent with the greater earnings in the two years prior to the application month.

Accept the AU's statement to establish which parent had the greater earnings unless the information provided conflicts with other information available to the agency.

Determine the PE at application following the guidelines in Chart 2826.1

.Chart 2826.1 Determining the PE at Application
|===
| IF AT APPLICATION | THEN

| Both parents have earned income in the 24 months preceding the application month
| The parent with the higher earnings in the 24 months is the PE

| The earnings of each parent are equal in the 24 months prior to the month of application
| The parent with the higher earnings in the most recent six months is the PE.
|===

=== Determine if the PE meets unemployment Criteria

Use Chart 2826.2 to determine if the PE meets the unemployment criteria at the time of application.

.Chart 2826.2 Establishing Unemployment Status
|===
| IF THE PE | THEN

| Is not working at the time of application

*AND*

Has been unemployed for 30 consecutive days
| The unemployment criterion is met on the date of application.

| Is employed at the time of application

*BUT*

Worked fewer than 100 hours in the 30 consecutive days prior to the application date
| The unemployment criterion is met on the day of application.
|===

=== Establish if the PE has a recent connection to the workforce

Use chart 2826.3 to determine recent connection to the work force.

At the point the PE meets any one of the requirements, recent connection to the work force is met.

Determine whether the requirement for recent connection is met in the order listed in the chart.

.Chart 2826.3 Recent Connection to the Work Force
|===
| IF THE PE | THEN

| Currently receives UCB
| A recent connection to the work force is met.

Verify with Clearinghouse or UCB check stub.

| Received UCB within one year prior to the date of application (including the application month and the 12 prior calendar months)
| A recent connection to the work force is met.

Verify with Clearinghouse or UCB check stub.

| Would have been eligible to receive UCB in the year prior to the application month had s/he applied
| A recent connection to the work force is met.

Verify with Clearinghouse

| Performed work which was not covered under Georgia's UCB law,

BUT

If it had been covered, the PE would have been eligible for UCB.

The PE must provide wage information on the nine quarters prior to the application quarter.
| Accept the PE's statement of earnings.

Complete Form 270 and submit to the Department of Labor (DOL) for a determination of potential eligibility for UCB.

If DOL shows potential eligibility for UCB, a recent connection to the work force is met.

a|
Had six calendar quarters of work, education or training within any of the 13 consecutive calendar quarter periods ending within the four years prior to the application.

This four-year period may include the application quarter and the prior 16 calendar quarters.

The six quarters may be met by a combination of the following:

* Wages of at least $50 gross earnings per quarter
| A recent connection to the workforce is met.

Use Clearinghouse to verify $50 in earnings for the work quarters in question.

If school attendance is used to meet this requirement, verify attendance through the school.

a|
* Participation in a JTPA education or training activity

* Enrolled as a full time student in elementary, secondary (or an equivalent secondary program), vocational or technical training course for any part of a quarter
|
|===

NOTE: No more than four quarters of education/training can be applied toward the six required quarters of work.
A maximum of four quarters of education/training may be applied in a lifetime.

=== Determine if the PE Has Refused a Bona Fide Offer of Employment

Determine if the PE failed to accept an offer of employment or training for employment within the 30 consecutive days prior to approval.

If the PE has refused such an offer without good cause, deprivation cannot be met.

Accept the SSCM's statement regarding the offer or acceptance of a bona fide offer of employment unless the agency has information which conflicts with the statement.

Determine the AU's eligibility taking the following into account:

=== Determine Eligibility Based on Financial Criteria

Apply all normal budgeting procedures with the exception of the budgeting of UCB income.
Refer to Section 2835, AFDC Relatedness Budgeting.

Retroactive UCB payments are not treated as a lump sum in AFDCUP.
Budget retroactive UCB payments in the month received.

=== Processing

The initial AFDC Relatedness budgeting is completed by SHINES based on information for the removal home.
RMS will review, verify, document and correct information in SHINES that pertains to the removal home circumstances including income, household management in order for the AFDC budgeting to be completed accurately for IV-E funding determination eligibility.
Reference Section 2835 – AFDC Relatedness Budgeting.

=== Redetermination

With passage of the Fostering Connections to Success and Increasing Adoptions Act of 2008 (Public Law 110-351), the AFDC criteria for redeterminations was eliminated.
Reference Section 2870 – Redeterminations for Children in Placement for redetermination policy.
