= 2149 Georgia Medicaid for Workers with Disabilities
:chapter-number: 2100
:effective-date: February 2020
:mt: MT-58
:policy-number: 2149
:policy-title: Georgia Medicaid for Workers with Disabilities
:previous-policy-number: MT 37

include::ROOT:partial$policy-header.adoc[]

== Requirements

Georgia Medicaid for Workers with Disabilities (GMWD) offers people with disabilities, who are working, the opportunity to buy health care coverage through the Georgia Medicaid Program.
This Program became effective March 2008.

== Basic Considerations

The “Ticket to Work and Work Incentives Improvement Act of 1999” (TWWIIA) enacted on December 17, 1999 provided states with new options for making it possible for people with disabilities to join, or remain in, the workplace without fear of losing their Medicare and Medicaid coverage.
Georgia elected to provide Medicaid coverage in the “Basic Coverage Group” of TWWIIA.
Under this group, Medicaid can cover individuals at least 16, but less than 65 years of age, who except for earned income would be eligible to receive SSI.

This program provides Medicaid coverage to workers with disabilities who are employed but are no longer eligible for SSI due to increased earnings.
There is no requirement other than an individual must have at one time been a recipient of SSI or SSA disability.
However, if an individual was not a recipient of SSI or SSA disability, a disability determination must be completed.
Other GMWD non-financial and financial eligibility criteria are based on similar ABD Medicaid Categories.

=== Eligibility Requirements

To qualify for GMWD, an individual must meet all of the following criteria:

* Have a disability that meets Social Security Administration's standards
* Have disability income between $600.00-$699.00/month
* Be employed and receiving compensation
* Be a Georgia resident
* Be at least 16 years of age, but less than the age of 65
* Have countable income less than the Medicaid Cap (300 percent of the FPL)
* Have countable resources less than $4000.00 for an individual, $6000.00 for couple

=== Premium Costs

GMWD participants may be required to pay a monthly premium.
Premium amount is based on the individual's age and total countable income.

=== Applying for GMWD

Application may be submitted in the following ways:

* Online at https://www.gateway.georgia.gov/[www.gateway.georgia.gov]
* Telephonically by calling 1-877-423-4746
* Paper application by mail, fax, or in person at the local DFCS office.
https://www.dfcs.dhs.georgia.gov/[www.dfcs.dhs.georgia.gov]

Individuals approved for GMWD may be required to pay a monthly premium.
GMWD premiums are paid to PSI, Inc., and are due one month prior to the month of coverage.
The address for payment of a GMWD premium is P.O. Box 162348 Atlanta, Ga.
30348-2348.
