= Appendix A1 ABD Financial Limits 2012
:chapter-number: Appendix A1
:effective-date: N/A
:mt: MT-45
:policy-number: Appendix A1
:policy-title: ABD Financial Limits
:previous-policy-number:

.CHART A1.1 - ABD MEDICAID RESOURCE LIMITS
|===
| Type Limit | Individual Limit | Couple Limit | LA-D Individual With a Community  Spouse | Effective Date

| SSI/LA-D
^| $2000
^| $3000
>| N/A
>| 7-88

| AMN
^| $2000
^| $4000
>| N/A
>| 4-90

| QMB/SLMB/ QI-1
^| $6940
^| $10,410
>| N/A
>| 1-12

| QDWI
^| $4000
^| $6000
>| N/A
>| 1-89

| Spousal Impoverishment
^| N/A
^| N/A
| $113,640 + 2000 =

$115,640.00
>| 1-12
|===

.CHART A1.2 - ABD MEDICAID NET INCOME LIMITS (GROSS - $20)
|===
| Type Limit | LA | Individual Limit | Couple Limit | Effective Date

^| *AMN*
^| All
^| $317
^| $375
| 10-90

.4+^| *FBR*

*(SSI Limit)*
^| A
^| $698
^| $1048
.4+^| 1-12

^| B
^| $465.34
^| $698.00

^| C
^| $698
^| N/A

^| D
^| $30
^| N/A

^| *Medicaid CAP*
^| D
^| $2094
^| $4188
| 1-12

.3+| *QDWI*
^| A
^| $3789
^| $5109
.3+a|
3-12

NOTE: Effective 3-98, ISM no +
longer applies to this COA +
eliminating LA-B.

^| C
^| $3789
^| N/A

^| D
^| $3789
^| N/A

^| *QMB*
^| A
^| $931
^| $1261
| 4-12

^| *SLMB*
^| A
^| $1117
^| $1513
| 4-12

^| *QI-1*
^| A
^| $1257
^| $1703
| 3-12
|===

.CHART A1.3 - TRANSFER OF RESOURCE PENALTY DETERMINATION
[cols=3*]
|===
| Averaging Nursing Home Private Pay Billing Rate
| $4988.33
^| 4-12
|===

.CHART A1.4 - PRESUMED MAXIMUM VALUE (PMV) OF ISM AND LIVING ALLOWANCE TO EACH INELIGIBLE CHILD
|===
| Income Limit | PMV for an Individual | PMV for a Couple | Living Allowance | Effective Date

^| *AMN*
^| $252.66
^| $369.33
>| $337.00
>| 1-12

^| *FBR*
^| $256.66
^| $369.33
>| $337.00
>| 1-12

^| *QMB*
^| N/A
^| N/A
>| $427.00
>| 4-12

^| *SLMB*
^| N/A
^| N/A
>| $511.00
>| 4-12

^| *QI-1*
^| N/A
^| N/A
>| $574.33
>| 3-12
|===

.CHART A1.5 - SUBSTANTIAL GAINFUL ACTIVITY
|===
| Category | Income Limit | Effective Date

^| Non-Blind individuals
^| $1010
.2+^| 1-12

^| Blind individuals
^| $1690
|===

.CHART A1.6 – BREAK-EVEN POINTS
[cols=6*]
|===
.2+h| Living Arrangement
2+h| Earned Income
2+h| Unearned Income
.2+h| Effective Date

^h| Individual
^h| Couple
^h| Individual
^h| Couple

| *A*
^| $1271
^| $1873
^| $603
^| $904
.2+^| 1-06

| *B*
^| $869
^| $1271
^| $402
^| $603

| *D*
^| $145
^| $205
^| $50
^| $80
^| 7-88
|===

.CHART A1.7 – MONTHLY AVERAGED MEDICAID RATES FOR KATIE BECKETT
|===
| Level of Care | Monthly Amount | Effective  Date

^| Skilled Nursing Facility
^| $4332.00
.2+| 05/12

^| ICF/MR
^| $8895.00
|===

[.text-center]
*A1.8 – MEDICARE EXPENSES*

Medicare Part B Premium rate: $99.90 (effective 1-12).

Medicare Part D Base Premium rate: 31.18 (effective January 2012)

.CHART A1.9 - PERSONAL NEEDS ALLOWANCES (PNA) FOR AN LA-D RECIPIENT
|===
| IF the LA-D Recipient is 2+| THEN use the following as the PNA in the Patient Liability/Cost Share Budget:

| an individual in a nursing home or Institutionalized Hospice
^| $50
>| Effective 7-06

| a VA pensioner or his/her surviving spouse in a nursing home who has dependents
^| $50
>| Effective 7-06

a|
a VA pensioner or his/her surviving spouse in a nursing home who has no dependents

NOTE: The VA check for these individuals is reduced to the amount of the PNA, regardless of other income.
^| $90
^| Effective 1-92

(Effective 1-93 for the Surviving Spouse)

| an individual in CCSP
2+| the current amount of the Individual FBR for LA-A

| an individual in ICWP
2+| the current amount of the Community Spouse Maintenance Need Standard

| an individual in NOW/COMP
2+| the current Medicaid Cap
|===

.CHART A1.10 - NEED STANDARDS FOR DIVERSION OF INCOME TO A COMMUNITY SPOUSE OR DEPENDENT FAMILY MEMBER IN A PATIENT LIABILITY/COST SHARE BUDGET
|===
| Diversion Standard | Amount | Effective Date

| Community Spouse Maintenance Need Standard
^| $2841.00
^| 1-12

| Dependent Family Member Need Standard
^| $1892
^| 4-12
|===

|===
| HOUSEHOLD SIZE | 100% | 135% | 150% | EFF.
DATE

^| 1
| $11,170.00
| $15,080.00
^| $16,755.00
.5+^| 2012

^| 2
| 15,130.00
| 20,426.00
^| 22,695.00

^| 3
| 19,090.00
| 25,772.00
^| 28,635.00

^| 4
| 23,050.00
| 31,118.00
^| 34,575.00

^| 5
| 27,010.00
| 36,404.00
^| 40,515.00
|===

The FPL (100% level) is increased by $3,960 for each additional person in the household.

.CHART A1.12 – COSTS AND GUIDELINES FOR RECEIPT OF MEDICARE PART D - LOW INCOME SUBSIDY
|===
| | Group 1 | Group 2 | Group 3 | Eff.  Date

^| *Resource Limit*
^| None
^| Non Q Track Individual -

$8,440

Non Q Track Couple -

$13,410
| Individual - $12,910 Couple - $25,010
.6+| 2012

^| *Income Limit*
^| Full

Medicaid
^| Q Track

or

Less than 135% of FPL
^| Less than 150% of FPL

| *Monthly*

*Premium*
^| $0
^| $0
^| Sliding Scale

| *Deductible*

*Per Year*
^| $0
^| Up to $53.00
^| Up to $62.00

| *Coinsurance*

*up to $3600 Out of Pocket*
| $1.10 -

$3.30

Copay
^| $2.60 - $6.50 Copay
^| 15% Coinsurance

| *Catastrophic*

*5% or $2/$5 Copay*
^| $0
^| $0
^| $2.60 - $6.50 Copay
|===

[.text-center]
*Low-Income Part D Premium Subsidy Amount* +
2010 – 29.62 +
2011 – 32.83 +
2012 – 31.18

[.text-center]
*A1.13 – Medically Needy Mileage Re-imbursement Rate* +
48.5 cents per mile – 9/10/05 – 12/31/05 +
44.5 cents per mile – 1/1/06 – 1/31/07 +
48.5 cents per mile – 2/1/07 – 03/31/08 +
50.5 cents per mile – 4/1/08 – 7/31/08 +
58.5 cents per mile – 8/1/08 – 12/31/08 +
55 cents per mile – 1/1/09 – 12/31/09 +
50 cents per mile – 1/1/10 – 12/31/2010 +
51 cents per mile – 01/01/11 – 04/16/2012 +
55.5 cents per mile – 04/17/2012 – to present
