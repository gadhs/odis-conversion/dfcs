= 245i Instructions for SMEU Request Form
:form-number: 245i

== Instructions for Form 245 SMEU Cover Letter

*Purpose:*

* Provide information on the SMEU packet submission including that which is required to correctly route communications to the Field and for the SMEU team to make a disability determination.

== Distribution:

* This form is submitted as part of the SMEU packet and a copy is uploaded to the digital case file.

== Instructions:

* This form should be completed thoroughly and accurately by the DFCS Case Manager.
* Do not give to AR/RP to complete.

|===
| Field | Instructions

| Date Submitted
| Enter the date the full packet was sent to Medicaid Policy Unit's SMEU staff.

| Date copy of this 245 uploaded to Documents
| Form 245 must be uploaded to Gateway as part of the digital case record.
Enter the date this was completed.
|===

Section: CASE MANAGER INFORMATION

* Complete all Fields in this section.
* This section provides routing information relating to the case manager.

[cols=2*]
|===
| 1. Case Mgr Name
| Enter case manager's full name.

| 2. Email
| Enter case manager's full email address.

| 3. Supervisor Name
| Enter supervisor's full name.

| 4. Email
| Enter supervisor's full email address.

| 5. Admin Name
| Enter administrator's full name.

| 6. Email
| Enter administrator's full email address.

| 7. Case Mgr District
| Enter district or special team name to which the case manager is assigned.
_Options: 1, 2, 3, 4, 5, 6, 7, Pathways, RSM, Sr SNAP, State Office_
|===

Section: CLIENT INFORMATION

* Complete all Fields in this section.
* This section provides information relating to the applicant.

[cols=2*]
|===
| 8. Case Number
| Enter the case number of case pending for SMEU decision.

| 9. Client ID
| Enter client ID of the applicant.

| 10.
Client First Name
| Enter client first name as it appears in Gateway.

| 11.
Client Last Name
| Enter client's last name as it appears in Gateway, including spaces and punctuation.

| 12.
Client DOB
| Enter client's date of birth.

| 13.
County of Residence
| Enter the name of the county where the client lives.

| 14.
Is a Hearing Pending?
| Enter “Yes” or “No”

| 15.
If Yes, Date
| Enter the date of the scheduled hearing.
If not yet scheduled, enter NA and add a note in Additional Notes section.
|===

Section: APPLICATION INFORMATION

* Complete all Fields in this section.
* This section provides information relating to the disability start date.

[cols=2*]
|===
| 16.
The application being used to protect the application date was filed for (Pick one)
a|
* Eligibility determinations require the use of protected months.
Applications for SSI and Medicaid are both considered when determining the allowed protected months.
* Enter the type of application that allows us to consider eligibility for the date of disability being requested.

_Note:_

* _This may not be the most recent DFCS Medicaid application._

| 17.
The date of this application
a|
* Enter the date of the SSI or DFCS Medicaid application that is used to protect the date of the disability request from question

#16.

| 18.
Date requesting Disability start (Consider ALL protected applications & the 3 months prior)
a|
* Enter the date the SMEU team should consider as the date disability began.
* This should not be earlier than the 3rd month prior to the application date in Field 17.

| 19.
SSI Application Status: (Check all that apply)
a|
* Mark an X next to all options that apply.
More than one option may be selected.

.6+>| _Approved Denied, No Appeal Pending Denied, Appeal Pending_

_Not Financially Elig for SSI_

_Deceased_

_Has Not Applied_
a|
* Enter “X” if client has applied for and been approved for SSI.

a|
* Enter “X” if client has applied for and has been denied for SSI and there is no hearing/appeal pending.

a|
* Enter “X” if client has applied for and has been denied for SSI and there is a hearing/appeal pending.

a|
* Enter “X” if client has income that places them over the income limit for SSI.

_Note:_

* _If client has also applied for SSI and been denied, also mark “X” next to Denied._

a|
* Enter “X” if client is deceased.

_Note:_

* _If client had applied for SSI and has a decision, also mark “X” next to appropriate status (Approved, Denied)._

a|
* Enter “X” if client has not applied for SSI.

_Note:_

* _If client has not applied because they are deceased, also mark “X” next to deceased._
* _If client did not apply because they are not financially eligible for SSI, also mark “X” not to Not Financially Elig for SSI._
|===

Section: A complete packet MUST include all the following, please submit in the order below & mark each item included.

* Complete all Fields in this section.
* This section provides information relating to mandatory documents.

[cols=2*]
|===
| 245 SMEU Cover letter
a|
* This item must be submitted as part of the SMEU packet.
* Enter X next to confirm it is being included in the submission.

| 188 Social Data Report – should be completed by DFCS Case

Manager
a|
* This item must be submitted as part of the SMEU packet.
* Enter “X” on this line to confirm it is being included in the submission.

| Medical Records – should include records to prove disability in the month

requested
a|
* This item must be submitted as part of the SMEU packet.
* Enter “X” on this line to confirm it is being included in the submission.
|===

Section: A packet MAY include one or more of the following.
Please review your packet & mark each item included.

* Complete relevant fields in this section.
* This section provides information relating to conditional documents.
These documents are submitted only when the circumstance is present.
This section should be completed to communicate to the SMEU team to look for these additional items.

[cols=2*]
|===
| 90-day post stroke or heart attack follow up
a|
* This item is required if the client reports disability based on stroke or heart attack.
* Enter “X” on this line to confirm it is being included in the submission.

| 526 - for EMA
a|
* This item is required if the client is being considered for EMA.
* Enter “X” on this line to confirm it is being included in the submission.

| Death Certificate
| This item is not required, however, if it is available, include with the SMEU packet and enter “X” on this line to confirm it is being

included in the submission.

| 115 – if alleging blindness
| This item is required if the client reports blindness.

Enter “X” on this line to confirm it is being included in the submission.

| Other:
| Additional relevant medical documents may be submitted.

If the document being submitted is not listed anywhere else on the Form 245, enter “X” on this line to confirm it is being included

in the submission and write in the document title or description.
|===

Section: Additional Notes:

* Complete relevant Fields in this section.
* This section assists in clarifying information above.

[cols=2*]
|===
| Notes
| Enter any additional information that is relevant to making the

disability determination.
|===
