= 2653 Prospective Budgeting
:chapter-number: 2600
:effective-date: June 2020
:mt: MT-60
:policy-number: 2653
:policy-title: Prospective Budgeting
:previous-policy-number: MT 40

include::ROOT:partial$policy-header.adoc[]

== Requirements

Prospective budgeting uses a representative amount of income received and dependent care expenses incurred to determine the Family Medicaid Assistance Unit's (AU's) or Budget Group's (BG's) eligibility and benefit amount.

== Basic Considerations

Representative income is the amount that best represents what the AU or BG will receive on an ongoing basis.

Representative dependent care expenses are the amounts that best represent what the AU or BG will incur on an ongoing basis.

=== Representative Income and Expenses

When the amount of the income or dependent care expense is stable (i.e., does not change from one period to the next), the stable amount is the representative amount.

When income or dependent care expenses vary, representative income and dependent care expenses are calculated as follows:

* the representative income or expense is based on available information and/or verification from the AU

* the representative income or expense may be an average of the last month's income or expense, or it may be for a specific period determined to be the most representative of the situation.
In certain instances, more or less than one month income or expense is used if one month income is not representative

* periods with little or no income received or expenses incurred are disregarded when determining representative amounts unless they are determined to be representative.

Monthly income and dependent care expenses are determined by multiplying the representative amounts by the following conversion factors:

|===
| Frequency | Conversion Factor

| weekly
| 4.3333

| bi-weekly
| 2.1666

| semi-monthly
| 2

| monthly
| 1
|===

Representative income and dependent care expenses are calculated at the following times:

* at the initial eligibility determination

* at periodic reviews

* when a change of income or dependent care expenses occurs

=== Verification

The best estimate of income used in determining an AU's benefit amount is based on verification of at least one full month's stable income or four current consecutive weeks for fluctuating income.
In some instances, more than one month's income and expenses is not representative of the AU's ongoing situation.
Refer to chart 2653.1, Minimum Verification Requirements.

If the AU does not have the minimum number of pay stubs for income verification because some are missing, use the year-to-date figures if these are displayed on the other pay stubs.
In order to use year-to-date figures, the AU must provide the paycheck stubs for the pay periods immediately prior to and immediately after the missing check stub.

If, because of a new source of income, the AU cannot comply with the minimum verification requirements specified above, obtain verification of all income received from the first receipt of the income to the present.

NOTE: Refer to xref:2051.adoc[Section 2051], Verification, for policy pertaining to acceptable verification sources.

== Procedures

Follow the steps below to determine the AU's representative monthly income and dependent care expenses:

*Step 1* Determine the following:

* the source and type of income or dependent care expense

* the amount of income or dependent care expense and the frequency with which the income is received or the expense is incurred

Verify all income of the BG from the source.
Refer to xref:2051.adoc[Section 2051], Verification.

NOTE: Accept the A/R's statement of dependent care expense unless questionable.
Verification of this expense is not required.

*Step 2* Calculate the representative income and/or dependent care expense.

*Step 3* Convert the representative amount from Step 2 to determine the AU's monthly gross income or dependent care expense.

Document the following:

* the type and source of income and/or dependent care expenses

* verification source

* the frequency and amount of income and/or dependent care expenses used in determining the representative amount(s)

* the reason for determining that income and/or expenses are not representative, if applicable

* calculation of the representative amount of income and/or dependent care expenses, including conversion calculation.

.CHART 2653.1 MINIMUM VERIFICATION REQUIREMENTS
|===
| FREQUENCY OF PAY OR EXPENSE | MINIMUM VERIFICATION REQUIRED

| Weekly, bi-weekly, or semi-monthly (stable or fluctuating income or expenses)
| One month or 4 current consecutive weeks of income or expenses

| Monthly
| 2 months of income or expenses

| Irregular
| 3 months of income or expenses
|===

Use the following chart to calculate monthly income.

.CHART 2653.2 - HOW TO DETERMINE MONTHLY INCOME/EXPENSES
|===
| IF | THEN

| the income is either stable or fluctuating and is received more often than monthly

OR

the expenses are either stable or fluctuating and incurred more often than monthly
| determine a representative amount of income received and/or expenses incurred by computing past, present and/or anticipated income and/or expense amounts that represent regular payments received and/or expenses incurred.

Convert to a monthly amount by using the appropriate conversion factor.

Document the case record.
Explain what income and expenses were used, and why.

| the income is received monthly or less often than monthly

OR

the expenses are incurred monthly or less often than monthly
a|
do not automatically convert the income or expense. +
Determine the best estimate based on the following criteria:

* If stable, budget the actual income received or expense incurred in the month preceding the application or review month.

* If fluctuating, average the income received or expense incurred in the months immediately preceding the application or review month.

* In some instances, use more or less than two months of income and/or expenses if the income received or expense incurred in the two months immediately preceding the application or review month is not representative of the ongoing situation.

* Convert to a monthly amount when income received or expenses incurred by an AU is intended to cover a specified period of time.
To obtain a monthly amount, divide the total income to be received or the total expenses to be incurred during the life of the contract or agreement by the number of months over which the contract or agreement extends.
|===

.CHART 2653.2 (CONT.) - HOW TO DETERMINE MONTHLY INCOME/EXPENSES
|===
| IF | THEN

|
| Document the case record.
Explain what income and/or expenses were used, and why.

| the AU will receive income or will incur expenses, or has received income or has incurred expenses, with no change in the rate at which income has been/will be received or at which expenses have been/will

be incurred
| determine the representative income.
Convert to a monthly amount using the appropriate conversion factors.

| the AU will receive income or will incur expenses, or has received income or has incurred expenses, and the rate at which income will be received or at which expenses will be incurred has changed
| do not convert.
Use the actual and/or representative income/expenses.
Use the actual income/expenses for dates that have already occurred AND representative income/expenses for future dates in the month.

| the AU will receive less than a full month's income, will incur less than a full month's expense, has received less than a full month's income or incurred less than a full month's expenses because of new, interrupted, or terminated income and/or expenses
| do not convert.
Use the actual and/or representative income/ expense.
Use the actual income/expense for dates that have already occurred AND the representative

income/expense for future dates in that month.

| Child support income is received through the Division of Child Support Services (DCSS)
a|
Determine the monthly amount of income using the last three months of child support income received, if representative.

If the AU reports a change after representative income has been determined, use anticipated income to determine the best estimate for future months.

NOTE: Any payments posted within the last two days of the month are considered received in the following month.
|===

If an A/R is receiving income or incurring expenses from more than one source, each source is treated separately in determining if income/expense is converted to a monthly amount.

A full month's income, if earned, is defined as receipt of or expected receipt of income at each regular pay date during a calendar month.

A full month's income, if unearned, is defined as receipt of or expected receipt of income intended to cover an entire calendar month.

A full month's expense is defined as an expense intended to cover an entire calendar month.
