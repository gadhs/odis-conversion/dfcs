= 2349 Personal Care Contracts
:chapter-number: 2300
:effective-date: April 2020
:mt: MT-59
:policy-number: 2349
:policy-title: Personal Care Contracts
:previous-policy-number: MT 34

include::ROOT:partial$policy-header.adoc[]

== Requirements

Personal Care Contracts provide a way for an elder individual to share assets with a family member while also spending down their estate.
Payments made under personal care contracts may be considered an uncompensated transfer of assets in the eligibility determination process.

== Basic Considerations

Services contracts, sometimes referred to as a personal care contracts, care agreement, etc.
will be referred to as personal care contracts in this section.
Personal care contracts allow an individual to pay another person to provide personal care.
Examples of personal care include, but are not limited to, grocery shopping, housekeeping, cooking and financial management, that an individual is no longer able to perform for him/herself.

== Procedures

When a personal care contract is presented as the basis for a transfer of assets you must first, determine if a valid agreement exists.
Secondly, if a valid agreement exists, you must determine whether adequate compensation in the form of services was provided.

*A Personal Care Contract is valid only if ALL of the following are met:*

* The personal care contract must be executed prior to the provision of services.
The contract cannot be applied retroactively to pay for services that were provided prior

to the agreement.

* The personal care contract must be in writing, signed, and dated by each party.
The contract must be notarized.
* The personal care contract must have been made by the applicant/recipient or a legally authorized representative such as an agent under a power of attorney, guardian or conservator.
If a representative signs the contract on behalf of the applicant/recipient of the services, that representative *may not* also be a beneficiary of the agreement.
* The personal care contract must specify the type, frequency, and number of hours spent for each service or assistance to be provided in exchange for the payment.
The terms must be specific and verifiable.
* These services must be provided at market rate.

* The personal care contract must provide for payment upon rendering the services or assistance, or within thirty (30) days thereafter and;
must be supported by evidence that payments were made in accordance with the agreement.
 ** Any payment(s) made prior to the date the contract was signed by all parties is considered an uncompensated transfer.
* The caregiver cannot be the spouse or parent of the applicant/recipient.
* The applicant/recipient or a legally authorized representative must have the power to modify, revoke or terminate the agreement.

A Personal Care Contract that fails to contain any of the mandatory provisions is considered to be invalid for Medicaid eligibility purposes.
Payments that are not made in accordance with a valid personal care contract are considered a transfer without compensation.

*If a valid personal care contract exists, determine whether or not adequate compensation in the form of services or assistance was provided.*

* Adequate compensation shall be measured against rates paid in the open market for the services or assistance actually provided.
If the services or assistance require extraordinary skill, the caregiver must possess the required skill, experience or expertise and licensing.
These services or assistance will be valued in accordance with similar services in the community.
* Adequate compensation shall not be met if the service(s) or assistance duplicates services that another party is being paid to provide or which another party is legally responsible to provide (i.e. the nursing facility, case manager).
* Adequate compensation shall not be met when the service(s) is a type of service or assistance that is not commonly paid for, including services that a relative would normally provide out of love and affection, like visiting the individual and updating other relatives on the individual's condition.

If payments are made for compensated services or assistance but the payments exceed the allowed market value of similar services or assistance, then the amount of the payment is a transfer without compensation.

The contracted service(s) or assistance must be provided within thirty (30) days from the date of payment.
Any payment for services that have not been performed within thirty (30) days is considered an uncompensated transfer of assets.

The Personal Care Contract ceases upon the death of the applicant/recipient or upon admission to a nursing facility.
