= Appendix A1 ABD Financial Limits 2001
:chapter-number: Appendix A1
:effective-date: N/A
:mt: MT-1
:policy-number: Appendix A1
:policy-title: ABD Financial Limits
:previous-policy-number:

.CHART A1.1 - ABD MEDICAID RESOURCE LIMITS
|===
| Type Limit | Individual Limit | Couple Limit | LA-D Individual  With a Community Spouse | Effective Date

| SSI
^| $2000
^| $3000
| N/A
^| 7-88

| AMN
^| $2000
^| $4000
| N/A
^| 4-90

| QMB/SLMB/QIs/ QDWI
^| $4000
^| $6000
| N/A
^| 1-89

| Spousal Impoverishment
^| N/A
^| N/A
| $87,000 + 2000 =

$89,000.00
^| 1-01
|===

.CHART A1.2 - ABD MEDICAID INCOME LIMITS
|===
| Type Limit | LA | Individual Limit | Couple Limit | Effective Date

^| *AMN*
^| All
^| $317
^| $375
^| 10-90

.4+^| *FBR*

*(SSI Limit)*
^| A
^| $530 / $531
^| $796/ $796
.4+| 1-01/8-01

^| B
^| $353.34/ $354
^| $530/ $530.67

^| C
^| $530/ $531
^| N/A

^| D
^| $30
^| N/A

^| *Medicaid CAP*
^| D
^| $1590/ $1593
^| $3180/ $3186
^| 1-01/8-01

.3+| *QDWI*
^| A
^| $1392/$1432
^| $1876/$1935
.3+a|
3-00/3-01

NOTE: Effective 3-98, ISM no +
longer applies to this COA eliminating LA- +
B.

^| C
^| $1432
^| N/A

^| D
^| $1432
^| N/A

^| *QMB*
^| A
^| $696/$716
^| $938/$968
^| 4-00/4-01

^| *SLMB*
^| A
^| $836/$859
^| $1126/$1161
^| 4-00/4-01

^| *QI-1*
^| A
^| $940/$967
^| $1267/$1307
^| 3-00/3-01

^| *QI-2*
^| A
^| $1218/$1253
^| $1642/$1694
^| 3-00/3-01
|===

.CHART A1.3 - TRANSFER OF RESOURCE PENALTY DETERMINATION
[cols=3*]
|===
| Averaging Nursing Home Private Pay Billing Rate
| $2930/$3042
| 4-96/4-01
|===

.CHART A1.4 - PRESUMED MAXIMUM VALUE (PMV) OF ISM AND LIVING ALLOWANCE TO EACH INELIGIBLE CHILD
|===
| Income Limit | PMV for an Individual | PMV for a Couple | Living Allowance | Effective Date

^| *AMN*
^| $196.66/$197
^| $285.33
^| $266/$266
^| 1-01

^| *FBR*
^| $196.66/$197
^| $285.33
^| $266/$266
^| 1-01

^| *QDWI*
^| N/A
^| N/A
^| $625.33/$651.67
^| 3-00/3-01

^| *QMB*
^| N/A
^| N/A
^| $313/$329.34
^| 4-00/4-01

^| *SLMB*
^| N/A
^| N/A
^| $375.33/$393.67
^| 4-00/4-01

^| *QI-1*
^| N/A
^| N/A
^| $422.33/$443
^| 3-00/3-01

^| *QI-2*
^| N/A
^| N/A
^| $547.33/$571.34
^| 3-00/3-01
|===

QI-2 Refund Amount is $3.09 effective 01-01.

Medicare Part B Premium rate is $50.00 effective 01-01.

.CHART A1.5 - PERSONAL NEEDS ALLOWANCES (PNA) FOR AN LA-D RECIPIENT
|===
| IF the LA-D Recipient is 2+| THEN use the following as the PNA in the  Patient Liability/Cost Share Budget:

| an individual in a nursing home
>| $30
^| Effective 1-92

| a VA pensioner or his/her surviving spouse in a nursing home who has dependents
>| $30
^| Effective 1-92

| a VA pensioner or his/her surviving spouse in a nursing home who has no dependents
>| $90
^| Effective 1-92

a|
NOTE: The VA check for these individuals is reduced to the amount of the PNA, regardless of other income.
|
^| (Effective 1-93 for the Surviving Spouse)

| an individual in CCSP
2+| the current amount of the Individual FBR for LA-A

| an individual in ICWP
2+| the current amount of the Community Spouse Maintenance Need Standard
|===

.CHART A1.6 - NEED STANDARDS FOR DIVERSION OF INCOME TO A COMMUNITY SPOUSE OR DEPENDENT FAMILY MEMBER IN A PATIENT LIABILITY/COST SHARE BUDGET
|===
| Diversion Standard | Amount | Effective  Date

| Community Spouse Maintenance Need Standard
^| $2175
^| 1-01

| Dependent Family Member Need Standard
^| $1472
^| 1-01
|===
