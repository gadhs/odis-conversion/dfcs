= 138 SP Notice of Requirement to Cooperate and Right to Claim Good Cause for Refusal to Cooperate with CSE (Spanish)
:form-number: 138 SP
:lang: es

== Departamento de Servicios Humanos de Georgia Notificación de requerimiento de colaboración y derecho a alegar causa justa de negativa a la colaboración para exigir el cumplimiento de la obligación de suministrar manutención infantil y requerimientos de recursos de terceros

== Beneficios derivados del cumplimiento de la obligación de suministrar manutención infantil

Su colaboración en el proceso destinado a exigir el cumplimiento de la obligación de suministrar manutención infantil puede ser valioso para usted y para su hijo ya que puede permitir:

* Encontrar al padre ausente.
* Establecer legalmente la paternidad de su hijo.
* Recibir pagos de manutención infantil que puedan facilitarle más dinero que si recibe Asistencia Temporaria para Familias Necesitadas (TANF, por sus siglas en inglés).
* Adquirir seguro médico privado a través del padre ausente, y
* Adquirir derecho a la obtención de futuros beneficios derivados de la Seguridad Social, beneficios para veteranos de guerra y otros beneficios.

== Colaboración con la DFCS y la OCSS

La ley requiere que usted colabore con la División de Servicios para Familias y Niños (DFCS, por sus siglas en inglés) y Oficina de Servicios de Manutención Infantil (OCSS, por sus siglas en inglés) para obtener las prestaciones que le corresponden a usted y a los hijos por los cuales solicita la TANF, excepto que exista causa justa para no prestar dicha colaboración.

Al colaborar con la DFCS o la OCSS, usted debe realizar uno o más de los siguientes actos:

* Consignar el nombre del padre ausente del hijo por el cual solicita la TANF y /o Medicaid.
* Proporcionar información para localizar al padre ausente.
* Ayudar a determinar quién es legalmente el padre si se trata de un hijo extramatrimonial.
* Aceptar que se le realice un análisis de sangre si la persona que usted indicó como padre niega su paternidad.
* Ayudar al estado a obtener el dinero adeudado a usted y/o al hijo que recibe la TANF.
* Proporcionar información acerca del seguro médico que el padre ausente posee sobre su hijo.

Usted debe presentarse en las oficinas de la DFCS, la OCSS o ante el tribunal para firmar documentación o proporcionar la información necesaria.

== Causa Justa

Es posible que usted posea una causa justa para no desear colaborar con la OCSS para percibir manutención infantil o cobertura médica para su hijo.
Es posible que no tenga que colaborar si usted considera que no resulta en beneficio de su hijo y si puede probarlo.
Si desea alegar causa justa, es necesario que se lo mencione a su trabajador.
Puede hacerlo en cualquier momento.

== Si usted no colabora y no posee una causa justa

* No cumplirá los requisitos para recibir la TANF para usted y para su hijo.
* Su hijo puede aún cumplir los requisitos para recibir Medicaid.

== Razones de causa justa

Puede alegar causa justa en razón de cualquiera de los siguientes motivos:

* Su colaboración podría provocar daños físicos o emocionales graves a su hijo o usted.
* Su hijo nació como resultado de violación o incesto.
* Se encuentra en trámite un proceso judicial para la adopción de su hijo.
* Un organismo colabora con usted para decidir si dar a su hijo en adopción.

== Para probar causa justa usted debe:

* Proporcionar a la DFCS la información necesaria para decidir si existe causa justa para negarse a colaborar.
Si teme sufrir daños físicos y no puede reunir las pruebas, la DFCS podrá igualmente efectuar una determinación de causa justa.
* Brindar pruebas a la DFCS dentro de los 20 días de haber alegado causa justa.
La DFCS le brindará más tiempo sólo si usted posee inconvenientes para obtener las pruebas.

La DFCS puede eximirlo de la obligación de colaborar en cuanto a la información que usted brinde.
La DFCS puede también solicitarle que proporcione mayor información.
La DFCS no contactará al padre ausente sin avisarle a usted al respecto.

*NOTA:* Si usted solicita la TANF, su solicitud no será aprobada hasta que proporcione pruebas a la DFCS de causa justa o la información que la DFCS necesita para investigar su situación.

*Formulario 138SP* (Rev. 12/2008)

== EJEMPLOS DE PRUEBA DE CAUSA JUSTA

* Certificado de nacimiento, registros médicos o legales que demuestren que el niño nació como consecuencia de violación o incesto.
* Documentación judicial o legal que demuestre que se ha iniciado el proceso de adopción.
* Servicios judiciales, médicos, penales, de protección de menores, servicios sociales, registros psicológicos o legales que demuestren que el padre ausente puede lastimarlo a usted o su hijo.
* Registros médicos o declaraciones escritas de un profesional de salud psíquica que demuestren la historia y estado actual de su salud emocional y/o la de su hijo.
* Declaración escrita de un organismo público o privado que demuestre que usted recibe asistencia para decidir si dar o no a su hijo en adopción.
* Declaraciones juradas de amigos, vecinos, sacerdotes, trabajadores sociales o profesionales médicos que sepan por qué usted posee causa justa.

Si necesita ayuda para obtener cualquiera de los documentos, consulte con su trabajador.

== Normas sobre suministro de manutención infantil

Si usted recibe la TANF, usted proporciona al estado de Georgia, por ley, cualesquiera derechos que usted posea a recibir manutención infantil.
Una vez que se establece la orden judicial, el padre ausente deberá pagar manutención infantil a través de la OCSS.
Luego de que se establezca la orden judicial, usted deberá informar todo el dinero que perciba directamente del padre ausente.
Usted deberá, asimismo, colaborar para establecer la paternidad de su hijo y colaborar con la OCSS para establecer una orden de suministro de manutención infantil.
Si usted no colabora y no posee justa causa para ello, es posible que no reúna los requisitos para obtener la TANF.

Si usted recibe la TANF y el padre ausente paga manutención infantil a través de la Oficina de Servicios de Manutención Infantil (OCSS), es probable que usted NO reciba el monto total del pago de manutención infantil.
En cambio, es posible que usted reciba un pago “de diferencia”.
Todos los pagos por manutención infantil efectuados por un padre ausente que superen el monto "de diferencia" serán retenidos por la OCSS y se utilizarán para devolver los fondos de la TANF que usted recibió.
_**El encargado de su caso TANF puede explicarle las diferencias y los procedimientos de pago.**_

Si se cierra su caso TANF, los pagos de manutención infantil le serán enviados hasta el monto de la obligación mensual actual del padre ausente.
Los montos de manutención infantil pagados que superen la obligación actual serán retenidos por el estado para devolver las asignaciones anteriores de TANF que usted haya recibido.
Una vez devueltas las asignaciones TANF pasadas, se le enviará la totalidad del pago por manutención infantil efectuado por el padre ausente.

Si su caso TANF se cierra y _luego_ se abre nuevamente, los pagos por manutención infantil retroactivos que se le adeuden serán asignados al Estado hasta el monto de la totalidad del dinero TANF que usted haya percibido.
Cuando se haya devuelto la Asistencia Pública No Reembolsada (UPA, por sus siglas en inglés), usted comenzará a percibir cualesquiera pagos adeudados.

Si usted percibe pagos de manutención infantil a los cuales no posee derecho, es posible que usted deba efectuar devoluciones al estado.
El estado lo notificará acerca del monto del pago en exceso y acerca del plazo para efectuar la devolución.

La OCSS puede revisar la decisión de la DFCS sobre causa justa en su caso.
Si usted solicita una audiencia acerca de la decisión, la OCSS podrá participar de la audiencia.

Si posee causa justa para no colaborar, la OCSS no efectuará intentos por establecer la paternidad o percibir manutención infantil.

*Declaro haber leído la presente notificación acerca de mis derechos a alegar causa justa y no colaborar para establecer Ia paternidad o percibir manutención infantil por parte del padre ausente.*

Firma del Solicitante/Destinatario

Fecha

*Declaro haber proporcionado copia de la presente notificación al solicitante/destinatario de TANF o Medicaid.*

Firma del Encargado del Caso

Fecha

*Formulario 138* (Rev. 12/2008) Reverso
