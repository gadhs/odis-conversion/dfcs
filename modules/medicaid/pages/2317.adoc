= 2317 Homeplace: Family Medicaid
:chapter-number: 2300
:effective-date: April 2020
:mt: MT-59
:policy-number: 2317
:policy-title: Homeplace: Family Medicaid
:previous-policy-number: MT 3

include::ROOT:partial$policy-header.adoc[]

== Requirements

A Family Medicaid applicant/recipient's (A/R's) homeplace, regardless of value, is an excluded resource in its entirety.

== Basic Considerations

A homeplace is property in which an A/R has an ownership interest and that serves as the principal place of residence of the A/R. +
Only one homeplace per AU is exempt.
The homeplace includes the following:

* the house, building or other shelter in which the A/R lives
* the land on which the homeplace is located (home plot)
* all land contiguous to the home plot
+
[caption=Exception]
NOTE: If the A/R owns a homeplace and s/he and another individual jointly own land contiguous to the homeplace property, the jointly owned contiguous property is not considered part of the homeplace and is therefore not excluded. +
*NOTES:* Easements and public rights of way, including roads do not separate the property of the homeplace. +
Surrounding property separated by property owned by individuals other than the A/R is not considered part of the homeplace and is therefore not excluded.

* All other outbuildings located on the homeplace property.
+
[caption=Exception]
NOTE: Buildings on the homeplace such as other houses or businesses, which are clearly not part of the home and its outbuildings, are not excluded.
Only one home and its outbuildings are exempt.

The original homeplace may have been expanded by purchase of property contiguous to the homeplace, therefore, more than one deed may exist for the homeplace.
These multiple deeds are considered a single homeplace provided all deeds are in the name of the A/R and/or the A/R's spouse.

The homeplace may continue to be excluded from resources when the A/R is absent from the home for any of the following reasons:

* illness
* vacation
* uninhabitability caused by natural disaster or other casualty
* employment
* training

Land or a lot purchased with the intent to build a home is excluded only if the AU currently does not own a home.

A partially built home is excluded only if the AU currently does not own a home.

Money derived from the sale of a homeplace must be reinvested in another homeplace within six months.
If a new homeplace is not purchased, the proceeds from the sale are considered a resource and are counted in the eligibility determination.
If a new homeplace is purchased, but the purchased price is less than the proceeds from the sale of the previous homeplace, the unspent proceeds are considered a resource and are counted in the eligibility determination.
