= 2135 Hospice Care
:chapter-number: 2100
:effective-date: November 2023
:mt: MT-71
:policy-number: 2135
:policy-title: Hospice Care
:previous-policy-number: MT 58

include::ROOT:partial$policy-header.adoc[]

== Requirements

Hospice Care is a class of assistance (COA) that provides Medicaid to cover care for terminally ill individuals.

== Basic Considerations

To be eligible under the Hospice Care class of assistance an A/R must meet the following conditions:

* The A/R has a medical prognosis of six months or less life expectancy.
* The A/R is receiving hospice care services from an approved hospice care provider.
* The A/R meets the Length of Stay (LOS) and Level of Care (LOC) basic eligibility criteria.
* The A/R meets all other basic and financial eligibility criteria

Hospice care services are provided to the A/R by a Medicaid hospice agency.
The A/R may reside at home or in a nursing home.

Hospice care services include but are not limited to the following:

* nursing home
* medical social services
* physician services
* counseling services
* respite care
* home health aide services

NOTE: DMA only reimburses for medical services provided by the hospice care agency.
These recipients receive a Medicaid card that identifies them as hospice care recipients, with a notation to medical service providers that all claims must be submitted through the hospice agency.
If the A/R has prescription needs which are not covered through the Hospice provider and are not related to the Hospice diagnosis, the authorizing physician must submit to DCH a statement containing the Hospice diagnosis, what the unrelated drug is and why it is necessary.
This statement should be sent for approval to: +
DCH Pharmacy Services Unit +
2 Martin Luther King Jr.
Drive SE +
East Tower, 19^th^ Floor +
Atlanta, Georgia 30303

NOTE: All SSI recipients receiving Institutionalized Hospice must be entered and processed in Gateway.
These cases should be consistent with case management for SSI only nursing home cases.
See *SSI Recipients*, xref:2578.adoc[Section 2578].

There is no patient liability or cost share for A/Rs in a home setting.
However, for Hospice A/Rs in a NH setting, a PL/CS is determined the same as for NH A/Rs.
See *Institutionalized Hospice*, xref:2136.adoc[Section 2136].

== Procedures

Follow the steps below to determine ABD Medicaid eligibility under the Hospice Care COA.

*Step 1* Accept the A/R's Medicaid application

*Step 2* Verify the following through receipt of a Hospice Care Communicator (HCC) from the hospice agency:

* A/R's medical prognosis (life expectancy)
* A/R's (or PR's) election of hospice services
* Date hospice care services began

*Step 3* If the A/Rs income includes SSI and not Institutionalized, STOP, deny application. +
SSI Hospice At Home A/R's claims are directly billed to DCH through the Hospice +
agency.

*Step 4* Conduct an interview.

*Step 5* Determine basic eligibility, including Length of Stay (LOS) and Level of Care (LOC).
Refer to Chapter 2200, Basic Eligibility Criteria.

*Step 6* Determine financial eligibility.

* Refer to Chapter 2500, ABD Financial Responsibility and Budgeting, for procedures on whose resources to consider and the resource limit to use in determining resource eligibility.

* Complete a Medicaid CAP budget to determine income eligibility.
Refer to xref:2510.adoc[Section 2510], Medicaid CAP Budgeting.
If A/R has income over the Medicaid Cap, refer to xref:2407.adoc[Section 2407], Qualified Income Trust.

*Step 7* Approve Medicaid on the system using the Hospice Care COA if the A/R meets all the above eligibility criteria.

For an A/R receiving hospice care services in a nursing home, determine Institutionalized Hospice eligibility.
See xref:2136.adoc[Section 2136], Institutionalized Hospice Care.

*Step 8* Notify the A/R of the case disposition via the system.
Notify the hospice provider of the case disposition using the Hospice Care Communicator (HCC) and enter the hospice agency as an Authorized Representative in the system.

*Step 9* Verify at the following intervals through receipt of an HCC that the hospice care provider has received a signed statement from the A/R electing to continue hospice care services:

* by the end of the first 90-day period of hospice care
* by the end of the second 90-day period of hospice care
* every 60 days thereafter
+
NOTE: Do not approve Medicaid under the Hospice Care COA for any month in which the A/R will not receive hospice services from an approved hospice agency.
If the A/R does not elect to continue hospice services at the intervals specified above, complete a CMD.
Refer to the xref:2052.adoc[Section 2052], Continuing Medicaid Determination.

== Special Considerations

For any month in which an A/R is in Hospice care and another COA such as Hospital or Nursing Home, approve the case on the system under the other COA.
Use Hospice COA when the A/R is in Hospice and not eligible under any other COA for a specific month.

A/Rs may receive Hospice services while receiving Medicaid through EDWP/CCSP.
It is not necessary to terminate the existing EDWP/CCSP case and switch to Hospice.
If A/R was eligible under the Hospice COA, switch to EDWP/CCSP.
The Hospice and EDWP/CCSP eligibility may occur simultaneously in MHN.
