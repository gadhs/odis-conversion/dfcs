= 2001 Computer Matches Overview
:chapter-number: 2000
:effective-date: December 2019
:mt: MT-57
:policy-number: 2001
:policy-title: Computer Matches Overview
:previous-policy-number: MT 01

include::ROOT:partial$policy-header.adoc[]

== Requirements

Applicants and recipients whose resources and income are used to determine eligibility are matched with the files of various governmental agencies.

== Basic Considerations

The applicant's and recipient's (A/R's) primary social security numbers (SSN) are matched with SSNs contained in other governmental agency files.
The information associated with the SSNs is compared.
Discrepancies are identified for follow-up and investigation.

Computer matches through the use of the A/R's SSN are designed to detect income, resources, and to provide other pertinent information required to establish eligibility and benefit levels.

Information obtained from the computer matches is used for the following purposes:

* to verify eligibility of A/Rs,

* to verify the proper amount of benefits,

* to determine whether recipients are receiving the benefits to which they are entitled,

* to obtain information to use to conduct criminal or civil prosecutions based on receipt of benefits to which recipients are not entitled.

Computer matches are accessed via system terminals or through personal computers connected to the system.

Users must have a valid password which is obtained from the State Office.

Unlawful access is prohibited.
A record of all inquiries by password is kept and monitored by the system.
