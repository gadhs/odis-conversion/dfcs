= 2344 Qualified Tuition Savings Program (529 Plans)
:chapter-number: 2300
:effective-date: April 2020
:mt: MT-59
:policy-number: 2344
:policy-title: Qualified Tuition Savings Program (529 Plans)
:previous-policy-number: MT 9

include::ROOT:partial$policy-header.adoc[]

== Requirements

The resource value of a Qualified Tuition Savings Program (529 Plan) is determined by whether an A/R is the donor of the plan or the beneficiary of the plan.

== Basic Considerations

A Qualified Tuition Savings Program (529 Plan) was enacted to allow a tax-advantage to individuals who wish to save for future education.
There are two types of 529 Plans:

* The College Savings Plan is a state-sponsored plan that helps families/individuals save for higher education.
It provides tax-free withdrawals for certain expenses, tax deferral on earnings, professional money management and the flexibility to use the money at any source of higher education.
* The Prepaid Tuition Plan allows the donor to pre-select a higher education institution in advance at today's prices.
These plans usually cover tuition only, not room and board, etc.

With either of the 529 Plans, the donor maintains control over the plan and may cancel or change it.
A change in the investment options for a 529 Plan may be made annually.

== Procedures

The person who is named on the plan as the owner/account holder is considered the donor(s).
If the A/R is the donor of the 529 Plan, consider the plan as a resource to the donor.
Do not count as a resource to the beneficiary.

If the A/R is the beneficiary of the 529 Plan, totally exclude the value of the plan and any disbursements made from the plan.

*Verification and Documentation*

Obtain a copy of the 529 Plan for the file.

If the A/R is the donor, for resource purposes, obtain verification from the source regarding mandatory fees, taxes and/or penalties that would be incurred if the 529 Plan were to be liquidated.
Count the remainder as a resource to the donor.
