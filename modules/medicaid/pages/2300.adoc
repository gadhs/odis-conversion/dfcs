= 2300 ABD Medicaid Resources Overview
:chapter-number: 2300
:effective-date: April 2020
:mt: MT-59
:policy-number: 2300
:policy-title: ABD Medicaid Resources Overview
:previous-policy-number: MT 26

include::ROOT:partial$policy-header.adoc[]

== Requirements

The value of an A/R's or couple's countable resources cannot exceed the appropriate resource limit in order for the A/R or couple to be eligible for ABD Medicaid.

== Basic Considerations

The appropriate resource limit is dependent upon several factors:

* the A/R's class of assistance (COA)
* whether the A/R is living with or has lived with an ineligible spouse or ineligible parent in LA-A, B, or C
* whether an A/R living in LA-D has a spouse living in LA-A or B

To determine an A/R's appropriate resource limit (individual or couple), refer to Chapter 2500, ABD Financial Responsibility and Budgeting.
Refer to Appendix A1 for current individual and couple resource limits for each COA.

Resources are assets.
Assets, with respect to an A/R, includes all income and resources of the A/R and of the A/R's spouse, including income and resources which the A/R or A/R's spouse is entitled to but does not receive because of some action by one or both of them.
Refer to Section 2342 for explanation of those actions.

Resources may include cash, other personal property and real property that the A/R or ineligible spouse or parent owns under the following conditions:

* The owner has the right, authority, or power to convert the asset to cash (if not already cash).
* The owner is not legally restricted from using the asset for his/her support and maintenance.

An asset is usually income in the month of receipt.
Any portion of a countable asset that is retained becomes a resource on the first day of the month following the month of receipt.
An asset cannot be considered income and a resource in the same month.

*EXCEPTIONS:*

* Checks dated and received early because the regular date of receipt falls on a weekend or holiday are counted as income, rather than a resource, in the month for which they were intended.
* Any portion of a lump sum SSI or RSDI payment retained after the month of receipt is excluded as a resource for up to nine full calendar months.
(These entire lump sum payments are income in the month of receipt.)

The values of some resources may be totally or partially excluded.
Refer to xref:2304.adoc[Section 2304], Treatment of Resources.

The countable value of a resource that is applied to the resource limit is the resource's equity value as of the first moment of the first day of each calendar month.

[caption=Exception]
NOTE: Current market value (CMV) is used for automobiles that cannot be totally excluded.

Refer to xref:2303.adoc[Section 2303], Determining the Countable Value of Resources.

If an adult A/R lives with a spouse in LA-A or B, the spouse's resources are considered to be available to the A/R.

If a Medicaid child lives with his/her parent(s) in LA-A, B or C, the parent(s) excess resources are deemed to the child.
Refer to xref:2502.adoc[Section 2502], Deeming.

There are two types of resources.

* Liquid resources are any resources in the form of cash or in any other form that can be converted to cash within 20 workdays.
* Non-liquid resources are any resources which are not in the form of cash and which cannot be converted to cash within 20 workdays.

If an individual is unaware of his/her ownership of an asset, the asset is not a resource during the period for which the individual was unaware of the ownership.
The previously unknown asset, including any monies (such as interest) accumulated on it through the month of discovery by the individual, is income only in the month of discovery.
For exceptions on interest or dividends as income, refer to xref:2499.adoc[Section 2499].
Thereafter, the previously unknown asset is subject to resource-counting rules.

=== Conversion of Resources

In the event an excluded resource is converted to a countable resource, including cash, the value of the resource is applied to the appropriate resource limit in the month after the month of conversion.

[caption=Exception]
NOTE: Proceeds from the sale of capital goods are considered income.
Refer to Section 2499.1, Treatment of Income by Type.
