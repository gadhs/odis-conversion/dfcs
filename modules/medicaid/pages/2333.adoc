= 2333 Safe Deposit Box
:chapter-number: 2300
:effective-date: April 2020
:mt: MT-59
:policy-number: 2333
:policy-title: Safe Deposit Box
:previous-policy-number: MT 16

include::ROOT:partial$policy-header.adoc[]

== Requirements

The contents of a safe deposit box may include undisclosed countable resources and/or legal documents necessary for resource verification.

== Basic Considerations

A safe deposit box is a strong metal container for storing valuable papers, jewels, or keepsakes, in a bank.

If an A/R alleges not having a safe deposit box, no further development is required unless it is necessary to send a Form 957 to a financial institution for verification of checking or savings accounts.
Always check the entry, “Does the person have a safe deposit box…?”, when a Form 957 is required.

== Procedures

=== ABD Medicaid Non-FBR COAs

If a non-FBR A/R alleges having a safe deposit box, ask the A/R or PR to itemize the contents of the box.
If necessary, require the A/R or PR to submit any document(s) for verification purposes.
Exclude any household goods and personal effects kept in the box.
_If the A/R or PR's statement of contents is questionable,_ follow the procedures below for FBR A/Rs.

=== ABD Medicaid FBR COAs

If an FBR A/R alleges to have a safe deposit box, the contents must be inventoried.
Complete a physical inventory or get a sworn statement from a bank official.

=== Family Medicaid

Accept the A/R's sworn statement as to the contents of a safe deposit box.
