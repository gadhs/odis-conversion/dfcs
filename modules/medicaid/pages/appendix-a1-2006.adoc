= Appendix A1 ABD Financial Limits 2006
:chapter-number: Appendix A1
:effective-date: N/A
:mt: MT-21
:policy-number: Appendix A1
:policy-title: ABD Financial Limits
:previous-policy-number:

.CHART A1.1 - ABD MEDICAID RESOURCE LIMITS
|===
| Type Limit | Individual Limit | Couple Limit | LA-D Individual With a Community  Spouse | Effective Date

| SSI/LA-D
^| $2000
^| $3000
>| N/A
>| 7-88

| AMN
^| $2000
^| $4000
>| N/A
>| 4-90

| QMB/SLMB/ QI-1/QDWI
^| $4000
^| $6000
>| N/A
>| 1-89

| Spousal Impoverishment
^| N/A
^| N/A
| $99,540 + 2000 =

$101,540.00
>| 1-06
|===

.CHART A1.2 - ABD MEDICAID NET INCOME LIMITS (GROSS - $20)
|===
| Type Limit | LA | Individual Limit | Couple Limit | Effective Date

^| *AMN*
^| All
>| $317
^| $375
| 10-90

.4+^| *FBR*

*(SSI Limit)*
^| A
>| $603
^| $904
.4+^| 1-06

^| B
>| $402
^| $603

^| C
>| $603
^| N/A

^| D
>| $30
^| N/A

^| *Medicaid CAP*
^| D
>| $1809
^| $3618
| 1-06

.3+| *QDWI*
^| A
>| $3352
^| $4485
.3+a|
3-06

NOTE: Effective 3-98, ISM no +
longer applies to this COA +
eliminating LA-B.

^| C
>| $3352
^| N/A

^| D
>| $3352
^| N/A

^| *QMB*
^| A
>| $817
^| $1100
| 4-06

^| *SLMB*
^| A
>| $980
^| $1320
| 4-06

^| *QI-1*
^| A
>| $1103
^| $1485
| 3-06
|===

.CHART A1.3 - TRANSFER OF RESOURCE PENALTY DETERMINATION
[cols=3*]
|===
| Averaging Nursing Home Private Pay Billing Rate
| $4257.60
^| 4-06
|===

.CHART A1.4 - PRESUMED MAXIMUM VALUE (PMV) OF ISM AND LIVING ALLOWANCE TO EACH INELIGIBLE CHILD
|===
| Income Limit | PMV for an Individual | PMV for a Couple | Living Allowance | Effective Date

^| *AMN*
| $221
^| $321.34
>| $301.34
>| 1-06

^| *FBR*
| $221
^| $321.34
>| $301.34
>| 1-06

^| *QMB*
| N/A
^| N/A
>| $373..33
>| 4-06

^| *SLMB*
| N/A
^| N/A
>| $446.67
>| 4-06

^| *QI-1*
| N/A
^| N/A
>| $501.67
>| 3-06
|===

.CHART A1.5 - SUBSTANTIAL GAINFUL ACTIVITY
|===
| Category | Income Limit | Effective Date

^| Non-Blind individuals
^| $860
.2+^| 1-06

^| Blind individuals
^| $1450
|===

.CHART A1.6 – BREAK-EVEN POINTS
[cols=6*]
|===
.2+h| Living Arrangement
2+h| Earned Income
2+h| Unearned Income
.2+h| Effective Date

^h| Individual
^h| Couple
^h| Individual
^h| Couple

| *A*
^| $1271
^| $1873
^| $603
^| $904
.2+^| 1-06

| *B*
^| $869
^| $1271
^| $402
^| $603

| *D*
^| $145
^| $205
^| $50
^| $80
^| 7-88
|===

.CHART A1.7 – MONTHLY AVERAGED MEDICAID RATES FOR KATIE BECKETT
|===
| Level of Care | Monthly Amount | Effective  Date

^| Skilled Nursing Facility
^| $3645
.2+| 11/04

^| ICF/MR
^| $6667
|===

[.text-center]
*A1.8 – MEDICARE EXPENSES*

Medicare Part B Premium rate: $88.50 (effective 1-06).

Medicare Approved Drug Discount Card: up to $30 (effective 6/04)

.CHART A1.9 - PERSONAL NEEDS ALLOWANCES (PNA) FOR AN LA-D RECIPIENT
|===
| IF the LA-D Recipient is 2+| THEN use the following as the PNA in the  Patient Liability/Cost Share Budget:

| an individual in a nursing home or Institutionalized Hospice
^| $50
>| Effective 7-06

| a VA pensioner or his/her surviving spouse in a nursing home who has dependents
^| $50
>| Effective 7-06

a|
a VA pensioner or his/her surviving spouse in a nursing home who has no dependents

NOTE: The VA check for these individuals is reduced to the amount of the PNA, regardless of other income.
^| $90
^| Effective 1-92

(Effective 1-93 for the Surviving Spouse)

| an individual in CCSP
2+| the current amount of the Individual FBR for LA-A

| an individual in ICWP
2+| the current amount of the Community Spouse Maintenance Need Standard

| an individual in MRWP
2+| the current Medicaid Cap
|===

.CHART A1.10 - NEED STANDARDS FOR DIVERSION OF INCOME TO A COMMUNITY SPOUSE OR DEPENDENT FAMILY MEMBER IN A PATIENT LIABILITY/COST SHARE BUDGET
|===
| Diversion Standard | Amount | Effective  Date

| Community Spouse Maintenance Need Standard
^| $2488.50
^| 1-06

| Dependent Family Member Need Standard
^| $1670
^| 4-06
|===

.CHART A1.11 – FEDERAL POVERTY LEVEL TABLES FOR MEDICARE PART D - LOW INCOME SUBSIDY
|===
| HOUSEHOLD  SIZE | 100% | 135% | 140% | 145% | 150% | EFF.  DATE

^| 1
^| $9,800.00
| $13,230.00
^| $13,720.00
^| $14,210.00
^| $14,700.00
.5+| 2006

^| 2
^| 13,200.00
| 17,820.00
^| 18,480.00
^| 19,140.00
^| 19,800.00

^| 3
^| 16,600.00
| 22,410.00
^| 23,240.00
^| 24,070.00
^| 24,900.00

^| 4
^| 20,000.00
| 27,000.00
^| 28,000.00
^| 29,000.00
^| 30,000.00

^| 5
^| 23,400.00
| 31,590.00
^| 32,760.00
^| 33,930.00
^| 35,100.00
|===

The FPL (100% level) is increased by $3,260 for each additional person in the household.

.CHART A1.12 – COSTS AND GUIDELINES FOR RECEIPT OF MEDICARE PART D - LOW INCOME SUBSIDY
|===
| | Group 1 | Group 2 | Group 3 | Eff.  Date

^| *Resource Limit*
^| None
^| Non Q Track Individual -

$6000

Non Q Track Couple -

$9000
| Individual - $10,000 Couple - $20,000
.6+| 2005

^| *Income Limit*
| 100% of

FPL or full Medicaid
^| Less than 135% of FPL
^| Less than 150% of FPL

| *Monthly*

*Premium*
^| $0
^| $0
^| Sliding Scale

| *Deductible*

*Per Year*
^| $0
^| $0
^| $50.00

| *Coinsurance*

*up to $3600 Out of Pocket*
| $1 - $3

Copay
^| $2 - $5 Copay
^| 15% Coinsurance

| *Catastrophic*

*5% or $2/$5 Copay*
^| $0
^| $0
^| $2 - $5 Copay
|===

[.text-center]
*A1.13 – Medically Needy Mileage Re-imbursement Rate*

48.5 cents per mile – 9/10/05 – 12/31/05

44.5 cents per mile – effective 1/1/06
