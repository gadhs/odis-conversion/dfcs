= 2332 Retirement Funds
:chapter-number: 2300
:effective-date: April 2020
:mt: MT-59
:policy-number: 2332
:policy-title: Retirement Funds
:previous-policy-number: MT 39

include::ROOT:partial$policy-header.adoc[]

== Requirements

A retirement fund owned by an eligible individual is a countable resource if s/he has the option of withdrawing the fund as a lump sum, even if s/he is not eligible for periodic payments.

== Basic Considerations

Retirement funds are work-related plans for providing income when employment ends, such as a pension, disability, or retirement plans administered by an employer or union.
Other examples are funds held in an individual retirement account (IRA), Roth or Traditional and plans for self-employed individuals, sometimes referred to as Keogh plans.
Also, depending on the requirements established by the employer, some profit-sharing plans may qualify as retirement funds.

Periodic retirement benefits are payments made to an individual at some regular interval (e.g., monthly) and which result from entitlement under a retirement fund.
Payments which consist of interest or dividends only do not meet the definition of entitlement under a retirement.
The payments must include a portion of the principal.

The value of a retirement fund is the amount of money that an individual can currently withdraw from the fund.
If there is a penalty for early withdrawal, the fund's value is the amount available to an individual after the penalty deduction.
Any income taxes incurred by the withdrawal are not deductible in determining the fund's value.
However, if the individual is eligible for and receives periodic payments which include a portion of the principal, the retirement fund is excluded as a countable resource.

To be eligible for ABD Medicaid, an individual must apply for periodic benefits.
If s/he has a choice between periodic payments and a lump sum, they must choose the periodic payments.

A retirement fund is not a countable resource if an individual must terminate employment in order to obtain any payment from the fund.

A previously unavailable retirement fund is not income to its recipient when the fund becomes available.
The fund is subject to resource counting rules in the month following the month in which it first becomes available.

A resource determination for the month following that in which a retirement fund becomes available for withdrawal must include the fund's value.
A delay in payment for reasons beyond the individual's control, such as an organization's processing time, does not mean that the fund is not a resource since the individual is legally able to obtain the money.
It is a non-liquid resource.

If an individual receives a denial on a claim for periodic retirement payments but can withdraw the funds in a lump sum, include the fund's lump sum value in the resource determination for the month following that in which the individual receives the denial notice.

If an ineligible spouse, parent or spouse of parent owns a retirement fund, the value is excluded as a countable resource.

== Procedures

If an individual has a retirement fund, determine and document whether he/she is eligible for periodic payments.
If not, determine and document whether he/she can make a lump-sum withdrawal.

If an individual is eligible for and receiving periodic payments from a retirement fund, which include a portion of the principal, budget the payments as income. +
If an individual is eligible for periodic payments but refuses to accept them, count the value of the retirement fund as a resource.
