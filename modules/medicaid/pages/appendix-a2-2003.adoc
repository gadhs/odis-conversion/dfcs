= Appendix A2 Family Medicaid 2003
:chapter-number: Appendix A2
:effective-date: N/A
:mt: MT-6
:policy-number: Appendix A2
:policy-title: Family Medicaid 2003
:previous-policy-number:

== 2003 Income Limits

|===
| LIM | LIM | RSM PgW, | RSM CHILD 0-1 | RSM CHILD 1-5 | RSM CHILD 6-19 | FM-MNIL

|
|
>| *NB, PCK*
^| *TMA, WIC*
|
|
|
|===

|===
| BUDGET GROUP (BG) SIZE | GROSS INCOME CEILING (GIC) | STANDARD OF NEED (SON) | 235% FEDERAL POVERTY LEVEL (FPL) | 185 % FEDERAL POVERTY LEVEL (FPL) | 133% FEDERAL POVERTY LEVEL (FPL) | 100 % FEDERAL POVERTY LEVEL (FPL) | FAMILY MEDICAID MNIL

^| *1*
>| *$ 435*
>| *235*
>| *1761*
>| *1385*
>| *996*
^| *749*
| *208*

^| *2*
>| *659*
>| *356*
>| *2374*
>| *1869*
>| *1344*
^| *1010*
| *317*

^| *3*
>| *784*
>| *424*
>| *2990*
>| *2353*
>| *1692*
^| *1272*
| *375*

^| *4*
>| *925*
>| *500*
>| *3605*
>| *2837*
>| *2040*
^| *1534*
| *442*

^| *5*
>| *1060*
>| *573*
>| *4219*
>| *3321*
>| *2388*
^| *1795*
| *508*

^| *6*
>| *1149*
>| *621*
>| *4834*
>| *3805*
>| *2736*
^| *2057*
| *550*

^| *7*
>| *1243*
>| *672*
>| *5450*
>| *4289*
>| *3084*
^| *2319*
| *600*

^| *8*
>| *1319*
>| *713*
>| *6063*
>| *4773*
>| *3432*
^| *2580*
| *633*

^| *9*
>| *1389*
>| *751*
>| *6678*
>| *5258*
>| *3781*
^| *2842*
| *667*

^| *10*
>| *1487*
>| *804*
>| *7293*
>| *5743*
>| *4130*
^| *3104*
| *708*

^| *11*
>| *1591*
>| *860*
>| *7908*
>| *6228*
>| *4479*
^| *3366*
| *758*

^| *12*
>| *1635*
>| *884*
>| *8523*
>| *6713*
>| *4828*
^| *3628*
| *808*

| *(+) PER ADDITIONAL*

*BG MEMBER*
^| *44*
>| *24*
>| *615*
>| *485*
>| *349*
^| *262*
| *50*
|===

[.text-center]
*2003 RESOURCE LIMITS*

LIM RESOURCE LIMIT: $1000

.FAMILY MEDICAID MEDICALLY NEEDY (FM-MN) RESOURCE LIMIT NUMBER OF INDIVIDUALS IN FM-MN BG
|===
| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12

^| *$ 2000*
^| *4000*
^| *4100*
^| *4200*
^| *4300*
^| *4400*
^| *4500*
^| *4600*
^| *4700*
^| *4800*
^| *4900*
^| *5000*
|===

(04/2003)
