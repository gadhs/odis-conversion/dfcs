= 2502 Deeming (ABD)
:chapter-number: 2500
:effective-date: February 2020
:mt: MT-58
:policy-number: 2502
:policy-title: Deeming (ABD)
:previous-policy-number: MT 57

include::ROOT:partial$policy-header.adoc[]

== Requirements

Deeming is the process by which the income and resources of an ineligible spouse or ineligible parent are included in the budget to determine the A/R's financial eligibility for ABD Medicaid.

== Basic Considerations

The ineligible spouse of the A/R may be a legal, non-legal or community spouse.
Non-legal marriages established on or after January 1, 1997, are not recognized as marriages in Georgia regardless of the ABD Medicaid Class of Assistance (COA).

*Legal Spouses (Prior to 1/1/97)* +
A legal spouse is a member of a couple who has been married by legal ceremony or +
common-law.
Under Georgia law, two individuals are in a common-law marriage if they +
live together at least one night, they are holding forth to the community as husband and +
wife, both are free to marry each other because neither is married to another person, and +
the relationship was established prior to January 1, 1997.
Legality matters in relationships +
established prior to 1/1/97 only when the A/R is in LA-D or applies under an ABD Medically +
Needy COA.

*Legal Spouses (On or after 1/1/97)* +
A legal spouse is a member of a couple who has been married by legal ceremony.
This definition of a legal marriage is effective as of January 1, 1997.
Common–law marriages established after this date are not considered legal.
Legality matters in relationships established on or after 1/1/97 for all ABD Medicaid COAs.

*Non-Legal Spouses* +
Non-legal spouses are holding out as husband and wife, but they are not free to marry +
each other.
One or both has a legal spouse.
Otherwise known as a non-member.

*Community Spouse* +
A community spouse is a person who is not living in a setting that provides medical care/services, such as a medical institution or nursing facility, and who is married to:

* An institutionalized person, or
* A person who has been determined eligible for a Home and Community-Based Services wavier program.

The community spouse may live in his/her own home (LA-A), in a Personal Care Home/Assisted Living (not A/R in LA-D COA) or in someone else's home (LA-B).
If the A/R in LA-D can only be considered under the AMN COA due to income over the Medicaid Cap (no Qualified Income Trust), the spouse at home does not meet the definition of a Community Spouse, and therefore Spousal Impoverishment regulations do not apply. +
If the community spouse is living in a personal care facility, check the bill to see if the spouse is actually living in a medical facility.
If the personal care facility is billing room and board only, the spouse meets the definition of a community spouse.
If the personal care facility is billing for the services of any medical professional (such as registered nurse [RN], licensed vocational nurse [LVN], doctor, etc.), the spouse does not meet the definition of a community spouse and spousal impoverishment policies does not apply.

An incarcerated spouse is *NOT* considered a community spouse for spousal impoverishment purposes.

*Medicaid Child* +
A Medicaid child is a child living with his/her parent(s) who is a SSI or Medicaid eligible under any COA.

*Ineligible Child* +
An ineligible child is a child living with his/her parent(s) who is not receiving Public Assistance payments, such as SSI, TANF or VA pension, and is not applying for or receiving Medicaid under any COA.

*Ineligible Parent* +
An ineligible parent is the natural, adoptive or stepparent of the Medicaid child or ineligible child and is not a Medicaid A/R.
If the Medicaid child lives with the ineligible parent, a portion of the ineligible parent's income and resources may be considered in determining the child's eligibility for ABD Medicaid.

*Deeming Begins* +
Spouse to Spouse Deeming begins the month following the month a marital relationship +
begins.
Refer to Section 2501, Marital Relationship.

Parent to Child Deeming begins the month after a parent(s) and child begin living together in the same household (LA-A, B or C).

*Deeming Ceases* +
Spouse to Spouse Deeming ceases the month following the month a marital relationship +
ends.
Refer to Section 2501, Marital Relationship.

Parent to Child Deeming ceases the month following the month parents and their children +
cease living together in the same household (LA-A, B or C).

After an A/R ceases living with the financially responsible relative, only income actually +
made available to the A/R is considered in determining the A/R's financial eligibility.

[caption=Exception]
NOTE: If the responsible relative is temporarily absent from the home and is +
expected to return within the same month or the following month, consider the absent +
relative to be living with the A/R.

When one spouse (or child) enters LA-D, deeming of income ceases for the LA-D A/R the +
month of admission.
Spousal impoverishment resource rules apply the month of +
admission.
If the community spouse is an A/R under a non-LA-D COA, deeming of income +
and resources ceases for the community spouse the month following the month of +
admission.

When both spouses enter LA-D, treat both A/Rs as individuals beginning the month of +
admission in determining income eligibility.
See Section 2510-2 for exceptions.
For +
resource eligibility, treat both A/Rs as a couple the month of admission and as individuals +
the month following the month of admission.

[caption=Exception]
NOTE: Admission to a hospital does not terminate the hospitalized individual's +
financial responsibility for his/her spouse or child residing in the community.
Refer to +
Special Considerations in Section 2503, Couples.

Deemors are entitled to the same resource exclusions allowed to the A/R.
Refer to Chapter 2300, Resources.

*Resources Excluded from Deeming* +
The following pension funds owned by an ineligible/community spouse or parent are not deemed to an A/R: (See Section 2332-2)

* IRAs
* Keoghs
* Private pension funds.
+
NOTE: Private pension funds owned by the A/R are always considered in the resource +
determination.

*Income Excluded from Deeming* +
The following types of income owned by an ineligible spouse or parent are not deemed to +
the A/R:

* Income Based on Need (IBON), such as the following:
 ** SSI/TANF
 ** IV-E payments
 ** VA pension
 ** Only VA compensation received by the parent(s) of a veteran who died from a service-related cause prior to January 1, 1957.
* Any income used in determining the amount of an IBON payment.
* Portions of scholarships, grants, or fellowships used to pay tuition and/or fees
* Foster care payments for the care of an ineligible child
* Value of Food Stamps/commodities.
* Home produce for personal consumption
* Tax refunds
* Court ordered support payments made by a deemor
+
NOTE: Support payments made by the A/R are not excluded.

* ISM received by a deemor or ineligible child
* Income otherwise excluded by federal statute
* Total earned income of an ineligible child if a student ($400/month, up to $1,620/year)
* Income necessary for a Plan to Achieve Self Sufficiency (PASS)
* Infrequent or irregular income of parents and spouses (one exclusion per marital relationship)
* Funds withdrawn from parent's retirement/pension fund (i.e. IRA) in the month of withdrawal.
+
NOTE: Any funds remaining the month after the withdrawal +
are a resource.

== Procedures

*Deeming Resources from an Ineligible Spouse to a Medicaid Individual in LA-A or B* +
Combine the resources of the Medicaid individual and his/her ineligible spouse and apply +
them to the appropriate couple resource limit.

If the combined resources do not exceed the couple resource limit, the A/R is eligible +
based on resources.

*Deeming Resources from A Non-legal Ineligible Spouse to a Medicaid Individual in* +
*LA-D* +
If the Medicaid individual enters LA-D, has a non-legal ineligible spouse at home, and the +
non-legal marital relationship was established prior to 1/1/97, combine the resources of the +
Medicaid individual and his/her ineligible spouse and apply them to the SSI Couple +
resource limit only for the month of admission to LA-D.
(If the non-legal marital relationship +
was established on or after 1/1/97, use the individual resource limit.)

Consider only the Medicaid individual's resources beginning the month following the month +
of admission to LA-D.
Use the SSI Individual resource limit.

NOTE: If the A/R transfers assets to a non-legal ineligible spouse, apply a transfer of assets +
penalty.
Refer to Section 2342, Transfer of Assets. +
*Deeming Resources When an A/R with a Community Spouse enters LA-D* +
Follow the steps below to determine the resource eligibility for an A/R in LA-D who meets +
all the related eligibility criteria and who has a community spouse for each prior month of +
Medicaid requested.
During the application process the A/R and spouse must have +
resources under the Spousal Impoverishment Limit.
The LA-D spouse cannot avoid +
ineligibility by transferring excess resources to the community spouse.

*Prior Months Eligibility* +
Combine the countable resources owned by the A/R and the community spouse on the +
first day of the prior month and apply them to the Spousal Impoverishment resource limit.

If the combined resources are greater than the Spousal Impoverishment resource limit, the +
A/R is ineligible based on resources.
Deny Medicaid for the prior month.

If the combined resources are less than or equal to the Spousal Impoverishment resource +
limit, the A/R is eligible based on resources for the prior month.

*Ongoing Eligibility* +
If the A/R meets the eligibility criteria for LOS and LOC, follow the steps below to determine the ongoing resource eligibility for an A/R in LA-D who has a community spouse:

*Step 1* Combine the countable resources owned by the A/R and the community spouse +
on the first day of the month of application and apply them to the Spousal +
Impoverishment resource limit.
If the combined resources are greater than the +
Spousal Impoverishment resource limit, the A/R is ineligible based on resources.
Deny the application.

If the combined resources are less than or equal to the Spousal Impoverishment +
resource limit, the A/R is eligible based on resources.
Proceed to Step 2.

*Step 2* Apply the countable resources owned by the A/R on the first day of the month +
of application to the SSI Individual resource limit.

If the resources owned by the A/R are less than or equal to the SSI Individual +
resource limit, the A/R continues to be eligible based on resources.
Consider +
the ongoing resource eligibility determination for the A/R to be completed.

If the resources owned by the A/R exceed the SSI Individual resource limit, +
require the A/R to transfer his/her resources in excess of the SSI Individual +
resource limit to the community spouse or to a third party for the sole benefit of +
the community spouse by no later than the first annual review. +
Proceed to Step 3.

NOTE: See page 2502-7, Chart 2502.1, for definition of “for sole benefit of” the community spouse.

*Step 3* Require the A/R to declare in writing the resources s/he intends to transfer to +
the community spouse or to a third party for the sole benefit of the community +
spouse.

*Step 4* Notify the A/R in writing of the type and value of resources s/he has declared +
and the requirement to transfer by the first annual review.

*A/R Receives an Additional Resource Before the First Annual Review* +
If the A/R receives an additional resource between the initial determination of eligibility and the first annual redetermination, redetermine the A/R's resource eligibility.

If the additional resource combined with other resources of the A/R does not exceed the SSI Individual resource limit, document the record that the A/R continues to be resource eligible without any further development. +
If the additional resource combined with other resources of the A/R exceeds the SSI Individual resource limit, repeat Step 1 above, using the resources of the community spouse for the first day of the month of application and the current month's first day resources for the A/R.

*Resource Eligibility Determination at First Annual Review* +
If the resources of the A/R are less than or equal to the SSI individual resource limit on the first day of the month of the first annual review, consider the A/R to remain eligible based on resources.
Verify that the A/R transferred his/her countable assets in excess of the SSI individual resource limit to the community spouse or to a third party for the sole benefit of the community spouse.

If resources of the A/R exceed the SSI individual resource limit on the first day of the month of the review, terminate Medicaid eligibility after giving timely notice.
If assets were transferred inappropriately, compute a transfer of assets penalty on the A/R.
See Chart 2502.1 and Section 2342, Transfer of Assets.

*A/R with a Community Spouse Receives an Additional Resource after the First Annual Review* +
Follow the steps below when a LA-D Medicaid recipient with a community spouse receives a resource after the first annual review which, combined with other resources, causes the total countable resources of the A/R to exceed the SSI individual resource limit.

*Step 1* Require the A/R to declare in writing the intent to transfer his/her resources in +
excess of the SSI individual resource limit to the community spouse or to a third +
party for the sole benefit of the community spouse.

*Step 2* Allow the A/R 90 days from the date of receipt of the excess resource to transfer +
the resources to the community spouse.

*Step 3* If the A/R transfers the resource within the 90-day grace period, consider the +
A/R to remain eligible based on resources.

If the A/R fails to transfer the resource within the 90-day grace period, terminate +
eligibility after giving timely notice.

NOTE: An individual can only be eligible using the Spousal Impoverishment resource limit one time, if the A/R remains institutionalized and continuously Medicaid eligible.
However, if the A/R's Medicaid is terminated at any point and the A/R is no longer in LA-D, subsequent applications may reuse the spousal Impoverishment resource limit. +
Use the following chart to assess a resource transfer made by a LA-D A/R with a community spouse:

.Chart 2502.1 - Resource Transfers by an A/R with a Community Spouse
|===
| if | then

| the A/R transfers an asset to the community spouse
| Do not apply a transfer of assets penalty.

| the A/R transfers an asset to someone else for the sole benefit of the community spouse
| Do not apply a transfer of assets penalty.

The transfer must be accomplished by a written document that legally binds the third party to a specified course of action, and which clearly states the condition of the transfer and who can benefit from the transfer.
The transfer must be made so that no individual or entity can benefit from the transfer, either at the time of transfer or in the future, except the community spouse.

| the A/R transfers an asset to someone other than the community spouse
| Apply a transfer of assets penalty.
Refer to Section xref:2342.adoc[2342, Transfer of Assets.]

| If the community spouse transfers an annuity or homeplace to someone other than the A/R spouse, and it is not for the sole benefit of the community spouse,
| Apply a transfer of assets penalty.
Refer to Section xref:2342.adoc[2342, Transfer of Assets.]

| If the community spouse transfers an asset (other than annuity/homeplace) to anyone *after* eligibility has been established for the A/R spouse,
| There is no penalty to the A/R, However, if the community spouse subsequently enters LA-D, then she/he will incur a penalty.
|===

*Spouse to Spouse Deeming of Income* +
Complete Spouse to Spouse Deeming of Income on Form 172 when the A/R is a Medicaid individual living with an ineligible spouse in LA-A or B. Refer to Section 2507, Spouse to Spouse Deeming.

[caption=Exception]
NOTE: DO NOT deem the income of a spouse for any month eligibility is determined for an A/R in LA-D under a class of assistance that uses the Medicaid CAP as the income limit.
Refer to Section 2510, Medicaid CAP Budgeting.

*Deeming Resources from Ineligible Parent(s) to a Medicaid Child* +
If only one parent is living in the household with the Medicaid child, deem the parent's resources as follows:

* Subtract the Individual resource limit appropriate for the COA from the parent's countable resources.
* Deem the amount in excess of the Individual resource limit to the eligible child(ren).

If two parents are living in the household with the Medicaid child, deem the parents' resources as follows:

* Subtract the Couple resource limit appropriate for the COA from the combined countable resources of both parents.
* Deem the amount in excess of the Couple resource limit to the eligible child(ren).

If the Medicaid child enters LA-D, deem the parent(s)' resources for the month of admission to LA-D.
Consider only the child's resources beginning the month following the month of admission to LA.

*Parent to Child Deeming of Income* +
Complete parent to child deeming of income on Form 171 if the A/R is a Medicaid child living in the household with his/her parent(s).
Refer to Section 2508, Parent to Child Deeming.

[caption=Exception]
NOTE: DO NOT deem the income of a parent(s) for any month eligibility is determined for a Medicaid child in LA-D under a COA that uses the Medicaid CAP as the income limit.
Refer to Section 2510, Medicaid CAP Budgeting.
