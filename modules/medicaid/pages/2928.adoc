= 2928 Georgia Hearing Aid Distribution Program
:chapter-number: 2900
:effective-date: June 2020
:mt: MT-60
:policy-number: 2928
:policy-title: Georgia Hearing Aid Distribution Program
:previous-policy-number: MT 30

include::ROOT:partial$policy-header.adoc[]

== Requirements

The Georgia Hearing Aid Distribution Program, enacted by the Georgia Legislature, provides hearing aids to Georgians, subject to eligibility requirements.

== History

The Public Service Commission has contracted with the Georgia Lions Lighthouse Foundation, a nonprofit organization that has provided hearing aids to low income Georgians for over thirty years.

== Basic Considerations

In order to qualify for this program, the applicant must meet the following guidelines:

* Applicant must be a Georgia resident for at least one year
* Applicant must present a recommendation from a state-licensed hearing care provider
* Hearing aids will only be dispensed through a state-licensed hearing care provider
* Applicant's income must not exceed 200% of the Federal Poverty guidelines;

This program will only provide hearing\aids to individuals 18 years of age or older as Georgia Medicaid provides hearing aids to those under age 18.

== Procedures

For more information on this program contact the Georgia Lions Lighthouse Foundation at (404)325-3630, or 1-800-718-7483 outside of metro Atlanta.
Georgia Lions Lighthouse Foundation can also be contacted at the following website: https://www.lionslighthouse.org/.
