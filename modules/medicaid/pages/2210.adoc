= 2210 Application for Other Benefits
:chapter-number: 2200
:effective-date: November 2023
:mt: MT-71
:policy-number: 2210
:policy-title: Application for Other Benefits
:previous-policy-number: MT 70

include::ROOT:partial$policy-header.adoc[]

== Requirements

An Applicant/Recipient (A/R) for Medicaid must apply for and accept all other monetary benefits, payments or allotments to which s/he or any member of the Assistance Unit (AU) or Budget Group (BG) may be entitled in order to be eligible for Medicaid.

== Basic Considerations

Application for other benefits must be made prior to the approval of the Medicaid application based on the applicant's or beneficiary's statement they will apply.
For Family Medicaid, do not delay benefits for proof of the application for other potential benefits.

*EXCEPTIONS:*

* Application for other benefits is not required for the following COAs:
 ** Newborn Medicaid
 ** TMA
 ** Four Months Extended (4MEx)
 ** Pregnant Woman Medicaid
 ** Q Track
 ** PeachCare for Kids® (PCK)
 ** Pathways
 ** Planning for Healthy Babies® (P4HB)

* Family Medicaid COAs do not require application for SSI or TANF.

* MAGI Family Medicaid COAs do not require an application for non-taxable income benefits;
however, they should be informed of the potential benefits.
Non-taxable benefits include, but are not limited to:
 ** Child Support
 ** Supplemental Security Income (SSI)
 ** Veterans Affairs (VA) benefits
 ** Worker's Compensation

* Applications for other benefits that would result in an overall reduction of current income are not required.

NOTE: Advise the applicant of potential benefits, even if application is not required.

Failure or refusal to apply for and accept other benefits results in the following actions for Family Medicaid COAs:

* If the potential benefit is for a parent, exclude the parent and everyone for whom s/he is financially responsible.

* If the potential benefit is for a child, exclude only the child.

An individual in a Family Medicaid COA who fails to apply for other benefits may be eligible for another Family Medicaid COA in which the application for other benefits is not required.
Complete a CMD prior to denial or termination of Medicaid and document the results of the CMD.

NOTE: Failure or refusal to apply for and accept other benefits (excluding RSDI Disability/SSI) results in ineligibility for ABD Medicaid COAs.

The A/R must apply for the highest possible benefit for which s/he is eligible and must accept a benefit for the earliest month it is available.
In addition, the A/R must comply with all requirements set forth by the agencies issuing the other benefits.
These agencies include, but are not limited to: Department of Labor, Social Security Administration, and Veteran's Administration.

AU members are required to apply for Unemployment Compensation Benefits (UCB) only if Clearinghouse indicates potential eligibility.

ABD Application for VA Compensation or VA Pension must be made by individuals who may be eligible for either benefit.
VA Pension applicants may choose either to project estimated medical expenses (prospective) or claim medical expenses for the past year (retrospective).

[caption=Exception]
NOTE: A/Rs who are currently receiving a VA Pension do not have to file a special application for the New Improved Pension.

Benefits and income are not synonymous terms.
Benefits include, but are not limited to the following:

* UCB
* annuities
* disability payments and Worker's Compensation

* pension
* unprobated estates

Benefits do *NOT* include the following:

* alimony
* child support
* Medicare
* payments on loans or promissory notes
* rent

The A/R is *NOT* required to apply for the following benefits:

* TANF
* benefits from a trust over which the A/R has no control
* Earned Income Credit (EIC)
* non-receipt of court ordered child support/alimony
* Prouty (Special Age 72) RSDI benefits
* Veterans Aid and Attendance
* Veterans Household Allowance
* Widow(er)'s Year's Support
* RSDI Disability, SSI
+
NOTE: If A/R appears eligible for RSDI Disability or financially eligible for SSI, refer A/R to apply.
However, failure to apply will not affect eligibility.

NOTE: Refer to Chapter 2400, Income, for more detailed information about specific types of income.

== Procedures

Determine those benefits to which the A/R may be entitled by asking the A/R about employment history, military service, etc., of the applicant, AU members and any person through whom the applicant or AU members may be entitled (i.e., spouse or parent).

Advise the A/R or PR of other benefits to which the AU may be entitled and refer the A/R to the appropriate agency to apply.
Do not refer A/R to apply for SSI if you know s/he will not be eligible (example: Katie Beckett child whose parents' income/resources exceed the SSI limit or aliens who don't meet the criteria for SSI eligibility).

Assign a reasonable deadline for the A/R to apply for the other benefits and to provide verification that the application was made.

Verify with one of the following:

* approval or denial letter
* documentation verifying proof of application
* contact with the agency where the application was filed.

NOTE: For Family Medicaid COAs, the A/R's verbal or written statement that s/he will apply or has applied for other benefits is sufficient to meet this requirement.
This is applicable at application, renewal, or interim change.

If the applicant is eligible on all other points of eligibility, do not delay approval of an application awaiting the approval/denial of the application for other benefits.

Schedule an interim review to verify the status of the application for other benefits if the application is still pending at time of approval.
For Family Medicaid COAs, follow-up is required in the third month following the month that potential eligibility is indicated.
Verification of the application for other benefits must be obtained at this time.
Check data sources and/or active related programs prior to obtaining verification.

No follow-up is required for ABD COAs as verification of application for other benefits must be obtained at application or renewal.

Document the case file to show:

* Type(s) of other benefit(s) for which A/R must apply
* Deadline date by which A/R must provide proof of application of other benefits
* Date of A/R's verbal statement that s/he will apply for other benefits and when follow- up is due
* Reason an AU was not required to apply for any potential other benefits and the benefit type (e.g., UCB)
