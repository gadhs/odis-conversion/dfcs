= 2111 Supplemental Security Income (SSI) Medicaid
:chapter-number: 2100
:effective-date: November 2023
:mt: MT-71
:policy-number: 2111
:policy-title: Supplemental Security Income (SSI) Medicaid
:previous-policy-number: MT 58

include::ROOT:partial$policy-header.adoc[]

== Requirements

Supplemental Security Income (SSI) is a direct monetary payment program administered by the Social Security Administration (SSA) under Title XVI of the Social Security Act. +
SSI Medicaid is used by DFCS as a class of assistance (COA) for determining sponsored alien and retroactive Medicaid eligibility.

== Basic Considerations

Georgia SSI recipients are automatically eligible for Medicaid for any month in which they receive a check except when they refuse to assign TPR.
Refer to xref:2230.adoc[Section 2230], Third Party Resources.

To be eligible under the SSI Medicaid COA, the A/R must meet the following conditions:

* The A/R has applied for SSI or ABD Medicaid.
* The A/R requests ABD Medicaid coverage for a month(s) for which s/he is not eligible to receive an SSI payment, such as the following:
 ** two months prior to an SSI approval (for the third prior month refer to xref:2053.adoc[Section 2053])
 ** three months prior to an SSI denial
 ** three months prior to an ABD Medicaid application
 ** the months related to an SSI application for which the applicant is not financially eligible for an SSI payment
 ** the months in which the income of a sponsor renders an alien ineligible for SSI.
* The A/R meets all basic and financial eligibility criteria.

NOTE: Length of Stay (LOS) and Level of Care (LOC) are *not* requirements for this COA.

Do *not* determine eligibility under the SSI COA for any month covered by an SSI application (the three prior months, the month of SSI application and ongoing) while the SSI application is pending.
Refer to xref:2053.adoc[Section 2053], Retroactive Medicaid.

Effective for SSI applications filed on or after August 22, 1996, the first month of SSI payment is the first month following the date the application is filed, or the first month following the month the individual becomes eligible for SSI, whichever is later.
For approved or denied SSI applications, the three prior months are the three months preceding the month of SSI application.

[caption=Exception]
NOTE: Do *not* use the SSI Medicaid COA to determine eligibility for the *third month* prior to an SSI approval.
Refer to xref:2053.adoc[Section 2053], Retroactive Medicaid, for instructions on processing eligibility for SSI prior months.

*SSI Terminations*

See xref:2579.adoc[Section 2579] for SSI Terminations of SSI 1619 Individuals.

== Procedures

Follow the steps below to determine Medicaid eligibility under the SSI Medicaid COA.

*Step 1* Accept the A/R's request for retroactive Medicaid.
A signed application is not required for this COA since an application has been filed with the Social Security Administration (SSA).

*Step 2* Obtain information necessary to process the requested months of eligibility.

*Step 3* Determine basic eligibility criteria.
Refer to Chapter 2200, Basic Eligibility Criteria.

*Step 4* Determine financial eligibility using the current SSI income and resource limits. +
Refer to Chapter 2500, ABD Financial Responsibility and Budgeting, to determine the following:

* Whose income and resources to consider
* Which SSI income/resource limits (individual or couple) to use
* Which eligibility budget to complete.

*Step 5* Approve Medicaid using the SSI Medicaid COA for any retroactive month in which the A/R meets all eligibility criteria.

NOTE: Do not approve SSI COA for the third month prior to an SSI approval.
Refer to xref:2053.adoc[Section 2053] for the appropriate COA and procedures on completing prior months for SSI applications.
