= 2903 Brain and Spinal Injury Trust Fund
:chapter-number: 2900
:effective-date: November 2023
:mt: MT-71
:policy-number: 2903
:policy-title: Brain and Spinal Injury Trust Fund
:previous-policy-number: MT 60

include::ROOT:partial$policy-header.adoc[]

== Requirements

The Brain and Spinal Injury Trust Fund provides disbursements from the Trust Fund to offset the costs of care and rehabilitative services to citizens of the state who have survived neurotrauma with brain or spinal cord injuries.
The Trust Fund will be utilized after the individual has exhausted all other resources.

== History

Senate Bill 110 created the Brain and Spinal Injury Trust Fund during the 1998 General Assembly.
Senator Charles Walker authored the bill.
This legislation created a 10% surcharge on all DUI fines beginning January 1, 1999, to offset the high costs of needed services for persons with traumatic brain injuries or spinal cord injuries.
The Trust Fund began receiving funds from the various courts through the state beginning April 1999.

A fifteen-member Commission appointed by the Governor is responsible for administering the Trust Fund and setting criteria for disbursement of the funds.
Individuals with brain or spinal cord injuries, or their family members, hold eight of the fifteen Commission seats.

== Basic Considerations

An individual with brain or spinal cord injuries may be considered eligible for a disbursement from the Trust Fund if he/she:

* Has sustained a neurotrauma with brain or spinal cord injuries

* Is a citizen of the state at the time of application and during the provision of services in Georgia

* Has exhausted all other insurance and governmental funding sources or if the service needed is outside the scope of other funding sources or is otherwise unavailable

* Submits a completed application form

Categories of care and rehabilitative services covered by the Trust Fund include but are not limited to:

* Assistive Technology
* Computers
* Dental Services
* Durable Medical Equipment/Wheelchairs
* Health and Wellness
* Home Modifications
* Personal Support Services/Attendant Care
* Respite
* Recreation
* Therapeutic/Rehabilitative Services
* Transportation/Vehicles
* Vision Services
* Vocational Support

== Definitions

Definitions listed below are for the purpose of the Brain and Spinal Trust Fund Commission Distribution Policies.

. “Brain Injury” means a traumatic injury to the brain, not of a degenerative or congenital nature, but arising from blunt or penetrating trauma or from acceleration-deceleration forces, that is associated with any of the symptoms or signs attributed to the injury.

. “Neurotrauma” means an injury to the central nervous system i.e., a traumatic brain or spinal cord injury that is caused by external physical forces.

. “Rehabilitative services” shall mean products or services for people with brain and spinal cord injuries that: (a) Enable them to have control of their own lives including their daily routine;
(b) Enable them to progress toward the goal of living in the community;
(c) Strengthen and enhance the support infrastructure, including the family, to avoid institutionalization;
and (d) Identify desired outcomes that can be measured over time.

. “Spinal Cord Injury” means a traumatic injury to the spinal cord, not of a degenerative nature, but caused by an external physical force resulting in paraplegia or quadriplegia, which can be a partial or total loss of physical function.

== Procedures

For more information about the Brain and Spinal Injury Trust Fund, visit the website at :

https://bsitf.georgia.gov/

or the customer may call the Commission's Office at the phone number below.
Applications may be submitted by mail, fax, or electronically.

Applications may be downloaded or completed and submitted online at:

https://bsitf.georgia.gov/.

Applications that have been downloaded and completed should be submitted to: +
Brain and Spinal Injury Trust Fund Commission 200 Piedmont Avenue SE +
Suite 472-F +
Atlanta, GA 30334 404-651-5112 Office Toll-free: 888-233-5760 404-656-9886 Fax +
Email: DPH-INFO-BSITF@dph.ga.gov
