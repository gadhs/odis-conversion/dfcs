= 224i Instructions for Removal Home Income and Asset Checklist
:form-number: 224i

== REMOVAL HOME – INCOME AND ASSET CHECKLIST Form 224 INSTRUCTIONS

[.underline]#*PURPOSE:*#

This form is to be completed by the SSCM/JPPS for all foster care and adoption assistance applications forwarded to the appropriate Revenue Maximization Regional Office.
The function of the form is to provide all necessary information for the Rev Max MES to determine financial eligibility in the removal home and is a part of the application for Medicaid and IV-E Foster Care and Adoption Assistance, Form 223.

Indicate on this form if the information provided is for the month of application or for a prior month's MAO and indicate the month.
If Medicaid is being requested for any of the three months prior to the application month, complete a separate Form 224 for the application month and for each month of retroactive MAO requested.

== Instructions:

This form should be completed for everyone that lived in the removal home.
Each block must be completed with an amount or n/a.
Drawn lines through this form are not acceptable.
The Rev Max MES may request additional information for a member of the removal home if needed for the eligibility determination.

*Income Section:* [.underline]#Each# block should be completed by the SSCM/JPPS.
Each income source has a brief description.
Some descriptions request additional information be provided.
If no income exists for the source, enter N/A.
If there is income from the source, list the monthly amount prior to any withholding.
In the “recipients” column, list the name of the person to whom the income belongs.
List employer, address, and phone number if available.

*Resources Section:* This section should be completed in the same manner as the income section.
In the Household Management Section, note whether any vehicle is used as a home or as a means of income (example: delivery of newspapers, taxi), transportation to and from work and indicate if any money is owed on the vehicle.

*Household Management*: A family with no source of income is managing to survive.
Indicate what they are doing to survive (living in a shelter, visiting soup kitchens, prostituting, drug dealing, any other illegal form of employment, family providing room and board, etc.).

The form should be signed and dated by the SSCM/JPPS with a printed name and telephone number.

[.underline]#*DISTRIBUTION:*#

The original should be retained in the case record with a copy faxed to the appropriate Revenue Maximization Regional Office.

Form 224 I (Rev. 12/04)
