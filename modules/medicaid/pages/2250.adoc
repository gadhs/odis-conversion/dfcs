= 2250 Cooperation With Division of Child Support Services
:chapter-number: 2200
:effective-date: July 2023
:mt: MT-70
:policy-number: 2250
:policy-title: Cooperation With Division of Child Support Services
:previous-policy-number: MT 68

include::ROOT:partial$policy-header.adoc[]

== Requirements

The AU must cooperate with the Division of Child Support Services (DCSS) in order for AU members to receive Family Medicaid, unless Good Cause exists, or it is a Child Only case.

== Basic Considerations

Eligibility of AU members for Family Medicaid is contingent upon cooperation with DCSS, unless Good Cause exists.

A non-custodial parent (NCP) whose child receives Medicaid under any Family Medicaid COA is required to provide medical insurance for the child.

States are required to establish a program to enforce the NCP's obligation to provide support in the form of medical insurance for his/her child(ren).
Child Support Services administers this program in Georgia.

DCSS performs the following functions:

* locating the non-custodial parent(s)
* establishing legal paternity
* establishing child support orders
* collecting and distributing child support payments
* obtaining medical support orders
* participating in hearings regarding Good Cause
* reviewing the agency's Good Cause decisions

A Medicaid AU's rights to medical support are assigned by law to the state upon receipt of Medicaid.

A Family Medicaid AU must cooperate with DCSS in locating the NCP of a dependent child included in the AU, establishing legal paternity, and obtaining medical support from him/her, unless Good Cause exists.

[caption=Exceptions]
NOTE: A referral to, and cooperation with DCSS is *NOT* a requirement in the following situations:

* TMA / 4Mex
* Pathways
* Any child-only Family Medicaid case.
+
NOTE: A child-only Family Medicaid case is defined as a Medicaid AU in which no adults are receiving Medicaid under a Parent/Caretaker or Parent/Caretaker related case.
A case with an adult receiving Pregnant Woman Medicaid or SSI would be considered child only.
An AU that contains a penalized adult is *not* considered a child-only case.

* A child receiving Medicaid under Newborn Medicaid COA.
* A Parent/Caretaker case in which a parent is receiving and the only child in the AU receives SSI.
* A Parent/Caretaker case in which both parents are in the home and are the natural parents to all children in the AU.
* Pregnant women receiving Medicaid under any COA are not referred to and are not required to cooperate with DCSS for the unborn child.
* Children Under 19 eligible pregnant women are not referred to and are not required to cooperate with DCSS for an existing or unborn child.
* A minor parent who is included in a Parent/Caretaker AU as a dependent child must cooperate with DCSS if the minor parent's child is included in the AU unless the minor parent chooses to exclude his/her child.
* A minor parent is not referred to DCSS unless he/she is receiving assistance as a dependent child.
* A referral to DCSS is not required for an 18-year- old receiving Medicaid under Children Under 19 and CWFC Medicaid COAs.

* The NCP(s) of a married minor is not legally responsible for the support of a married minor and is therefore not referred to DCSS.
* The NCP of a child included in a Medicaid AU is not referred to DCSS if the NCP provides health insurance for the child.

Cooperation with DCSS includes, but is not limited to the following:

* Providing the following relevant information about the NCP in attempt to locate and obtain support from the NCP:
 ** name
 ** date of birth
 ** Social Security number
 ** current and/or former address
 ** medical insurance information
 ** employment information (current and/or former)
 ** any other information that would assist in locating the NCP.
* Attesting to the above information or attesting to the lack of information, under penalty of law.
* Submitting to a paternity test if paternity is in question.

=== Good Cause

Good Cause for failure to cooperate with DCSS may be claimed for non-cooperation with either the child support process or the medical support process.

Good Cause may be claimed at any time during the application process or following approval.

DCSS will not attempt to establish paternity or collect child and/or medical support if Good Cause is established.

When Good Cause is asserted on an active case, DCSS will suspend activity until the Good Cause determination is made.

The AU has the primary responsibility for establishing Good Cause.
The agency, however, must assist the AU in obtaining information to establish Good Cause upon request of the A/R.

Good Cause can be established if one of the following circumstances exists:

* Cooperation with DCSS would result in physical or emotional harm to the child or the A/R.
* The child was conceived as the result of rape or incest.
* Legal proceedings for the adoption of the child are pending
* The A/R is receiving assistance from a public or licensed social service agency to resolve the issue of whether to keep the child or release the child for adoption and the discussions have not pended for more than three months.

Assistance is not delayed, denied or terminated pending a determination of Good Cause if the A/R has cooperated in providing information and/or evidence in support of the Good Cause claim.

.CHART 2250 .1 – EVIDENCE NEEDED TO SUBSTANTIATE GOOD CAUSE DETERMINATION
|===
4+| GOOD CAUSE CIRCUMSTANCE | PROOF REQUIRED

4+| Physical and/or emotional harm to the
| Child Protective Services (CPS), court,

4+| child
| criminal, law enforcement, medical,

psychological, or social services records

4+|
| indicating the possibility of physical or

4+|
| emotional harm by the NCP

4+| Physical and/or emotional harm to the
| Court, criminal, law enforcement, medical

4+| grantee relative
| psychological or social services records

indicating the possibility of physical or

4+|
| emotional harm by the NCP

4+| Child conceived as a result of rape or incest
| Medical or law enforcement records indicating conception resulted from rape or incest

4+| Pending legal adoption proceedings
| Court documents or statement from social

services indicating that adoption is pending

4+| A public or private social service agency is assisting the A/R in deciding whether to keep the child or release him/her for adoption
| Written statement from the public or private social service agency assisting the A/R

^| Any of the
| above
| Good
| Cause
| Sworn statement from individual(s) with

^| circumstances
|
|
|
| knowledge of Good Cause circumstances when the above proof cannot be obtained
|===

=== Failure to Comply

If a Family Medicaid AU member fails to cooperate with DCSS without Good Cause, the adult who failed to cooperate is penalized.

== Procedures

Provide the following information to the A/R at initial application, renewal and when adding a child to the AU:

* explanation of the child support program
* assignment to the state of child support and/or medical support
* the requirement to cooperate with DCSS and the consequences for failing to cooperate
* notice to the A/R of the right to claim Good Cause at any time.

=== Form 138

Review with the A/R xref:form-138.adoc[Form 138], Notice of Requirement to Cooperate and Right to Claim Good Cause for Refusal to Cooperate in DCSS and Third Party Resource Requirements.

Obtain the A/R's signature on Form 138 and provide the A/R with a copy at initial, renewal and when adding a child to the AU.
File the signed original in the case record or scan into DIS.
If the application or renewal is completed using GA Gateway or a 94A, a 138 is not required as the 138's language is included in the GA Gateway, and 94A application and renewal.

NOTE: Form 138 may be mailed to the applicant.
The applicant is *NOT* required to sign and return the form, provided the case record is documented that the form was sent.

=== Determining Good Cause

Follow the procedures below when an A/R claims Good Cause:

*Step 1* Refer to the Chart 2250.1 in this section for types of documentary evidence needed to establish a Good Cause claim.

*Step 2* Notify the A/R of the evidence needed to establish Good Cause and establish a deadline for returning the information 20 calendar days from the date Good Cause was claimed.

NOTE: Reasonable extensions may be granted with supervisor approval.
Document the reason for the extension.

*Step 3* Notify DCSS immediately when Good Cause is asserted if an NCP had previously been referred.

NOTE: DCSS will suspend enforcement activities pending the Good Cause determination.

*Step 4* Review all information provided by the A/R and any other available evidence.

*Step 5* Request additional evidence from the A/R if necessary.
Assist the A/R in obtaining information if requested to do so.

*Step 6* Conduct an investigation if the evidence submitted by the A/R is insufficient to substantiate the Good Cause claim.
Notify the A/R in writing when such an investigation is required.

NOTE: Do not contact the NCP unless necessary to determine Good Cause.
Notify the A/R prior to contacting the NCP.

*Step 7* Base the Good Cause determination on the supporting evidence provided by the A/R and/or the information obtained during the investigation.

*Step 8* Determine Good Cause within 45 calendar days of the application or 30 calendar days at any other time.

*Step 9* Document the Good Cause determination.

*Step 10* Notify the A/R of the Good Cause determination.

=== Good Cause Established

If Good Cause *IS* established, notify the A/R that the NCP will *NOT* be referred to DCSS and that DCSS activities will be terminated if the NCP had been previously referred.

NOTE: Review the case circumstances at the next renewal if Good Cause is subject to change.

=== Good Cause Not Established

If Good Cause is *NOT* established, notify the A/R within 2 days of the decision.
Notify the A/R of the following options and allow 10 days for the A/R to choose an option:

* cooperate with DCSS
* request termination of Medicaid benefits for non-cooperating adult AU member
* request a hearing
* withdraw the application

Take appropriate action based on the decision of the A/R.

Do not impose a penalty or refer to DCSS if a hearing is requested.

Notify the A/R that Good Cause may be asserted again if circumstances change.

=== Notice of Non-Cooperation

Follow the steps below when a notice received from DCSS cites substantial evidence of the AU's non-cooperation.

*Step 1* Discuss any mitigating circumstances with the DCSS agent.

*Step 2* Determine if Good Cause exists.

*Step 3* If Good Cause is established, notify DCSS.

*Step 4* If Good Cause is not established, impose appropriate penalty, and notify the AU and DCSS.
Refer to Section xref:2657.adoc[2657], Penalized Individuals, and Section xref:2714.adoc[2714], Family Medicaid AU/BG Composition Changes.
Inform the AU of the right to request a hearing.

If a hearing is requested, include the name and address of the local DCSS agent on the hearing request.

NOTE: A penalized recipient whose case is closed and who reapplies will continue to be penalized until s/he cooperates.

Do not penalize a Medicaid A/R who fails to cooperate in obtaining child support.
A Medicaid A/R is required to cooperate in obtaining *medical support only*.
Penalize only if s/he fails to cooperate in obtaining medical support unless good cause exists.

=== Adding Penalized Individual back to AU Following Denial/ Termination Due to Non- Cooperation

Follow the steps below when an application for Family Medicaid is made for a penalized adult following denial or termination due to non-cooperation with DCSS:

*Step 1* Inform the A/R that cooperation with DCSS, prior to approval of Medicaid, is required.

*Step 2* Obtain A/R signature on the DCSS Compliance Agreement.
Provide a copy of the Agreement to the A/R and to the DCSS office assigned to the case.
File the original Agreement in the case record.

*Step 3* Inform the A/R that s/he must contact DCSS and, if deemed necessary by DCSS, schedule an appointment.

*Step 4* If DCSS notifies the agency that the A/R has cooperated, approve Medicaid for the penalized adult effective the month of compliance if otherwise eligible.
Refer to Section xref:2714.adoc[2714], Family Medicaid AU/BG Composition Changes.

*Step 5* If DCSS notifies the agency that the A/R has failed to cooperate, determine whether Good Cause for non-cooperation exists.
If it is determined that Good Cause exists, approve Medicaid for the penalized adult if otherwise eligible.

*Step 6* If it is determined that Good Cause for non-cooperation does NOT exist, deny Medicaid for the penalized adult, complete CMD to the appropriate class of assistance and notify the A/R of the decision.

NOTE: If the A/R has a non-cooperation penalty applied and the NCP for whom the non- cooperation penalty was applied moves back into the home, the DCSS referral would no longer be required, and the penalty can be lifted.
DCSS should be contacted and informed that the NCP is back in the home so they can close their case.
