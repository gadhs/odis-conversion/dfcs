= 2712 Family Medicaid Changes Overview
:chapter-number: 2700
:effective-date: July 2023
:mt: MT-70
:policy-number: 2712
:policy-title: Family Medicaid Changes Overview
:previous-policy-number: MT 69

include::ROOT:partial$policy-header.adoc[]

== Requirements

When changes occur in a Medicaid AU's and/or BG's circumstances, ongoing eligibility must be established, based on the new circumstances.

== Basic Considerations

The date the change occurs is the day the event actually happens.
Examples include the following:

* the date a first paycheck is received
* the date a paycheck reflecting a change in pay is received
* the date unearned income is first received
* the date an A/R becomes aware she is pregnant.

NOTE: This list is not all-inclusive.

Changes may be reported in any of the following ways:

* in person
* by telephone
* by mail
* by email
* by facsimile
* by Gateway “Report My Change”

* by automatic system update

AUs must report all changes within 10 calendar days of the date the change occurs.

The agency must take action, based on the change, as soon as possible but no later than 10 days after the report.

If the AU fails to report a financial change within 10 days, the agency must determine when the change should have been effective, based on the time frames specified above and allowing for timely notice, if appropriate.

Changes are effective the month after the change occurs or the second month, depending on when the AU reports the change, when DFCS takes action and when timely notice expires.

The National Voter Registration Act (NVRA) of 1993 requires that DFCS is to provide a voter registration form to the A/R when an address change is reported in person, electronically, via telephone, facsimile, or mail and would necessitate a change in the A/R's voting location, Refer to xref:ROOT:voter-registration.adoc[Section 2980], Voter Registration.

Ineligibility of an individual or an entire AU occurs the month after the required timely or adequate notice expires and a Continuing Medicaid Determination has been completed.

[caption=Exception]
NOTE: Individuals approved for Emergency Medical Assistance (EMA) have specific days of eligibility.

There is no penalty for late reporting in a Family Medicaid case.
Late reporting requires calculation and documentation of when the change should have been budgeted.
Financial changes do not affect Medicaid eligibility for pregnant women.
Refer to xref:2184.adoc[Section 2184], Pregnant Women and xref:2720.adoc[Section 2720], Continuous Coverage for Pregnant Women.

== Procedures

Follow the steps below to process changes.

*Step 1* Document the reported change.

*Step 2* Determine if the change is reported timely or untimely.
Late reporting requires calculation and documentation of when the change should have been budgeted.

*Step 3* Determine if verification is necessary.
Request that the AU provide verification within 14 days.
Refer to Chart 2712.1, Required Verification.

*Step 4* Take appropriate action based on the change reported.

*Step 5* Review all electronic data sources and information known to the agency, and determine if an administrative renewal can be completed without further contact with the AU.
Refer to Section 2706, Medicaid Renewals.

*Step 6* Provide the AU with appropriate notice of action taken.
Refer to xref:2701.adoc[Section 2701],Notification.

Use the chart below to determine if verification is required when an AU reports a change.

.CHART 2712.1 - REQUIRED VERIFICATION
|===
| CHANGE | FAMILY MEDICAID

| income - new source or change in amount
| client statement, unless questionable for Pregnant Woman Medicaid and Deemed Newborn Medicaid *

For all other COAs, self- attestation verified by electronic data sources or other information known to the agency must be used to the maximum extent possible.
Refer

to xref:2051.adoc[Section 2051], Verification.

| resources (vehicle, real property, life insurance, etc.) – acquisition, sale of, etc.
| For MAGI COAs, resources are not applicable.
For non-MAGI COAs, client statement, unless questionable *

The value of all vehicles must be verified.
Refer to xref:2308.adoc[Section 2308], Vehicles.
Resources must be verified if the total of all liquid and non-liquid resources exceeds 75% of the total resource limit.
Refer to xref:2301.adoc[Section 2301], Family Medicaid Resources Overview.

| birth of a baby
| client statement, unless questionable *

| decrease in AU or BG size
| client statement, unless questionable *

MAGI Medicaid requires tax filing status to be established, document tax status statement.

| increase in AU or BG size
| A new AU member who is a U.S. citizen must provide verification of his/her citizenship/qualified immigrant status when SVES is not available.
A new BG member is NOT required to verify citizenship/ qualified immigrant status or identity.

A new AU member who is not a U.S. citizen must provide verification of his/her immigration status in order to confirm on WEB1.
A new BG member is NOT required to verify immigration status.
Refer to xref:2215.adoc[Section 2215], Citizenship/Immigration status/Identity.
|===

.CHART 2712.1 - REQUIRED VERIFICATION
|===
| CHANGE | FAMILY MEDICAID

|
| MAGI Medicaid requires tax status to be established, document tax status statement.

| dependent care costs
| client statement, unless questionable (non- MAGI COAs only)

| medical expenses
| yes (Medically Needy only)

client statement, unless questionable is acceptable for all other Family Medicaid COAs for prior months requested, verification of medical expenses is not required.

| pregnancy due date and the number of expected births
| client statement, unless questionable * Refer to xref:2184.adoc[Section 2184] Pregnant Woman Medicaid

| residence
| client statement, unless questionable * Refer to xref:2713.adoc[Section 2713].

| Change in tax status
| Statement is accepted, verification of tax return is not required for expected tax filer household (MAGI Medicaid only).
For non- tax filer households, statement is accepted (MAGI Medicaid only)

| Pre-tax or 1040 deductions (MAGI Medicaid only)
| Verification is required when not available via data sources or related active cases

| Changes in Qualifying Hours and Activities (Pathways)
| Electronic data sources may be used when applicable.
Third party verification must be received if unable to verify with data sources.
Refer to Section 2256, GA Pathways Qualifying Activities Reporting.
|===

{asterisk} If verification is requested, the worker must document WHY the situation was considered questionable.

Use the following chart to determine procedures when an AU fails to provide verification.

.2712.2 - FAILURE OF A FAMILY MEDICAID AU TO PROVIDE VERIFICATION
|===
| IF THE AU FAILS TO PROVIDE REQUESTED VERIFICATION OF: | THEN

| Questionable income that can't be verified by other sources (new source or change in amount)

or

change in resources (acquisition, sale, etc.) for non-MAGI COA

or

questionable increase or decrease in AU and/or BG size

or

questionable change of residence
| terminate Medicaid effective the month following the expiration of timely notice.

If all requested verification is returned within 90 days of termination (last day of the third month following termination) the case should be reinstated back to the first month following the month in which timely notice expired.

| new medical expense
| do not allow the medical expense in the Medically Needy spenddown calculation.

| questionable change in dependent care expense (non-MAGI COAs only)
| remove the original dependent care expense deduction and do not allow the new expense.

| Pre-tax or 1040 deductions (MAGI Medicaid only)
| Do not allow the new deductions or remove the previously allowed deductions
|===
