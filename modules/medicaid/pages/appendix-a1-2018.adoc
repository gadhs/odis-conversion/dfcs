= Appendix A1 ABD Financial Limits 2018
:chapter-number: Appendix A1
:effective-date: N/A
:mt: MT-54
:policy-number: Appendix A1
:policy-title: ABD Financial Limits
:previous-policy-number:

.CHART A1.1 - ABD MEDICAID RESOURCE LIMITS
|===
| Type Limit | Individual Limit | Couple Limit | LA-D Individual With a Community  Spouse | Effective Date

| SSI/LA-D
>| $2000
^| $3000
^| N/A
>| 7-88

| AMN
>| $2000
^| $4000
^| N/A
>| 4-90

| QMB/SLMB/ QI-1
>| $7560
^| $11,340
^| N/A
>| 1-18

| QDWI
>| $4000
^| $6000
^| N/A
>| 1-89

| Spousal
>| N/A
^| N/A
| $123,600 + 2000 =
>| 1-18

| Impoverishment
|
|
| $125,600.00
|
|===

.CHART A1.2 - ABD MEDICAID NET INCOME LIMITS (GROSS - $20)
|===
| Type Limit | LA | Individual Limit | Couple Limit | Effective Date

^| *AMN*
^| All
| $317
^| $375
^| 10-90

.4+^| *FBR*

*(SSI Limit)*
^| A
| $750
^| $1125
.4+^| 1-18

^| B
| $500
^| $750

^| C
| $750
^| N/A

^| D
^| $30
^| N/A

^| *Medicaid CAP*
^| D
| $2250
^| $4500*
| 1-18* corrected in 12/2018

.3+| *QDWI*
^| A
| $4133
^| $5573
.3+a|
3-18

NOTE: Effective 3-98, ISM no +
longer applies to +
this COA eliminating LA-B.

^| C
| $4133
^| N/A

^| D
| $4133
^| N/A

^| *QMB*
^| A
| $1012
^| $1372
^| 4-18

^| *SLMB*
^| A
| $1214
^| $1646
^| 4-18

^| *QI-1*
^| A
| $1366
^| $1852
^| 3-18
|===

.CHART A1.3 - TRANSFER OF RESOURCE PENALTY DETERMINATION
[cols=3*]
|===
| Averaging Nursing Home Private Pay Billing Rate
| $6707.00
^| 4-18
|===

.CHART A1.4 - PRESUMED MAXIMUM VALUE (PMV) OF ISM AND LIVING ALLOWANCE TO EACH INELIGIBLE CHILD
|===
| Income Limit | PMV for an Individual | PMV for a Couple | Living Allowance | Effective Date

^| *AMN*
^| $270.00
^| $395.00
>| $375.00
>| 1-18

^| *FBR*
^| $270.00
^| $395.00
>| $375.00
>| 1-18

^| *QMB*
^| N/A
^| N/A
>| $477.00
>| 4-18

^| *SLMB*
^| N/A
^| N/A
>| $568.66
>| 4-18

^| *QI-1*
^| N/A
^| N/A
>| $637.24
>| 3-18
|===

4

.CHART A1.5 - SUBSTANTIAL GAINFUL ACTIVITY
|===
| Category | Income Limit | Effective Date

^| Non-Blind individuals
^| $1180
.2+^| 1-18

^| Blind individuals
^| $1970
|===

.CHART A1.6 – BREAK-EVEN POINTS
[cols=6*]
|===
.2+h| Living Arrangement
2+h| Earned Income
2+h| Unearned Income
.2+h| Effective Date

^h| Individual
^h| Couple
^h| Individual
^h| Couple

^| *A*
^| $1271
^| $1873
^| $603
^| $904
.2+^| 1-06

^| *B*
^| $869
^| $1271
^| $402
^| $603

^| *D*
^| $145
^| $205
^| $50
^| $80
^| 7-88
|===

.CHART A1.7 – MONTHLY AVERAGED MEDICAID RATES FOR KATIE BECKETT
|===
| Level of Care | Monthly Amount | Effective  Date

^| Skilled Nursing Facility
^| $5,629.91 (31 days)
.3+| 04/18

^| ICF/MR
^| $20,614.30 (31 days)

^| Hospital
^| $5,680.95
|===

[.text-center]
*A1.8 – MEDICARE EXPENSES*

Medicare Part B Premium rate: $104.90 (effective 1-14) +
$121.80 (effective 1-16) +
$134.00 (effective 2017 and 2018) +
Effective 01/2016 Medicare Part B Premium rates may vary check BENDEX for applicable rate.

.CHART A1.9 - PERSONAL NEEDS ALLOWANCES (PNA) FOR AN LA-D RECIPIENT
|===
| IF the LA-D Recipient is 2+| THEN use the following as the PNA in the Patient Liability/Cost Share Budget:

| an individual in a nursing home or Institutionalized Hospice
^| $65
^| Effective 7-18

| a VA pensioner or his/her surviving spouse in a nursing home who has dependents
^| $65
^| Effective 7-18

a|
a VA pensioner or his/her surviving spouse in a nursing home who has no dependents

NOTE: The VA check for these individuals is +
reduced to the amount of the PNA, regardless of other income.
^| $90
^| Effective 1-92

(Effective 1-93 for the Surviving Spouse)

| an individual in CCSP
2+| the current amount of the Individual FBR for LA-A

| an individual in ICWP
2+| the current amount of the Community Spouse Maintenance Need Standard

| an individual in NOW/COMP
2+| the current Medicaid Cap
|===

.CHART A1.10 - NEED STANDARDS FOR DIVERSION OF INCOME TO A COMMUNITY SPOUSE OR DEPENDENT FAMILY MEMBER IN A PATIENT LIABILITY/COST SHARE BUDGET
|===
| Diversion Standard | Amount | Effective  Date

| Community Spouse Maintenance Need Standard
^| $3090.00
^| 1-18

| Dependent Family Member Need Standard
^| $2058.00
^| 4-18
|===

|===
| HOUSEHOLD  SIZE | 100% | 135% | 150% | EFF.  DATE

^| 1
^| $12,140.00
^| $16,389.00
^| $18,210.00
.5+^| 2018

^| 2
^| 16,460.00
^| 22,221.00
^| 24,690.00

^| 3
^| 20,780.00
^| 28,053.00
^| 31,170.00

^| 4
^| 25,100.00
^| 33,885.00
^| 37,650.00

^| 5
^| 29,420.00
^| 39,717.00
^| 44,130.00
|===

The FPL (100% level) is increased by $4,320 for each additional person in the household.

.CHART A1.12 – COSTS AND GUIDELINES FOR RECEIPT OF MEDICARE PART D - LOW INCOME SUBSIDY
|===
| | Group 1 | Group 2 | Group 3 | Eff.
Date

^| *Resource Limit*
^| None
^| Non Q Track Individual -

$9,060

Non Q Track Couple -

$14,340
| Individual - $14,100 Couple - $28,150
.6+| 2018

^| *Income Limit*
^| Full

Medicaid
^| Q Track

or

Less than 135% of FPL
^| Less than 150% of FPL

| *Monthly*

*Premium*
^| $0
^| $0
^| Sliding Scale

| *Deductible*

*Per Year*
^| $0
^| Up to $83.00
^| Up to $83.00

| *Coinsurance*

*up to $3600 Out of Pocket*
| $1.25 -

$3.70

Copay
^| $3.35 - $8.35 Copay
^| 15% Coinsurance

| *Catastrophic*

*5% or $2/$5 Copay*
^| $0
^| $0
^| $3.35 - $8.35 Copay
|===

[.text-center]
*Low-Income Part D Premium Subsidy Amount* +
2010 – 29.62 +
2011 – 32.83 +
2012 – 31.18 +
2013 – 34.22 +
2014 – 29.32 +
2015 – 26.47 +
2016 – 25.78 +
2017 – 26.43 +
2018 – 24.53

[.text-center]
*A1.13 – Medically Needy Mileage Re-imbursement Rate* +
48.5 cents per mile – 09/10/05 – 12/31/05 +
44.5 cents per mile – 01/01/06 – 01/31/07 +
48.5 cents per mile – 02/01/07 – 03/31/08 +
50.5 cents per mile – 04/01/08 – 07/31/08 +
58.5 cents per mile – 08/01/08 – 12/31/08 +
55.0 cents per mile – 01/01/09 – 12/31/09 +
50.0 cents per mile – 01/01/10 – 12/31/10 +
51.0 cents per mile – 01/01/11 – 04/16/12 +
55.5 cents per mile – 04/17/12 – 12/31/12 +
56.5 cents per mile – 01/01/13 – 12/31/13 +
56.0 cents per mile - 01/01/14 – 12/31/14 +
57.5 cents per mile – 01/01/15 – 12/31/15 +
54.0 cents per mile – 01/01/16 – 12/31/2016 +
53.5 cents per mile – 01/01/17 - 12/31/2017 +
54.5 cents per mile – 01/01/2018 - present
