= 2503 Couples
:chapter-number: 2500
:effective-date: February 2020
:mt: MT-58
:policy-number: 2503
:policy-title: Couples
:previous-policy-number: MT 51

include::ROOT:partial$policy-header.adoc[]

== Requirements

The income and resources of a Medicaid Couple are considered jointly in determining ABD Medicaid eligibility as long as they live together in LA-A or B.

== Basic Considerations

If the Couple resource limit is used to determine resource eligibility, the countable resources of both spouses are combined and applied to the appropriate Couple resource limit.

If the Spousal Impoverishment resource limit is used to determine resource eligibility, the countable resources of both spouses are combined and applied to the Spousal Impoverishment resource limit.
Refer to Determining Resources when an A/R with a Community Spouse Enters LA-D in the Procedures portion of Section 2502, Deeming.

If a Couple budget is completed to determine income eligibility, the combined countable income of both spouses and the appropriate Couple income limit are used to complete the budget.
Refer to Section 2509, Couple Budgeting.

If a spouse-to-spouse deeming budget is used to determine income eligibility, both the appropriate Individual and Couple income limits are used to complete the budget.
Refer to Section 2507, Spouse to Spouse Deeming.

[caption=Exception]
NOTE: Effective July 1, 2016 the couple income and resource limit for the individual and the individual's spouse will be used whether or not the spouse is applying (Medicare Eligible) for the QI1 COA.

If a Medicaid CAP budget is used to determine income eligibility, the Medicaid CAP is the income limit used to complete the budget.
Refer to Section 2510, Medicaid CAP Budgeting.

== Procedures

*Determining Financial Eligibility for a Medicaid Couple Living Together in LA-A* +
*Or LA-B*

Follow the procedures below to determine financial eligibility for a Medicaid Couple living together in LA-A or B and applying under the same class of assistance (COA):

* Combine the countable resources of both spouses and apply them to the appropriate Couple resource limit to determine resource eligibility.
* Complete a Couple budget using the combined gross countable unearned and earned income of both spouses to determine income eligibility.
Use the appropriate Couple income limit.

*Deeming Financial Eligibility when Both Spouses enter LA-D*

*Resource Eligibility*

* Combine the countable resources of both spouses and apply them to the Couple resource limit to determine resource eligibility for the month of admission to LA-D.
* Treat each spouse as an Individual in determining resource eligibility beginning the month following the month of admission to LA-D.
Apply each Individual's resources to the Individual resource limit.

*Income Eligibility*

* If both spouses have eligibility determined under a COA that uses the Medicaid CAP as the income limit, treat each spouse as an Individual when determining income eligibility beginning with the month of admission to LA-D.
In some instances, a couple Medicaid Cap budget is appropriate.
Complete an individual or couple Medicaid CAP budget.
Refer to Section 2510, Medicaid CAP Budgeting, for specifics
* If both spouses have eligibility determined as ABD Medically Needy, complete a Couple budget for the month of admission to LA-D.
Complete an Individual budget on each spouse beginning the month after the month of admission to LA-D.
Refer to Section 2506, Medicaid Individual Budgeting.

Use the following chart to determine treatment of income and resources when both spouses are Medicaid and/or Q Track eligible and have eligibility determined under different classes of assistance.
Consider Spouse A to be residing at home in LA-A or B:

.Chart 2503.1 - Financial Responsibility for Medicaid Couples
|===
| | SPOUSE A | SPOUSE B

| CLASS OF

ASSISTANCE:

Resource:

Income:
^| *AMN*

Use the AMN Couple

resource limit.

Complete a Couple budget.
^| *SSI*

Use the SSI Couple resource limit

Complete a Spouse-to-Spouse Deeming budget.

| CLASS OF

ASSISTANCE:

Resource:

Income:
^| *AMN*

Use the AMN Couple

resource limit any month in

which Spouse B is in the

home at least one day.

Complete a Couple budget if the couple are together at home on the first day of the month.
^| *CCSP, NOW/COMP, ICWP, Hospital or Hospice at home*

If Spouse A is a community spouse, use the Spousal Impoverishment resource limit.

If Spouse A is a non-legal spouse, use the SSI Couple resource limit through the month of admission to the hospital, hospice or CCSP, etc.
Use the SSI Individual resource limit beginning the month after the month of admission.

Complete a Medicaid CAP budget for A/Rs whose income is under the Cap or who have established a Qualified Income Trust (QIT).

|
^| *SPOUSE A*
^| *SPOUSE B*

| CLASS OF ASSISTANCE:

Resource:

Income:
^a|
[.text-center]
*AMN*

Use the AMN Couple resource limit through the month of admission of Spouse B to the nursing home (NH).
Use the AMN Individual resource limit beginning the month after the month of admission.

Complete a Couple budget through the month of admission of Spouse B to the NH.
Complete an Individual budget beginning the month after the month of admission.

NOTE: Include any income diverted from Spouse B in the Individual budget.
^| *Nursing Home/Institutionalized*

*Hospice*

If Spouse A is a community spouse, use the Spousal Impoverishment resource limit.

If Spouse A is a non-legal spouse, use the SSI Couple resource limit through the month of admission to the nursing home (NH).
Use the SSI Individual resource limit beginning the month after the month of admission.

Complete a Medicaid CAP budget for A/Rs whose income is under the Cap or who have established a Qualified Income Trust (QIT).

| CLASS OF

ASSISTANCE:

Resource:

Income:
^| *AMN*

Use the AMN Couple resource limit.
Also, refer to Special Considerations at the end of this section.

Complete a Couple budget

Also refer to Special

Considerations at the end of this section.
^| *AMN IN HOSPITAL*

Use the AMN Couple resource limit.
Also, refer to Special Considerations at the end of this section.

Complete a Couple budget.
Also refer to Special Considerations at the end of this section.

|
^| *SPOUSE A*
^| *SPOUSE B*

| CLASS OF

ASSISTANCE:

Resource:

Income:
^| *AMN*

*(QMB/SLMB eligible)*

Use the AMN Couple resource limit.

Complete a Spouse-to-

Spouse Deeming budget.
^| *Q Track*

Use the Q Track Couple Resource limit.

Complete a Couple budget.

| CLASS OF ASSISTANCE:

Resource:

Income:
^| *AMN*

*(not Q Track eligible)*

Use the AMN Couple resource limit.

Complete a Couple budget.
^| *Q Track*

*(QMB/SLMB eligible)*

Use the Q Track Couple resource limit.

Complete a Spouse-to-Spouse Deeming budget.

*(QI1 eligible)*

Use the QI1 couple income and Q Track couple resource limit.

| CLASS OF ASSISTANCE:

Resource:

Income:
^| *Public Law*

Use the SSI Couple resource limit.

Complete a Spouse-to-Spouse Deeming budget.
^| *Q Track*

Use the Q Track Couple resource limit.

Complete a Spouse-to-Spouse Deeming budget.
If Spouse A is ineligible for Q Track.

OR

Complete a Q Track Couple budget, if Spouse A is QMB or SLMB eligible.

|
^| *SPOUSE A*
^| *SPOUSE B*

| CLASS OF

ASSISTANCE:

Resource:

Income:
| *Public Law*

Use the SSI Couple resource limit through the month of admission of Spouse B to the NH.
Use the SSI Individual resource limit beginning the month after the month of admission.

Complete a Couple budget through the month of admission of Spouse B to the NH.
Complete an Individual budget beginning the month after the month of admission.
| *Nursing Home/Institutionalized*

*Hospice*

If Spouse A is a community spouse, use the Spousal Impoverishment resource limit.

If Spouse A is a non-legal spouse, use the SSI Couple resource limit through the month of admission to the NH.
Use the SSI Individual resource limit beginning the month after the month of admission.

Complete a Medicaid CAP budget for A/Rs whose income is under the Cap or who have established a Qualified Income Trust (QIT).

| CLASS OF

ASSISTANCE:

Resource:

Income:
| *Public Law*

Use the SSI Couple resource limit.
Also refer to Special Considerations at the end of this section.

Complete a Couple budget.
Also refer to Special Considerations at the end of this section.
| *CCSP, NOW/COMP, ICWP, Hospital or Hospice at home*

If Spouse A is a community spouse, use the Spousal Impoverishment resource limit.

If Spouse A is a non-legal spouse, use the SSI Couple resource limit through the month of admission to the hospital, hospice or CCSP, etc.
Use the SSI Individual resource limit beginning the month after the month of admission.

Complete a Medicaid CAP budget for A/Rs whose income is under the Cap or who have established a Qualified Income Trust (QIT).

|
^| *SPOUSE A*
^| *SPOUSE B*

| CLASS OF

ASSISTANCE:

Resource:

Income:
^| *Q Track*

Use the Q Track Couple resource limit.
Also refer to Special Considerations at the end of this section.

If Spouse B is Q Track eligible, complete a Couple budget.

If Spouse B is not Q Track

eligible (no Medicare),

complete a Spouse-to-Spouse Deeming budget.

If the couple is not Q track

eligible because of Spouse B's

income, complete an

individual budget for Spouse

A, not including any income

diverted to Spouse A from

Spouse B.

Refer to Special

Considerations at the end of

this section.
^| *CCSP, NOW/COMP, ICWP, Hospital or Hospice at home*

If Spouse A is a community spouse, use the Spousal Impoverishment resource limit.

If Spouse A is a non-legal spouse, use the SSI Couple resource limit through the month of admission to LA-D.
Use the SSI Individual resource limit beginning the month after the month of admission.

Complete a Medicaid CAP budget for A/Rs whose income is under the Cap or who have established a Qualified Income Trust (QIT).

|
^| *SPOUSE A*
^| *SPOUSE B*

| CLASS OF

ASSISTANCE:

Resource:

Income:
^| *Q Track*

Use the Q Track Couple resource limit.
Also, refer to Special Considerations at the end of this section.

If Spouse B is Q Track eligible, complete a Couple budget.

If spouse B is not Q Track

eligible (no Medicare),

complete a Spouse-to-Spouse Deeming budget.

If the couple is not Q track

eligible because of Spouse B's

income, complete an

individual budget for Spouse

A, not including any income

diverted to Spouse A from

Spouse B.

Refer to Special

Considerations at the end of

this section.
^| *AMN in Hospital*

Use the AMN Couple resource limit.
Also, refer to Special Considerations at the end of this section.

Complete a Spouse-to-Spouse Deeming budget if LOS is met.
Also, refer to Special Considerations at the end of this Section.

If LOS is not met, complete a couple AMN budget.

|
^| *SPOUSE A*
^| *SPOUSE B*

| CLASS OF

ASSISTANCE:

Resource:

Income:
^| *Q Track*

Use the Q Track Couple resource limit through the

month of admission of Spouse

B to the NH.
Use the Q Track

Individual resource limit

beginning the month after the

month of admission.

If Spouse B is Q Track eligible,

complete a Couple budget

through the month of

admission of Spouse B to the

NH.
Complete an Individual budget beginning the month after the month of admission.

If Spouse B is not Q Track

eligible (no Medicare),

complete a Spouse-to-Spouse Deeming budget through the month of admission.

Complete an Individual budget beginning the month after the month of admission.
^| *AMN (S99) in Nursing Home/Hospice in NH*

Use the AMN Couple resource limit the month of admission.
Use the AMN individual limit afterwards.
Also, refer to Special Considerations at the end of this section.

Complete a Spouse-to-Spouse Deeming budget the month of admission.
Afterwards complete an individual AMN budget.
Also, refer to Special

Considerations at the end of this Section.
Remember there will be no vendor

payment to the NH.

|
^| *SPOUSE A*
^| *SPOUSE B*

| CLASS OF ASSISTANCE:

Resource:

Income:
^| *Q Track*

Use the Q Track Couple resource limit through the month of admission of Spouse B to the NH/IH.
Use the Q Track Individual resource limit beginning the month after the month of admission.

If Spouse B is Q Track eligible, complete a Couple budget through the month of admission of Spouse B to the NH.
Complete an Individual budget beginning the month after the month of admission.

If Spouse B is not Q Track eligible (no Medicare), complete a Spouse-to-Spouse Deeming budget through the month of admission.
Complete an Individual budget beginning the month after the month of admission.
^| *Nursing Home/Institutionalized Hospice*

If Spouse A is a community spouse, use the Spousal Impoverishment resource limit.

If Spouse A is a non-legal spouse, use the SSI Couple resource limit through the month of admission to the NH.
Use the SSI Individual resource limit beginning the month after the month of admission.

Complete a Medicaid CAP budget for A/Rs whose income is under the Cap or who have established a Qualified Income Trust (QIT).

| CLASS OF ASSISTANCE:

Resource:

Income:
^| *Q Track*

*(not SSI eligible)*

Use the Q Track Couple resource limit.

If Spouse B is Q Track eligible, complete a Couple budget.

If Spouse B is not Q Track eligible (no Medicare).

Complete an Individual

budget.
^| *SSI*

Use the SSI Couple resource limit.

Complete a Spouse-to-Spouse Deeming budget.
|===

== Special Considerations

*Hospitalization Treated as a Temporary Absence*

Hospitalization is generally considered to be a temporary absence from the home.

* The hospitalized spouse is considered to be living in the home with his/her Medicaid spouse during the hospital confinement.
* The hospitalized spouse's income and resources are considered when determining the Medicaid eligibility of the Medicaid spouse remaining at home.
* Refer to Chart 2503.1, Financial Responsibility for Medicaid Couples.

*Hospitalization Treated as Full Separation of Spouses*

Do not consider the income and resources of the hospitalized spouse beyond the month of hospital admission when determining the Medicaid eligibility of his/her spouse at home if the hospitalized spouse meets the length of stay (LOS) basic eligibility requirement

The LOS requirement may be met by the hospitalization, or the hospitalization and a subsequent stay in another LA-D situation.
Use this rule on separation even if the hospitalized spouse's eligibility is determined under ABD Medically Needy.

If the hospitalized/LA-D spouse returns home to live with his/her spouse after meeting the LOS requirement, resume considering the income and resources of the spouses jointly for purposes of determining Medicaid eligibility effective with the month following the month the hospitalized/LA-D spouse returns home.

NOTE: The income of the spouse is not considered if eligibility is determined under a COA that uses the Medicaid CAP as the income limit.
Refer to Section 2510, Medicaid CAP Budgeting.

NOTE: Use the Spousal Impoverishment resource limit in determining the hospitalized/LA-D spouse's eligibility if the conditions for a full separation of spouses are met as outlined above.
