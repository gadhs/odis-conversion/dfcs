= 2818 Chafee Independence Program Medicaid
:chapter-number: 2800
:effective-date: November 2020
:mt: MT-62
:policy-number: 2818
:policy-title: Chafee Independence Program Medicaid
:previous-policy-number: MT 48

include::ROOT:partial$policy-header.adoc[]

== Requirements

The Foster Care Independence Act allows Medicaid coverage to be extended to individuals who age out of foster care the month of their 18th birthday up until their 21st birthday. +
Applicants may not be determined ineligible based on a diagnosis or pre-existing condition.

== Basic Considerations

Chafee Independence Program Medicaid became effective July 1, 2008.
Former foster youth may apply for Chafee Independence Medicaid through the Gateway Customer Portal or at any Division of Family and Children Services (DFCS) office.

Reference Section xref:2890.adoc[2890] – Foster Care Medicaid Age 18-21 for children in placement for whom DFCS has partial or total custody and may be age 18 to 21.

The eligibility month is the month following a foster child's 18th birthday or the month a former foster child over the age of 18 and under the age of 21 applies for the Chafee Independence Program Medicaid.

=== Basic Eligibility Criteria

The following basic eligibility criteria must be met to qualify for Chafee Medicaid:

* Age: A youth must have been in foster care the month of his/her 18th birthday and be under the age of 21;
self – attestation is accepted for verification of entry into foster care.

* Enumeration;.
+
NOTE: Enumeration is not a requirement for Emergency Medical assistance (EMA).
Refer to Section xref:2054.adoc[2054], EMA;
Reference Section xref:2220.adoc[2220], Enumeration.

* Residency, Reference Section xref:2225.adoc[2225], Residency;

* Citizenship/Immigration Status/Identity;
+
NOTE: Individuals who were in foster care under Title IV-B or Title IV-E of the Social Security Act are exempt from providing additional documentary evidence of citizenship/immigration status/identity as long as they were in Foster Care in Georgia.

* Third Party Liability, Reference Section xref:2230.adoc[2230], TPL;

* Application for other benefits, Reference Section xref:2210.adoc[2210], Application for Other Benefits.

There are *no* income or resource limits for Chafee Medicaid.

=== Other Considerations

Chafee Medicaid Cases are reviewed on an annual basis until the recipient turns 21.
Chafee Independence recipients are eligible for retroactive Medicaid EMA is available under Chafee Independence Medicaid.

Chafee recipients are enrolled in Amerigroup as their CMO enrollment.

Non-MAGI Family Medicaid recipients who are former foster children may request a Class of Assistance change to Chafee Independence Medicaid.

== Procedures

=== Continuing Medicaid Determination

Rev Max will complete a continuing Medicaid determination (CMD) following the steps below for a current foster child in the month of their 18th birthday:

* For eligibility month following the month of individual's 18th birthday:
 ** Verify that individual was in a state's legal custody on his or her 18th birthday.
 ** For ICPC placements in Georgia, verify through Independent Living Regional Coordinators the foster care status in original state of custody.

NOTE: Written verification must be maintained in the case record.
An ICPC placement in Georgia aging out of foster care and remaining in Georgia is considered a Georgia resident.

* Verify that individual will not be eligible for Child Welfare Foster Care Medicaid criteria
 ** Accept the client's statement as to Georgia residency
 ** Complete the DMA 285 – Third Party Liability NOTE: There is no income or resource test for Chafee Medicaid.

=== New Applications

For determining eligibility for Chafee Medicaid on new Gateway applications, Rev Max Specialist (RMS) complete the following steps:

* Verify that the individual was in foster care on his or her 18th birth day by screening in SHINES;

* Self-attestation of receipt of Foster Care in another state at the time the youth turned 18 or aged out of the foster care system is acceptable.

* Assume the individual who ages out of care with the State of Georgia meets citizenship/immigration status/identity unless information to the contrary is known to the agency.
No additional citizenship/immigration status or identity documents are required.
+
NOTE: Individuals who were in foster care under Title IV-B or Title IV-E of the Social Security Act are exempt from providing additional documentary evidence of citizenship/immigration status/identity as long as they were in Foster Care in Georgia.

* Verify citizenship/immigration status/identity for individuals who were in foster care in other states and request Chafee Medicaid in Georgia after their 18th birthday.

* Accept the client's statement as to Georgia residency

* Complete the DMA 285 – Third Party Liability

NOTE: There is no income or resource test for Chafee Medicaid.

=== Ongoing Cases

Chafee Medicaid cases are hard case owned by Revenue Maximization staff with no other Gateway benefit cases associated with the Chafee Medicaid case.

Chafee Medicaid Cases are required to have an annual review.

NOTE: Court orders and existing foster care documentation must be retained in the closed foster care case record and may NOT be cleansed.

=== Case Records For Children with Expired Eligibility

*Closed Cases – Foster Care* case records for recipients for whom eligibility has expired, +
i.e. where the recipient has turned age 26, should be maintained in Rev Max closed files following the retention schedule found in Section xref:2760.adoc[2760] - Case Record Maintenance.
Court orders and existing foster care documentation must be retained in the Chafee Medicaid case record and may not be cleansed.

*Retention of Case Records* – Due to IV-E Foster Care and Medicaid regulations, Chafee Medicaid case records must be retained in their entirety (both IV-E FC material and Medicaid material) for a period of three years from the recipient's 26th birthday (i.e. no earlier than the recipient's 29th birthday).
