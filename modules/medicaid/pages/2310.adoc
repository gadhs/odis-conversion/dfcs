= 2310 Bonds- U.S. Savings
:chapter-number: 2300
:effective-date: July 2022
:mt: MT-65
:policy-number: 2310
:policy-title: Bonds- U.S. Savings
:previous-policy-number: MT 59

include::ROOT:partial$policy-header.adoc[]

== Requirements

The resource value of a U.S. Savings Bond is its current market value (CMV).

== Basic Considerations

U.S. Savings Bonds are obligations of the federal government.
Unlike other bonds, they are not transferable.
They can only be sold back to the federal government.

If bonds are owned jointly, co-owners own equal shares of the value of the bond.

A U.S. Savings Bond is not a resource to a co-owner if another co-owner has and will not relinquish possession of it.

== Procedures

*All* saving bonds are a countable resource.
Although the minimum retention period for several savings bond series is 12 months (Series EE bonds, Series I bonds and Series HH bonds), the minimum retention period may be waived easily if the owner/family member is experiencing an illness, has need of institutional care, etc.
The owner of the Bond would contact the Bureau of Fiscal Service regarding the hardship and the owner will be issued a refund of the Bond.
Any A/R or deemor who owns such a Bond is required to request such a waiver.
Obtain verification that the request of a refund due to hardship has been made to the Bureau of Fiscal Service.
Count the value of the Bond unless proof is provided that the request of the refund was denied.
Use the Table of Redemption Values for U.S. Savings Bonds to determine value.

Purchase of the bonds is not considered a transfer of resources, because the buyer is purchasing the bonds at fair market value, and if the bonds are held to maturity, the buyer will get the full return on the investment.

If the table is not available, obtain the value by telephone or in writing from a local bank.
The bank will need the series, denomination, and date of purchase and/or issue date. +
Document the case.

If the individual alleges that he or she cannot submit a bond because a co-owner has and will not relinquish physical possession of it, obtain a signed statement from the co-owner.

=== ABD Medicaid

Determine the CMV, if any, as of the first moment of the first day of the month.

=== Family Medicaid

Accept the A/R's statement of CMV, unless the total of all resources exceeds 75% of the resource limit.
If the total resources exceeds 75% of the resource limit, the amount must be verified.
