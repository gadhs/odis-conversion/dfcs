= 2860 IV-E Reimbursability
:chapter-number: 2800
:effective-date: January 2021
:mt: MT-63
:policy-number: 2860
:policy-title: IV-E Reimbursability
:previous-policy-number: MT 45

include::ROOT:partial$policy-header.adoc[]

== Requirements

The determination that a child is IV-E reimbursable qualifies the State to obtain federal IV-E funding for maintenance costs (board and care) associated with the child.

== Basic Considerations

Title IV-E reimbursability may fluctuate from month to month.
A child may lose and regain IV- E reimbursability depending on changes in the child's income and resources, the circumstance in the placement, or in obtaining the required judicial determinations while the child remains in DFCS custody or the custody of another public agency under contract with Georgia Department of Human Services.
The loss of IV-E reimbursability does not deprive the child of future IV-E reimbursability once the reimbursability criteria are met again.

The following criteria must be met for a child to be IV-E reimbursable:

* child is under the age of 18.

* child meets financial need criteria (based on only the child's income and resources once initial IV-E eligibility has been established).

* child resides in an IV-E reimbursable placement;

* child is in the custody of DFCS or another public agency under contract with Georgia Department of Human Services.

* there is a judicial determination of reasonable efforts to finalize the child's permanency plan that is in effect within 12 months of the child's removal and at least every 12 months thereafter while the child is in foster care.

* for those children in DFCS care under a VPA, Federal financial participation is claimed only for voluntary foster care maintenance expenditures within the first 180 days of a child's placement in foster care unless there has been a judicial determination of a _best interest_ or _contrary to the welfare_ by a

court: otherwise the child is IV-E reimbursable for the first 180 days only.

NOTE: Social Services has a 90 day requirement that must be met.
The only eligibility requirement is by the 180th day.
The 90 day requirement applies only to Social Services.

Any time that reimbursability is lost, the child is reclassified as CWFC (IV-B) effective the first day of the month following the month in which the change occurred.

If the child regains reimbursability, the child is reclassified as IV-E effective the first day of the month in which the change occurred.

[caption=Exception]
NOTE: A change in placement affects the reimbursability as of the date of the change.

A child cannot receive two IV-E payments for the same day.
If a child is moved from one IV- E reimbursable placement to another the same day, the IV-E payment is made to the home where the child “spent the night”.
The SSCM's judgment as to where the child spends the night is accepted.

Another situation that requires consideration of payment sources is when a child is in a concurrent placement.
A concurrent placement is defined as follows: It is the planned, purposeful absence of a child from his original foster home/facility which continues to be paid at the same time his temporary payment is made.

If and when you are notified of a concurrent payment to another IV-E placement, the concurrent payment must be made from IV-B funds while the IV-E per diem to the original placement continues.
It is the responsibility of the SSCM notify the assigned RMS through a SHINES Notification of Change (NOC).
When the information is provided the RMS will complete the required Form 529 to authorize payment from IV-B funds to a concurrent placement.

[caption=Exception]
NOTE: A change in placement affects the reimbursability as of the date of change.
The standard of promptness for changes is 10 days.

=== IV-E Reimbursable Placement

Federal regulations, effective March 27, 2000 require that a foster family home (relative or non-relative) and a residential childcare facility must meet the standards for full approval as a foster family or residential childcare facility.
_Temporary approvals of foster families or residential childcare facilities do not meet the full approval/licensure requirement._

There are four types of providers which meet the legal definition of an IV-E reimbursable facility:

* a licensed or approved foster family home

* a licensed or approved relative foster home

* a private, non-profit group home or childcare facility licensed by the state;
and

* a public (government) non-medical child group home or childcare facility licensed for no more than 25 children

=== Non-IV-E Reimbursable Placements

Non-IV-E reimbursable placements include the following:

* regional youth detention centers (RYDC)

* youth forestry camps (YFC) (secure and non-secure)

* youth development centers (YDC) and other public or private facilities (secure and non-secure) that are operated primarily for the detention of delinquent children, which must be (a) physically restricting and (b) likely to be non-operational without a population of children adjudicated delinquent (i.e., hardware secure, locked facilities)

* medical facilities.
If a child enters a reimbursable foster care placement for part of a month but is subsequently moved to a non-reimbursable facility for part of the same month, the child's cost of care is not reimbursable beginning on the date of placement in the non-reimbursable placement.
The child is not IV-E reimbursable until entering a reimbursable placement.

* relative homes.
Refer to Section xref:2848.adoc[2848], Relative Care Placement and Section xref:2850.adoc[2850] - Special Considerations, for more information.
