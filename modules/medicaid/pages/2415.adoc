= 2415 Self-Employment
:chapter-number: 2400
:effective-date: February 2020
:mt: MT-58
:policy-number: 2415
:policy-title: Self-Employment
:previous-policy-number: MT 49

include::ROOT:partial$policy-header.adoc[]

== Requirements

Earnings from an applicant/recipient's (A/R) own business or self-employment, as opposed to wages or a salary from an employer, are included as earned income when determining eligibility for any Medicaid Class of Assistance (COA) and when determining ABD patient liability/cost share budgets.

== Basic Considerations

*Net Earnings*

The net earnings from self-employment (NESE) is the gross income from any trade or +
business, plus capital gains, less allowable business expenses, including depreciation.

NESE also includes any distributive share (whether or not distributed) of income or loss +
from a trade or business operated as a partnership.

*Appreciation*

Appreciation, or capital gain, is an increase in the value of a business resource, and is a +
result of any of the following:

* improvement in the property
* normal market increases
* interest accrued

Determine appreciation by obtaining verification of the value of the resources from a reliable +
source

*Depreciation*

Depreciation occurs when a business resource loses value because of either of the +
following:

* destruction of property in a storm, fire or other disaster
* long term use of the resource reduces its value (e.g., vehicles, machinery)

Determine depreciation by obtaining verification of the value of the resource from a +
reliable source.

== Procedures

*ABD MEDICAID*

Develop NESE in the following situations:

* the A/R was self-employed in the prior tax year
* the A/R is currently self-employed
* the A/R has been self-employed during the current tax year

Calculate NESE on a tax year basis.

* Subtract all allowable IRS business deductions claimed on the self-employed individual's federal tax return from the gross self-employment earnings for the year to determine the NESE.

* Divide the NESE equally among the 12 months in the tax year to determine monthly earnings.

Divide net losses from self-employment over the tax year in the same way as net earnings. +
Deduct each month's net loss from other earned income for that month.

Divide the entire taxable year's NESE equally among 12 months in the taxable year, even +
if the business is seasonal, starts late in the year, ceases operation before the end of the +
tax year or ceases operation prior to the initial application for ABD Medicaid.

When a Medicaid individual, a member of a Medicaid couple, a Medicaid individual's +
ineligible spouse, or one of two ineligible parents incurs a verified net loss, deduct the loss +
from other earnings for the tax year in which the loss was incurred, regardless of which +
individual incurred the loss.

In the event cash or in-kind items are withdrawn from a business for personal use, +
determine whether the withdrawals were properly accounted for in determining NESE.

Accept the individual's allegation that withdrawals were deducted on his/her tax return in +
determining the cost of goods sold or that they were deducted on his business records.

If a withdrawal(s) was deducted, then it was properly accounted for.

*FAMILY MEDICAID*

*Capital Gains*

Consider the total proceeds from the sale of capital goods or equipment, less depreciation, +
as capital gains income.

Add capital gains income to the gross self-employment income.

*Business Expense*

Deduct from the gross self-employment wages all allowable IRS business deductions +
claimed on the self-employed individual's federal tax return. +
For information on business expenses, refer to +
https://www.irs.gov/businesses/small/article/0,,id=109807,00.html

NOTE: The following expenses are not allowable deductions for self-employed individuals:

* Payment on the principal of the purchase price of income-producing real estate, equipment, machinery, etc.
* Federal *income* taxes
+
NOTE: A corporation or partnership can deduct state and local income taxes imposed on +
the corporation or partnership as business expenses.
Various federal, state, local, and +
foreign taxes directly attributable to a trade or business can be deducted as business +
expenses.
For more information on this, refer to +
https://www.irs.gov/publications/p535/ch05.html#d0e3422

* Portions of self-employment taxes.
Refer to Line 27 on Form 1040 for portion that is allowable.
* Personal expenses (transportation to and from work, living expenses)

*Boarder Income*

Deduct actual cost of doing business from the boarder income.

*Rental Income*

Rental income is budgeted as earned or unearned, depending on the number of hours an +
individual is engaged in property management. +
Consider self-employment income from rental property as follows:

* If the individual is actively involved in property management at least 20 hours per week, count the gross income, less the cost of doing business, as *earned* income

* If the individual is not actively involved in property management at least 20 hours per week count the gross income, less the cost of doing business, as *unearned* income.

*Annualized Income*

Annualize self-employment income if the following occurs:

* the self-employment income represents a year's support, even if the income is received in a short time period
* the self-employment income accurately reflects the AU's current circumstances.
+
NOTE: Annualize the self-employment income, even if the AU receives additional income +
from other sources.

Do not annualize self-employment income if the following occurs:

* the self-employment income is not an accurate reflection of the AU's current circumstances because income has recently increased or decreased

* the self-employment income represents support for only part of the year

* the self-employment income is from a new business in operation for less than one year

To annualize self-employment income, total the gross annual receipts, subtract the cost of +
doing business and divide by 12.

*Non-Annualized Income*

Refer to Chart 2415.1, Calculation of Non-Annualized Income, in this section.

*Self-Employment Income Budgeting Procedures*

Follow the steps below to determine self-employment income for inclusion in the budget. +
Refer to Chapter 2650, Family Medicaid Budgeting.

*Step 1* Add all gross self-employment income.

*Step 2* Add any capital gains, less depreciation.

*Step 3* Subtract the cost of doing business.

*Step 4* Consider the result as the adjusted gross self-employment income.

*Step 5* Calculate other deductions.
Refer to Section 2655, Family Medicaid +
Deductions.

== Verification

*ABD and Family Medicaid*

Verify gross self-employment earnings and allowable IRS deductions through the use of any of the following:

* federal income tax return
* business records including receipts, bills and invoices
* the A/R's signed statement if neither of the above are available.
+
NOTE: Assume that any deductions taken on a tax return or business record is allowable +
by the Internal Revenue Service.

Document the case record as to why federal income tax returns or business records were +
not used if the A/R's statement was accepted as verification.

NOTE: The A/R's statement of self-employment earnings and allowable deductions is +
accepted as verification for Pregnant Woman and Newborn COAs unless questionable.

Use Chart 2415.1 to determine treatment of income that is not annualized:

.CHART 2415.1 CALCULATION OF NON-ANNUALIZED INCOME - FAMILY MEDICAID
|===
| IF THE INCOME | THEN

| does not reflect current circumstances (recent increase or decrease in income)
| determine the best estimate of current gross income less cost of doing business to be used as the monthly amount budgeted.

| is from a new business, i.e.,

in operation less than one

year
| average gross income less cost of

doing business over the period of

operation to determine projected

monthly income.

| represents support for only

part of the year
| average gross income less cost of

doing business over the number of

months the income is intended to

cover.

| is received monthly
| count total gross monthly income less

cost of doing business.
|===
