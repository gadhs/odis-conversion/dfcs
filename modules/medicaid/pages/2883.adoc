= 2883 Standard Filing Unit
:chapter-number: 2800
:effective-date: January 2021
:mt: MT-63
:policy-number: 2883
:policy-title: Standard Filing Unit
:previous-policy-number: MT 20

include::ROOT:partial$policy-header.adoc[]

== Requirements

The Assistance Unit (AU) in the removal home must be established using the 1996 AFDC Standard Filing Unit (SFU) policy.

== Basic Considerations

The SFU consists of the foster child for whom a IV-E determination is requested and certain individuals living in the removal home with the foster child who must be included in the AU.
The following individuals are required to be included in the SFU:

* Eligible minor siblings of the foster child (half, whole or adoptive siblings)

* Biological or adoptive parents.

*EXCEPTIONS:*

* SFU does not apply to a IV-E or CW-FC child who applies for TANF for his/her child.
The child of the IV-E or CWFC child can be an AU separate from the minor parent for TANF purposes.

* Any household member receiving SSI benefits is not counted as a member of the AFDC AU.
In addition, the SSI benefits and any other income or resources of the SSI recipient are not counted in determining financial need.
If the child in custody is a SSI recipient, the AFDC financial need criteria for both income and resources has been met.
See Section xref:2845.adoc[2845], SSI Eligible Child.

* An adoptive sibling to the child, who is receiving adoption assistance, may be excluded from the AFDC AU.
(The adoptive sibling's income and resources would be excluded.)

The foster child must have lived with a relative within the proper degree of relationship in the removal home.
Refer to Section xref:2825.adoc[2825], AFDC Relatedness, page 1, Living with a Specified Relative in the Removal Home.

Other individuals living in the home may be included because of their relationship to the specified relative from whom custody was removed.

Children in the AU do not have to be related to each other.

== Procedures

Follow the steps below to determine the composition of the AFDC AU.

*Step 1* Begin with the dependent child for whom the IV-E determination is requested and determine the following:

* The removal home
* Who lived in the home with the child at the time of removal

*Step 2* Include the eligible minor sibling(s) and half sibling(s) who lived in the home with the child identified in Step 1.

*Step 3* Include the biological or adoptive parent(s) of the children included in the AU by Steps 1 and 2. Include only the parents who actually live in the home with their children.

*Step 4* Include, if advantageous to the IV-E child, any children in the home who are within the proper degree of relationship to an eligible adult in the AU.
If a child is added for this process, repeat Steps 1 through 3 treating the child being added as the child in Step 1.

*Step 5* If there is no parent of the foster care child in the home, consider including an adult who is in the home and within the proper degree of relationship to the AU children.
If the foster child is eligible without the inclusion of the adult, do not include the caretaker in the AU.

*Step 6* Exclude the following individuals from the AU:

* A child who is not deprived

* A child who does not meet the relationship requirement to the grantee relative

* An individual who does not meet the citizenship/alienage requirement

* An individual ineligible because of the receipt of a lump sum

* A parent living in the home who is sentenced to perform unpaid public

work or community service in lieu of imprisonment

* An adult relative who fails to cooperate with the third party resource requirements, even if the information the A/R refuses to provide is regarding TPR coverage of a child in the AU.

* An SSI recipient.

Accept the SSCM's statement to determine AU composition.
If conflicting information is known, the discrepancy must be resolved.

Document the following for every individual in the removal home:

* The name of the individual and their relationship to the foster child

* The reason the individual is included or not included as an AU member

Use the following chart to determine the composition of the AFDC Unit (AU), starting with the foster child for whom a IV-E determination has been requested.

.Chart 2883.1 Determining the Composition of the AFDC AU
|===
| IF | THEN

| Adult is absent from the home because of treatment or training.
a|
Include the adult in the AU when all of the following conditions exist:

* The absence is temporary, with a plan for treatment or training to return the adult to the home

[.text-center]
AND

* The adult continues to exercise care and control of the AU child(ren)

[.text-center]
AND

* It is advantageous to the IV-E child to include in the AU during the absence

NOTE: Treatment or training may be received at locations such as schools, general hospitals, private psychiatric hospitals, nursing home, and Job Corps facilities.
This list is not all inclusive.

[.underline]#Exclude# the adult from the AU if any of the following conditions exists:

* The adult is incarcerated.
* The adult is in a public institution for the treatment of tuberculosis or a mental disease.

The adult is legally committed to an institution

>| Alien lives in the home.
| Include the alien in the AU if s/he meets citizenship requirements and is required to be in the AU.
Exclude if the citizenship requirements are not met.

>| AU resides in a shelter.
| Determine the AU composition of individuals residing in facilities such as homeless or battered women and children shelters as for any other group of related individuals who live together.

a|
Biological parent lives in the home with the child who has been adopted

[.text-center]
AND

The adoptive parent(s) is in the home

NOTE: Adoption terminates both legal responsibility and the SFU requirement to be included as a parent.
a|
Include the biological parent as a sibling only if both of the following conditions exists:

* The biological parent is also the child (step, adoptive or natural) of the adoptive parent(s)

[.text-center]
AND

* The biological parent meets the age and deprivation requirements

| Biological parent lives in the home with the child who has been adopted

AND

The adoptive parent(s) is not in the home.
| Include the biological parent as the grantee relative if s/he is eligible and it is advantageous.

Adoption terminates both the legal responsibility and the SFU requirement to

be included as a parent.

| Biological parent whose parental rights have been terminated lives in the home with the child and a specified relative.
a|
Include either the biological parent or the specified relative as the caretaker relative.

Termination of parental rights severs legal responsibility to be included as a parent;
therefore, the specified relative or the biological parent can include only if advantageous to the IV-E child.
The individual who is included in the AU must be in financial need.

NOTE: If the home is shared with a person who is not within the specified degree of relationship to the child, then only the biological parent may be included as the caretaker relative.

| Both parents live in the home with a mutual child.
a|
Include both parents in the AU if one is either disabled or meets AFDC-UP criteria.

NOTE: The parents do not have to be married to each other.

If the mother was legally married to another man at the time of the child's birth, exclude the biological father living in the home

|
| unless paternity is established through judicial proceedings or completing of the Form 185, Affidavit of Paternity.

| Child lives with a legal guardian who is not within the AFDC degree of relationship.
| Does not meet AFDC criteria.

| Child lives with an individual who has legal custody only (not a legal guardian and not within the degree of relationship).
| Does not meet AFDC criteria.

| Dependent child is in and out of the home.
| Include the dependent child in the AU when the absence is to be temporary and care

and control of the dependent child remain with the parent(s)

| Couple lives together

*AND*

They are not married

*AND*

There is no eligible mutual child(ren) but each has a child.
| Determine eligibility based on an AU of the parent of the foster child and the foster child.

| Minor parent (no spouse) lives in the home with her parents.
a|
Include the minor parent as a dependent child if one of the following conditions exists:

* The minor has sibling(s).
* The minor's parent is included as a caretaker.

DO NOT include the minor's spouse in the AU unless his/her child is included in the AU.
Consider his/her income in determining eligibility.

NOTE: Include or exclude the minor's child;
whichever is more advantageous.

If ineligible, count the minor parent as caretaker if the minor lives with her parent(s).

| Minor parent and her spouse live in the home with her parents.
a|
Include the minor parent as a dependent child if one of the following conditions exists:

* The minor has sibling(s).
* The minor's parent chooses to be included as a caretaker.

DO NOT include the minor's spouse in the AU unless his/her child is included in the AU.
Consider his/her income in determining eligibility (refer to AFDC Policy, Section 1457, “Spouse of a Married Minor +
Budgeting.”)

NOTE: The AU may choose to include or exclude the minor's child.

If ineligible, count the minor parent as caretaker if the minor lives with her +
parent(s).
Include the minor's spouse in the AU if he is the parent of the minor's child.
If he is included, consider the AU's eligibility for AFDC-UP or disability.

| Parent is in and out of the home.
a|
*Include* a parent who appears to reside in the home based on any of the following indicators:

* The parent has no other residence.
* The parent lists the home as his/her address.

*Exclude* a parent who appears to visit the home based on any of the following:

* The parent maintains another residence.
* The parent does not share in household expenses
* The parent has a specific time frame for his/her visits.

NOTE: Thoroughly document the record to substantiate the inclusion/exclusion of a parent.

| Parent lives in the home with the child(ren) and a specified relative who has legal custody/legal guardianship of the child(ren) (parental rights have not been terminated).
| Include the parent in the AU as the caretaker relative.

| Roomer or boarder lives with the AU.
| Determine the AU composition without regard to others living with the AU and paying a fee for food and/or shelter unless they meet relationship or other SFU criteria.

| Stepparent lives in the home

AND

The stepparent has no children of his own living in the home.
a|
Include stepparent in the AU, (if advantageous) if one of the following situation exists:

* There is no parent living in the home

[.text-center]
OR

* The parent living in the home receives SSI.

| Married couple has no eligible mutual child(ren)
| Determine the option that is most advantageous to the family.
Combine in one AU or make separate AUs.

| Child is placed in a residential childcare institution, such as GA.
Baptist Children's Home, United Methodist Children's Home or GA.
Sheriffs Boys' Ranch.
a|
Determine the child's eligibility if the following conditions are met:

* The center is privately owned and operated

[.text-center]
*OR*

* The center is a public facility and the placement is temporary pending other arrangements appropriate to

the child's needs.

NOTE: If a IV-E foster care child is placed in a public childcare facility, continue IV-E eligibility only if the facility accommodates no more than 25 children.
|===
