= 400 Community Action Plan
:chapter-number: 400
:effective-date: October 2020
:policy-number: 400
:policy-title: Community Action Plan
:previous-policy-number:

include::ROOT:partial$policy-header.adoc[]

All eligible entities are required to submit a Community Action Plan (CAP) that links all services to the National Goals.
The CAP is the framework in which eligible entities address the problem of poverty situations in their community as identified in the current needs assessment.
All CAPs are to be completed utilizing EasyTrak.

== Requirements

Programs offered should be directly related to the mission statement of the agency.
Eligible entities must develop a CAP that outlines each of the services provided by the CSBG program.
The CAP includes but is not limited to:

*Title of Program/Service* – The title name given to the program is chosen by the agency.
Generally, the title name of the program reflects some aspect of the program category.
*Contract Period* – The contract period lists the beginning and end dates of the period covered by the plan.
Most often the contract covers the 12 months of the federal fiscal year;
however certain programs within the agency may not, i.e. youth summer camp programs.
Additionally, the contract period for plans supported by discretionary funding may have different start and end dates.

*Program Category* – The program category best reflects the primary tenets of the program and is selected from the thirteen (13) CSBG statute service categories or “Other Negotiated Services”.
Although “Other Negotiated Services” is rarely used, it was designed by the State Department to capture any program that does not fall into one of the thirteen CSBG service categories.
Programs that fall into Other Negotiated Services must seek prior approval from the State Department prior to the submission of the CAP.
*Determination of Need* – This shows the degree of need for the program as identified in

the current needs assessment.
Multiple resources should be used to make the determination of need.

*Individuals to be Served Directly* – The number of units expected to be served is determined by the type of services provided, target population, contract period, program cost per unit, budget amount, etc.
A unit may be an individual, household or community as appropriate.

*Objectives/Outcomes* – The outcomes are the objectives or expected results that the agency hopes to achieve by performing the services and activities of the program.
Individual activities can have objectives and outcomes.
Each service/activity should lead to an outcome and national performance indicator;
however, a service/activity may be linked to multiple outcomes and national performance indicators.

*Program Description/Activities* – The program description is a comprehensive detailed narrative that clearly presents the services and activities offered.
It includes specific terminology to describe processes used to deliver the service and activities.
Each of the programs selected must address at least one (1) of the six (6) national ROMA goals.

*National CSBG Goals* – The three (3) national ROMA goals were designed to measure effectiveness and accountability for the expenditure of CSBG funds.
ROMA goals measure results of programs and services at the local level.
Each outcome is linked to a national ROMA goal.

*Outcome Measures* – Outcome measures are the national performance indicators and are linked to each major service/activity of the program as appropriate.
National performance indicators may be called direct measure and/or outcome indicators.

*Measurement Tools* – Measurement tools are all methods of measurement used in collecting data for the program by service/activity.

*Funding Categories* – The funding categories to cover program costs are captured in the budget information section.
The categories are:

* CSBG Allocation
* Non CSBG Funds Mobilized (Other Federal, State, Local, Private, In-kind)

*Non-CSBG Funds Leveraged* – CSBG funds are traditionally used to leverage other fund sources in order to eliminate or improve the conditions of poverty.
