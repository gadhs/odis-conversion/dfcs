= 1302 Domestic Violence
:chapter-number: 1300
:effective-date: December 2022
:mt: MT-70
:policy-number: 1302
:policy-title: Domestic Violence
:previous-policy-number: MT 68

include::ROOT:partial$policy-header.adoc[]

== Requirements

The PRWORA requires states to establish standards and procedures to ensure that domestic violence is identified.

The Consolidated Appropriations Act, 2022 amended section 402(a) of the Social Security Act (42 U.S.C.602(a)) and added new certification requirements.
The new certification requirements include several elements:

* Each state must certify that it has established and is enforcing standards and procedures to ensure that applicants and potential applicants for TANF are notified of assistance made available by the state to victims of sexual harassment and survivors of domestic violence, sexual assault, or stalking.

* It must also ensure that case workers and other agency personnel responsible for administering the TANF program are trained in:
 ** the nature and dynamics of sexual harassment and domestic violence, sexual assault, and stalking
 ** state standards and procedures relating to the prevention of, and assistance for, individuals who are victims of sexual harassment or survivors of domestic violence, sexual assault, or stalking
 ** methods of ascertaining and ensuring the confidentiality of personal information and documentation related to applicants for assistance and their children who have provided notice about their experiences of sexual harassment, domestic violence, sexual assault, or stalking

* If the state has adopted the Family Violence Option, it must provide information about the options available to current and potential beneficiaries and ensure that case workers and other agency personnel are provided with training regarding relevant state standards and procedures.

== Basic Considerations

=== Types of Domestic Violence and Definitions

The term “domestic violence” has the same meaning as the term “battered or subjected to extreme cruelty” as defined in section 408(1)(7)(C)(iii).

Sec.
402(a)(7)(B) defines domestic violence as:

* physical acts that resulted in, or threatened to result in, physical injury to the individual:
* sexual abuse:
* sexual activity involving a dependent child
* being forced as the caretaker relative of a dependent child to engage in nonconsensual sexual acts or activities:
* threats of, or attempts at, physical or sexual abuse;
* mental abuse;
* neglect or deprivation of medical care.

The term “sexual harassment” means hostile, intimidating, or oppressive behavior based on sex that creates an offensive work environment.

Sexual assault means any nonconsensual sexual act proscribed by Federal, tribal, or State law, including when the victim lacks capacity to consent.

Stalking is defined as is the act or crime of willfully and repeatedly following or harassing another person in circumstances that would cause a reasonable person to fear injury or death especially because of express or implied threats

=== Family Violence Option

Georgia has adopted the Family Violence Option (FVO), which is intended to provide for the identification and screening of domestic violence victims, their referral for services and waiver of program requirements for good cause.

In accordance with the FVO, Georgia has established and is enforcing the following standards and procedures:

. to screen and identify individuals who receive cash assistance and who have a history of domestic violence, sexual assault, sexual harassment, or stalking while maintaining the confidentiality of these individuals,
. to refer an individual so identified to counseling and supportive services, and
. to provide waivers, in accordance with a determination of good cause, certain

program requirements where compliance with such requirements would make it more difficult for an individual receiving cash assistance to escape domestic violence or unfairly penalize those who are or have been victimized by such violence or who are at risk of further domestic violence.

=== DFCS Responsibilities

The DFCS case manager performs the following functions:

* provides information about what constitutes domestic violence, sexual assault, sexual harassment, or stalking
* obtains information about the family's circumstances and the potential existence of domestic violence, sexual assault, sexual harassment, or stalking
* obtains information to determine the effect of domestic violence, sexual assault, sexual harassment, or stalking on the AU's ability to meet certain requirements,
* provides the telephone number for the 24-hour domestic violence hotline,
* explains that certain requirements can be waived,
* refers the AU to the Domestic Violence Assessor (DVA) when domestic violence, sexual assault, sexual harassment, or stalking is reported,
* refers the AU to domestic violence resources available in the community when the AU does not wish to speak with the DVA or a DVA is not assigned to a county,
* determine whether any requirements can be waived, taking into consideration recommendations made by the DVA.

=== Domestic Violence Assessor Responsibilities

The DVA performs the following functions:

* meets with the family to assess their circumstances and to determine if domestic violence, sexual assault, sexual harassment, or stalking or the potential for domestic violence, sexual assault, sexual harassment, or stalking exists,
* determines the effects of domestic violence, sexual assault, sexual harassment, or stalking on the family,
* makes recommendations to DFCS on Form 523, Family Violence Option Assessment Report regarding potential waivers,
* develops and coordinates a service and safety plan,
* refers the AU to supportive services available in the community,
* completes an assessment report for the TANF worker,
* provides case consultation to DFCS staff, including crisis intervention services when requested.

=== Providing Information to the AU

All AUs must be provided with Form 522, the brochure “What Every Person Needs to Know”, at the following times:

* application
* standard review
* 44-month review
* any other time the existence of domestic violence is reported or suspected.

The information in the brochure is discussed with every AU at the above times.
The AU is informed that certain program requirements may be waived and the circumstances under which a waiver may be granted.
Refer to Waivers in this section.

When it is determined or suspected that an AU member has been or may become a victim of domestic violence, sexual assault, sexual harassment, or stalking the AU is given the 24-hour hotline number.
The AU must be informed that the hotline is available whenever services are needed, regardless of whether the AU continues to receive assistance through DFCS.

Form 194, TANF Assessment for Domestic Violence, Sexual Assault, Sexual Harassment, or Stalking must be discussed with the family at applications and at each standard review.

Form 194 must be completed and signed by the client at application if the client requests a waiver.

=== Identifying Domestic Violence, Sexual Assault, Sexual Harassment, or Stalking

The existence of domestic violence, sexual assault, sexual harassment, or stalking must be identified so that appropriate referrals can be made, and program requirements can be waived if barriers to fulfilling these requirements exist.

The need to identify the existence of domestic violence, sexual assault, sexual harassment, or stalking may occur at any time.

The responses to the questions included in the brochure, other information obtained from the AU or other sources and the worker's own observations will be used to determine if domestic violence, sexual assault, sexual harassment, or stalking exists or if there is reason to suspect that it exists.

The AU member's statement that s/he is a victim of domestic violence, sexual assault, sexual harassment, or stalking may be accepted, unless questionable.

If questionable, the following sources may be used to support the AU member's claim:

* proof of services from or referral by a domestic violence agency, including the assessment and services through the DVA,
* a temporary protective order,
* a law enforcement report of domestic violence, sexual assault, sexual harassment, or stalking
* a medical report substantiating domestic violence, sexual assault, sexual harassment, or stalking
* documentation of domestic violence, sexual assault, sexual harassment, or stalking from an agency, professional, friend, or relative from whom the victim has sought assistance in dealing with domestic violence, sexual assault, sexual harassment, or stalking.

=== Referrals

Victims of domestic violence, sexual assault, sexual harassment, or stalking are referred to the DVA.

If there is not a DVA assigned to a county, the victim may be referred to the domestic violence agency that serves that county.

If an individual does not want to be referred to the DVA, s/he will be referred to or provided with the name, address and telephone number of other community resources.

When there is an indication that an individual is a victim of domestic violence, sexual assault, sexual harassment, and/or stalking but s/he does not acknowledge it, a referral to the DVA is not made.
However, s/he will be provided with the information about other community resources.

Victims of domestic violence, sexual assault, sexual harassment, or stalking may be referred to community resources that provide the following services:

* safety planning
* legal advocacy
* shelter
* hotlines
* medical services
* mental health care
* counseling, including educational and family planning
* substance abuse treatment
* financial assistance.

=== Waiver of TANF Requirements

A waiver of certain program requirements may be granted if any of the following circumstances exist:

* The requirement makes it more difficult for the individual to escape domestic violence, sexual assault, sexual harassment, and/or stalking.
* The requirement may result in physical and/or emotional harm to the family.
* The requirement unfairly penalizes an individual who is, has been, or may be in danger of becoming a victim of domestic violence, sexual assault, sexual harassment, and/or stalking.

The DVA determines the effects of the domestic violence, sexual assault, sexual harassment, and/or stalking on the AU and makes recommendations regarding which requirements may need to be waived using the Family Violence Assessment Form.

The worker confers with the DVA to discuss the DVA's recommendations regarding waivers.

The worker determines which requirements to waive based on information obtained through discussions with the AU and recommendations from the DVA.

If there is no DVA available to a county, the worker must determine which requirements to waive based on information obtained from the AU and other sources, and the worker's observations.
The worker may contact the domestic violence agency for consultation.

The worker must reassess waivers at no less than 30-day intervals or more often if circumstances are subject to change in less than 30 days.

Requirements which may be waived include the following:

* personal responsibilities
* minor parent living arrangement
* work requirements
* cooperation with Child Support Enforcement
* lifetime limit

Requirements that cannot be waived include the following:

* deprivation
* age
* living with a specified relative
* lawbreaker penalty
* citizenship/alienage
* Financial eligibility.
+
NOTE: Refer to Section 1310 for policy pertaining to aliens certified by the INS to be battered spouses or children.

Waiving the work requirement does not restrict otherwise qualified individuals from participation in work, job training, education, job readiness, work placement assistance or a community service program.
However, failure to continue participation in the activity does not result in a sanction.

An AU cannot be required to take actions such as seeking orders of protection, attending counseling or other actions in order to be granted a waiver of any requirement.

Waivers are granted on a temporary basis.
When meeting a program requirement no longer puts an AU at risk of domestic violence, sexual assault, sexual harassment, and/or stalking the waiver of the program requirement is lifted.
The AU must meet the requirement.

NOTE: An AU may have several program requirements waived because they put the AU at risk of domestic violence, sexual assault, sexual harassment, and/or stalking.
As an AU's situation changes, these waivers may be lifted one or two at a time or all at once, depending on the situation and the requirement.

=== Confidentiality

Any information related to individuals known to DFCS is confidential.
Confidentiality guidelines must be closely followed.
Confidential information includes but is not limited to the following:

* the AU's current address, workplace or work placement
* other personal information
* identification of the individual as a victim of domestic violence
* any other detail concerning the domestic violence.

Refer to Section 1002, Confidentiality.

== Procedures

Follow the steps below to screen for and identify the existence of domestic violence, and to waive program requirements.

*Step 1:* Provide every AU with Form 522 at the appropriate times.
Review and discuss the brochure with the AU. +
*Step 2:* Interview the AU using the nine questions on the Form 522. +
*Step 3:* Obtain other information about the family's circumstances. +
*Step 4:* Discuss the existence of domestic violence, sexual assault, sexual harassment, and/or stalking in the family.
Accept the AU's statement unless questionable. +
*Step 5:* Discuss the potential for waivers of program requirements because of domestic violence, sexual assault, sexual harassment, and/or stalking. +
*Step 6:* Complete Form 194, TANF Assessment for Domestic Violence, Sexual Assault, Sexual Harassment, or Stalking as applicable. +
*Step 7:* Refer the AU to the Domestic Violence Assessor and/or other resources available in the community, if appropriate.
Provide the AU with the 24-hour hotline number. +
*Step 8:* Determine which program requirements, if any, will be waived. +
*Step 9:* If the AU includes a work eligible individual, advise the AU to contact their case manager for a review of barriers no less than every 30 days.
Longer interval puts the AU at risk of domestic violence, sexual assault, sexual harassment, and/or stalking. +
*Step 10:* If the AU contains no work eligible individuals, enter a manual task to contact the AU at a minimum of 90-day intervals to determine current risk and continuation of waivers.
Document the contact. +
*Step 11:* Take action to lift the waiver and ensure that the AU meets a program requirement when the requirement no longer puts the AU at risk of domestic violence, sexual assault, sexual harassment, and/or stalking.
