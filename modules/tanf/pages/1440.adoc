= 1440 Prisoner Verification Inquiry System
:chapter-number: 1400
:effective-date: January 2020
:mt: MT-52
:policy-number: 1440
:policy-title: Prisoner Verification Inquiry System
:previous-policy-number: MT 1

include::ROOT:partial$policy-header.adoc[]

== Requirements

The Prisoner Verification Inquiry is a computer interface between the system and the Social Security Administration to determine if a member of an assistance unit (AU) is incarcerated.

== Basic Considerations

The Social Security Number (SSN) of individuals in an active or pending Food Stamp case and who are age 14 or older are matched with the SSA Prisoner Verification Inquiry system.

An alert is generated when the SSN is matched with the SSN of an individual who is in a federal, state or local penal, correctional or other detention facility for more than 30 days.

The SSN of a member of a TANF AU who is not also a member of a Food Stamp AU will not be matched with the Prisoner Verification Inquiry system.

The interface occurs at the following times:

* application,
* finalization of a new or reopened case,
* when a primary SSN is changed or added, and
* in the month prior to the review month.

The alert includes one of four codes from the SSA.
The codes are numbered 1 through 4.

Refer to Volume III, Food Stamps, Section 3520, for additional information and procedures concerning the Prisoner Verification Inquiry system.
