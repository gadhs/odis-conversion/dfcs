= 1826 Subsidized Employment Grant Diversion
:chapter-number: 1800
:effective-date: January 2020
:mt: MT-53
:policy-number: 1826
:policy-title: Subsidized Employment Grant Diversion
:previous-policy-number: MT 30

include::ROOT:partial$policy-header.adoc[]

== Requirements

The Georgia Division of Family and Children Services (DFCS) will divert the TANF assistance unit's cash benefits to a public or private sector employer for employing a work eligible TANF recipient.

== Basic Considerations

Subsidized employment (SE) is a countable (core) work activity in which the employer receives a subsidy from TANF or other public funds as an incentive to hire and train TANF recipients or TANF-eligible individuals.
SE participants will receive wages from employers for all hours of participation.

=== SE-Grant Diversion

Subsidized employment-grant diversion means that the AU 's cash assistance benefit is diverted (paid) to the employer to offset some or all of the wages and costs of employing a TANF individual.

A third-party vendor may be used to place participants and to reimburse the employers.

Participants in the SE grant diversion program are regular employees and are subject to minimum wage and other Fair Labor Standards Act (FLSA) protections.

An SE grant diversion placement may begin at any time during the month.
If a client begins participation before the 12th calendar day of a month, the cash assistance of the participating client's AU is to be diverted to the employer in the following calendar month.

If a client begins participation on or after the 12th calendar day of a month, the cash assistance must be diverted to the employer no later than the second month after employment begins.

Diversion of the cash assistance grant is possible only when the earnings from a job are enough to reduce the TANF grant to zero.
If the wages received for a job will not reduce the grant to zero, no subsidy can be paid.

Prior to the client beginning employment, a trial budget must be computed that includes only the subsidized earnings to determine the AU's eligibility for cash assistance.

If the earnings from a job are enough to reduce the grant to zero, the entire amount of the grant will be paid to the employer.

=== Premature Termination of Subsidized Employment

Once the initial determination of eligibility to participate in subsidized employment is made, the amount of the grant that is diverted to the employer remains the same throughout the duration of the client's participation period.

If the subsidized employment placement ends prior to the date agreed upon in the Form 200, Subsidized Employment Agreement (SEA), the employer will be paid for the days worked by the client during the last calendar month of employment.

The amount of reimbursement due to the employer is determined by dividing the grant amount by thirty days, arriving at a daily rate, and multiplying the daily rate by the number of days worked by the client in the final month of employment.

An AU is subject to sanction if the AU member participating in subsidized employment fails to participate after having been referred to an employer.

NOTE: The voluntary quit policy and ineligibility period must be reviewed before the AU is approved for TANF.

=== Fixed TANF Benefit Amount

The amount of the TANF benefit diverted to the employer is fixed beginning with the month of placement in subsidized employment.
The amount remains unchanged for the duration of the client's participation in the activity.

The TANF benefit amount diverted to the employer is the TANF benefit amount the AU receives in the month of placement.

[caption=Exception]
NOTE: If the ongoing benefit amount changes because of a change in the AU's size and the change is known prior to the subsidized employment contract being finalized, then the new cash assistance amount is the amount to be diverted to the employer.

Participants in the grant diversion program are regular employees and are subject to minimum wage and other Fair Labor Standards Act (FLSA) protections.

=== Program Requirements

The subsidized job must be full-time (minimum 30 hours weekly) and pay at least minimum wage.
The maximum length of the grant diversion program is six months.

A Form 200, Subsidized Employment Agreement (SEA) must be completed for each participant.

At the end of the subsidy period, the employer is expected to retain the participant as a regular employee without receiving a subsidy.

Participants eligible for this program are considered "Near Job-Ready".
That is, the participants who have:

* no high school diploma/GED and limited work experience
* a high school diploma/GED and limited work experience
* received TANF for more than 12 months
* low-functioning tendencies.

_To learn more about general requirements of subsidized employment, please refer to section 1825, Subsidized Employment General Requirements of the policy manual._

=== Determination of Eligibility for Participation

Follow the steps below to determine the client's eligibility for participation in subsidized employment:

*Step 1:* Obtain a copy of the Form 200, SEA.
The agreement includes the beginning date of employment, the salary the employee will receive, and the duration of the training period.

*Step 2:* Determine the gross earned income the client will receive from subsidized employment earnings.

*Step 3:* Subtract $90.00 from the gross earned income in Step 2 to determine the net earned income.
Compare the net earned income to the standard of need (SON) for the appropriate AU size.

Proceed to Step 4 if the income equals or exceeds the SON.
If it does, the client is eligible for participation and the employer is eligible to receive the grant amount the AU was receiving prior to subsidized employment participation.

*Step 4:* Complete the Form 200, SEA indicating the grant diversion amount and the reimbursement dates based on the training period.

*Step 5:* Scan a copy of the completed Form 200, SEA in the case record to serve as verification of wages and employment.

*Step 6:* Make the necessary changes on the system to begin the grant diversion.

== Procedures

=== Other Required Actions for SE-Grant Diversion

Complete the following after eligibility for participation in subsidized employment is established:

Do not add the subsidized employment wages to the TANF case budget.
Create a task as a reminder to review the case in the client's last month of participation in the subsidized employment.

Send a notice informing the AU of the termination of cash assistance and the months of participation in the program.

NOTE: In order to allow only adequate notice prior to terminating cash assistance to the AU, the client must complete Form 102, Waiver of Timely Notice Period.

Suspend any scheduled and/or existing claims for the subsidized employment period.

Follow the procedures below for clients participating in subsidized employment grant diversion program:

.CHART 1826.1 - SUBSIDIZED EMPLOYMENT PROCESSING PROCEDURES
|===
| IF | THEN

| the client begins participation on or after the 12th of the month
| the first reimbursement payment must be made to the employer in the second month following the month in which subsidized employment began.

To determine the actual months of participation in the activity, consider the first month of participation as the month after the individual begins work.

Make the necessary changes on the system.
Mail a copy of Form 200 to the AU and employer.

| participation terminates prior to the scheduled ending date
| remove the subsidized employment grant diversion coding information case for the ongoing month to ensure that the cash assistance is not diverted to the employer.

Determine the reimbursement amount, if any, and issue the payment from employment services funds.

Reimburse the employer on a prorated basis for the month of change.
Reimburse the employer based on the number of days the client worked in the final month.

Complete a review as soon as possible, but no later than the month following the final month of participation, to determine ongoing eligibility.

| participation terminates prior to the scheduled ending date (cont'd)
a|
Determine the cash assistance amount to which the AU is entitled to receive for the month of change by completing the following:

* recalculate the budget for the month based on the AU's current situation, i.e., subsidized employment wages, other income, AU size, etc.,
* allow all appropriate earned income deductions

Determine if good cause exists and, if applicable, review continued eligibility. +
If eligible, issue a manual corrective for the new cash assistance amount calculated for the month of change.

| notice of the termination of subsidized employment grant diversion is received too late to make the necessary system changes to avoid the payment to the employer
| contact and notify the employer that the cash assistance issued for that month must not be accessed.

Notify the employer that a prorated share, based on the number of days worked by the client, will be paid to the employer.

| participation in the subsidized employment grant diversion activity ends as scheduled
| complete a review in the final month of participation to determine ongoing eligibility for cash assistance.

Remove the subsidized employment coding information from the TANF case.
|===
