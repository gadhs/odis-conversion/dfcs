= 1805 Applicant Services
:chapter-number: 1800
:effective-date: September 2023
:mt: MT-74
:policy-number: 1805
:policy-title: Applicant Services
:previous-policy-number: MT 68

include::ROOT:partial$policy-header.adoc[]

== Requirements

Applicants for cash assistance are required to participate in job search when they are determined to be job-ready and if they are not exempt from work requirements.
Applicants who meet the exemption criterion may choose to waive their exemption and participate in job search.

== Basic Considerations

All applicants should be assessed for their job readiness level before an activity is assigned.

An applicant's strengths, barriers, resources, limitations, and disabilities should be taken into consideration when determining job readiness level.
Reasonable accommodations must be provided before engaging the applicant in an activity.

An applicant for cash assistance may participate in any activity, including job search, prior to the disposition of the application.
However, only non-compliance with job search can be a reason to deny the application.

DFCS can provide Support Services to support applicant job search only.
However, DFCS collaborates with employers, educators, trainers, and support service providers to ensure that each participant has access to the resources that are available in the community, to achieve the goal of self-sufficiency.
Each county is responsible for developing procedures for how applicant services will be managed in its local office.
Services may be provided individually or as a group.

An applicant whose disability(ies)/limitation(s) prevent him/her from doing job search is not considered job ready.
If otherwise eligible for cash assistance, the application should be approved prior to the completion of an in-depth assessment.

=== Exemption

A single custodial parent, with a child less than twelve months of age, may choose to receive an exemption from work activities.
However, the cost and consequences of the choice must be explored with the client.
The reason (s) why the client chose exemption should be clear to both parties, i.e., the client and the case manager.

The maximum allowable exemption is up to three months per child.
However, the exemption period for the single custodial parent cannot exceed a total of 12 months.

The case manager will explain to A/R that personal responsibilities are applicable during exemption period.

The case manager will schedule an appointment with the exempt single parent during the last month of exemption to develop the TANF Family Service Plan, which will be effective the following month.
An applicant who qualifies for the exemption from work activities may choose to participate in job search while retaining the option to exercise the exemption from work activities if the application is approved.

=== Orientation

Orientation to employment services can be provided for clients on an individual basis or in a group setting.
Orientation includes an emphasized explanation of 48-month TANF lifetime limit, provision of information about seeking the employment, attending school or other training, the availability of childcare, and other resources that would assist the client in becoming economically self-sufficient.
It also provides the opportunity to inform the participants of our beliefs:

* Government cannot and should not take the place of family.
* The benefit of work will always be better for families than the limited benefits of TANF.
* There is dignity in work, whether with the hand or the head.
* TANF is not good enough for any family.

The orientation can give local employers and representatives of partner agencies and service providers a chance to present information about employment opportunities and their services.

During orientation, information on the following topics must be provided:

* 48-month lifetime limit of TANF benefits
* the purpose of federal work requirements
* the role/responsibilities of the case manager
* the purpose of applicant job search
* the rights and responsibilities of the client
* the availability of support services (Incidentals, Earned Income Tax credit (EITC), Child Tax credit (CTC)
* criteria for exemption from work requirements
* distinctions between work eligible and non-work eligible status
* Americans with Disabilities Act (ADA) requirements
* consequences of a failure to participate
* the fair hearing process
* role of partner agencies that may become involved in the process of assisting the participant to prepare for and find employment.
* the consequences of a failure to meet work requirements.

Applicants must be assessed so that a determination can be made concerning the client's job readiness level and the appropriateness of participation in applicant job search.
Form 490, Applicant Services Assessment Form, is used to guide the assessment and to record this information.

Refer to Section 1815, TANF Family Service Plan Development, for additional guidance on conducting an assessment.

Based on the case manager's assessment the job readiness level must be determined for the applicant.

=== Assessment

The initial assessment determines that the applicant is:

* job ready and may be scheduled for job search
* near job ready and needs some more skills, training and /or extensive job search
* not job ready and job search is not appropriate at this time.

The applicant may be given a waiver or delay of participation to job search requirements due to good cause.
However, the cost and consequences of the choice must be explored with the client.
The reason(s) why the client chose exemption should be clear to both parties, i.e., the client and the case manager.

Refer to Section 1835, Failure to Participate, for examples of good cause.

When determined to be job ready, applicants are required to participate in job search.
Counties may designate a period of required job search, not to exceed six weeks from the date of application.

A minimum of twenty-four employer contacts must be completed within the designated job search period.
A county may, based on local conditions, require more than twenty-four contacts.

The job ready applicant assigned to job search must keep any appointments with the Department of Labor (DOL) and comply with job referrals.
The applicant must submit a record of employment contacts on Form 495, Job Search Record.

Childcare must be provided when necessary to enable an applicant to complete job search requirements.
The case manager can authorize the reimbursement of or payment for transportation and incidental expenses incurred as a direct result of completing the job search requirements.
Refer to Section 1830, Employment Support Services.

At the conclusion of the designated job search period, the case manager must meet with the applicant to verify compliance with applicant job search.
This meeting is known as the progress review and may be documented on Form 196A of the TFSP and relevant screens in Gateway.

=== Guidelines for Applicant Job Search

Counties should regularly monitor participation in applicant services on a weekly or other periodic basis during the job search period.
The purpose is to monitor participant's progress, to provide encouragement, problem resolution, and additional job leads and determine job readiness levels.

The case managers need to assist the applicants in beginning the job search, provide information on job leads, job search resources and determine which support services are needed.
Consideration should be given to the following:

* TANF clock- How many months of TANF this AU has already received,
* job readiness level,
* the types of jobs in which the client has previously worked,
* previously held jobs preferred by the client, and the reasons why some were preferred over others,
* the availability in the community of jobs like those preferred by the client,
* types of work suggested by family members or friends as being particularly suitable for the client,
* whether or not the client has an employed friend who might assist the client in being hired by the friend's employer.

In order to determine needs and potential solutions, consider all available resources, for example:

* the means by which the client would get to work if a job offer was received the next day;
* what arrangements the client currently makes when transportation is needed;
* whether the client knows how to use public transportation, if available;
* whether the client has a valid driver's license and insurance and, if not, what is needed in order to obtain a license or insurance;
* Who provides care for the client's children when the client is absent from the home, e.g., to go to the store, to DFCS, to the doctor, etc.
What kind of childcare provider does the client want for the children?
If the client was offered a job the next day, who would care for his/her children?

During the job search monitoring process, the following questions should be considered:

NOTE: See monitoring guide to TFSP (Form 196A)

* What positive things have happened during the client's job search?
What resources did the client utilize to find job leads?
* What difficult situations must the family now manage?
* What can be done to eliminate the problem(s)?
* Is Child Welfare involved with this AU?
* Is there any change in job readiness level?
* Does the client expect to hear from any prospective employers?
How soon might the client know the results from any interviews?
* If the client was to be offered one of the jobs for which s/he applied, is there anything the client will need before being able to accept the job offer?
* Where else is the client planning to look for work?

The applicant must be provided with Form 495, Job Search Record.
Items 1-4 must be completed by the case manager, indicating the number of job contacts required and the date the client must return for a review.

The case manager must schedule a final progress review appointment with the applicant to assess the success of job search at the end of the designated job search period.
The purpose of the progress review is to verify compliance with the job search requirements and determine if the applicant has been successful in finding employment.
This review is also used to re-assess the job readiness level and determine if the applicant needs any additional assistance to move towards stable employment.

If the applicant fails to keep the appointment without good cause, the application must be denied.

If the applicant keeps the appointment, complies with the applicant job search requirements and is otherwise eligible for cash assistance, then the TANF application must be approved.
However, any barriers that might affect participation in the work activity should be included in the TANF Family Service Plan (Form 196A) upon approval.

If the applicant keeps the appointment, fails to meet the applicant job search requirement, but has good cause and is otherwise eligible for cash assistance, then the TANF application must be approved.
Any barriers that might affect participation in the work activity should be included in the TANF Family Service Plan (Form 196A) upon approval.

If the applicant keeps the appointment, fails to meet applicant job search requirements without good cause but conciliates and is otherwise eligible for cash assistance, then the TANF application must be approved.

If the applicant keeps the appointment, fails to meet the applicant job search requirements without good cause, and refuses to conciliate, then the TANF application must be denied.

The applicant must be assisted with transportation and childcare, as needed, so that job search requirements can be met.

Refer to Section 1830, Employment Support Services, for a detailed description of available support services and eligibility requirements to receive these services.

=== Child Care

The case manager must assist the applicant in obtaining childcare services through the Childcare and Parent Services (CAPS) Section.
Complete policies and procedures for providing subsidized childcare are located in the CAPS policy manual.

=== Transportation

Applicants can be provided $7.00 per day to assist with transportation costs for job search.
These costs may be provided up-front or as a reimbursement.
If a county is able to purchase transportation for the applicant through bus tokens or other means, it may do so in lieu of providing cash to the applicant.
The cost must not exceed a daily rate of $7.00.

An applicant may receive assistance with auto repair and maintenance expenses if these services are deemed necessary for the applicant to fulfill job search requirements or to accept employment while the application is pending.

=== Incidental Expenses

Incidental expenses include other services needed by the applicant to find a job and keep it.
These services can be purchased only when they are not available from other sources.
The need to purchase these services must be approved in advance by the case manager and documented on The TANF Family Service Plan.
(Form 196A)

=== Post-Applicant Support Services

Refer to Section 1830, Employment Support Services, for a complete listing of reimbursable expenses and the amounts that can be paid for applicants.
