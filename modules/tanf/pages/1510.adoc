= 1510 Treatment of Resources by Resource Type
:chapter-number: 1500
:effective-date: March 2023
:mt: MT-71
:policy-number: 1510
:policy-title: Treatment of Resources by Resource Type
:previous-policy-number: MT 52

include::ROOT:partial$policy-header.adoc[]

The chart below lists types of resources alphabetically and provides the following information:

* the source/type of resource,
* a description of the resource,
* the value to consider cash value (CV), equity value (EV) or fair market value (FMV),
* Whether the resource is included (I) or excluded (E) in the eligibility determination.

Refer to Chart 1530.1 Treatment of Income by Income Type, for information on how to treat a source of income prior to considering its treatment as a resource.

.CHART 1510.1 - TYPES OF RESOURCES
|===
| SOURCE/TYPE | DESCRIPTION/VALUE TO CONSIDER | TREATMENT

| Achieving Better Life Experiences Act (ABLE)

The Georgia ABLE program is named STABLE.
| The Stephen Beck, Jr., Achieving a Better Life Experience Act (ABLE) aims to ease financial strains faced by individuals with disabilities by making tax-free saving accounts available to cover qualified disability expenses.
^| E

| Agent Orange Payment
| money retained from a payment made to a Vietnam veteran who was exposed to Agent Orange and to the surviving spouse and/or children of a deceased Vietnam veteran who was exposed to Agent Orange
^| E

| Alaska Native Claims Settlement Act (ANCSA), PL 100-241
| Stock, land, an interest in land or an interest in a settlement trust received from a native corporation

Up to $2000 annually in money retained from a payment received from a native corporation is excluded.

Money in excess of $2000 that is retained from a payment received from a native corporation is a countable resource.
^| E

I

| Annuity (Supplemental Retirement Plan)
a|
an investment plan.
It can be established as a supplemental retirement plan through an insurance company or other investment source +
Count cash value.

[caption=Exception]
NOTE: Exclude if termination of employment or retirement is required for access and employment continues.
>| I

| Bond
| government-issued, interest-bearing certificate, such as U.S. savings bond, municipal bond, etc.

Count cash value.
>| I

| Burial Contract and Burial Insurance
a|
prepaid contract to cover funeral expenses or an insurance policy specifically designated for burial

NOTE: The funeral home is usually named as the beneficiary of the insurance policy. +
*Exclude up to $1500 of the combined equity value of all burial contracts and burial insurance for each AU member. +
*Include the equity value in excess of $1500.
>| *

| Burial Plot
| one burial plot per AU member

Count the equity value of each additional plot.
^| E I

| Cash
| money on hand

Count the cash value minus any money considered income in that month.
>| I

| Certificate of Deposit (CD)
| certificate representing a specific sum of money on deposit with a financial institution which accrues interest over a set period.
Count the cash value minus any penalties for early withdrawal.
Count the cash value minus any money considered income in that month.
>| I

| Checking Account
| an account on which checks may be written against amounts on deposit

Count the cash value minus any money considered income in that month.
>| I

| Coin Collection
| a collection of coins

Count the face value of the coin collection as the cash value, regardless of the collection's age.
^| I

| Commingled Funds
| an excluded resource commingled with a countable resource

*The portion of the commingled funds that can be identified as an excluded resource retains the exclusion.
Funds that cannot be identified as an excluded resource are counted in their entirety.
^| *

| Credit Union Account
| money on deposit with a cooperative organization which functions as a bank

Count cash value minus any money considered income in that month.
^| I

| Crime Victim Compensation Program
| money retained from payments received from federal or federally funded state or local programs that cover costs incurred by victims of crimes
^| E

| Deferred Compensation Plan
| a retirement plan, including a 401K and 457, available only upon retirement, onset of a hardship, or termination of employment
^| E

| Disaster Relief Act of 1974 and Emergency Assistance Act of 1988
| Money retained from government (federal, state, local) payments designated for the restoration of a home damaged in a major disaster or natural catastrophe.

Included are government payments intended to save lives, protect property and public health and safety or to lessen or avert the threat of a catastrophe or major disaster.

Also included are loans and grants received from the Federal Emergency Management Assistance (FEMA) and payments made by the Department of Housing and Urban Development, disaster loans, family grant programs and grants made by the Small Business Administration because of a disaster.
^| E

| Earned Income Tax Credit (EITC)
a|
Money retained from a tax credit that is received in one of the following ways:

* advanced payment – a tax credit received as part of the regular paycheck
* non-recurring lump sum – a tax credit received in the form of an income tax refund.
^| E E

| Energy Assistance other than LIHEAA (Low Income Home Energy Assistance Act)
| money retained from payments or allowances made under any federal, state, or local program for the purpose of energy assistance

Federal or state one-time assistance for weatherization or emergency repair or replacement of heating or cooling devices.
^| E

E

| Home Place
| the home and surrounding land occupied by the AU

Refer to Section 1520, Real Property, for additional information.
^| E

| Household/Personal Goods
| household and personal effects or other belongings such as furniture, appliances, clothing, personal items or items required because of a disability

Included are items of unusual value such as silver, jewelry, stamps, guns or other collections.
^| E

| Income Tax Refund
| money retained from monetary refunds paid to taxpayers from the state or federal government

Count the cash value of the money retained.

If any portion of the refund includes a tax credit, refer to Earned Income Tax Credit (EITC) in this section.
^| I

| Indian Tribe Payment
| money retained from payments to native Americans based on federal statutes, including distribution of judgment and settlement funds and receipts from lands held in trust

Federal statutes that provide for payments to members of various Indian tribes include Public Laws 85-794, 92-254, 93-134, 93-

531, 94-114,

94-189, 94-540, 95-433, 95-498, 95-499,

95-608,

96-318, 96-420, 97-95, 97-372, 97-376, 97-

402,

97-403, 97-408, 97-436, 97-458, 98-64, 98-

123,

98-124, 98-432, 98-500, 98-602, 99-130,

99-146,

99-264, 99-346, 99-377, 100-139, 100-383,

100-

411, 100-580, 100-581, 101-41, 101-277,

101-

153, 101-618, 103-66, 103-116 and 103-

436.
^| E

| Individual Development Account (IDA)
| A trust account, established by or on behalf of a TANF client, and opened with income, to pay for post-secondary educational expenses, for the first purchase of a home or to start a new business.

Exclude funds up to $5000, including funds withdrawn and used for the stated purpose.

Refer to Section 1530, Treatment of Income by Income Type.
^| E

| Individual Retirement Account (IRA)
| funds deposited into a retirement account

Count the cash value of the funds in the account minus any early withdrawal penalty.
^| I

| Installment

Contracts/Agreement (for sale of land or buildings)
a|
a written agreement with specific stipulations for the sale of land or buildings

NOTE: The property sold under the contract or held as security is also excluded.
^| E

| Keogh Plan (owned by individual)

Keogh Plan (owned with others)
| funds deposited into a retirement plan

Count the cash value of the funds in the retirement plan minus any early withdrawal penalty.

If the plan contains a contractual agreement with an individual whose resources will not be considered in determining eligibility, the funds are considered inaccessible to the AU.
^| I

E

| Life Insurance
| insurance policy which pays a beneficiary on the death of the individual insured

Refer to Section 1650, Budgeting Lump Sum Income, for information on treatment of the proceeds to the beneficiary.
^| E

| Life Interest
| property that an individual has a right to use but not dispose of during his/her life
^| E

| Livestock
| animals owned for any reason unless the animal is of unusual value
^| E

| Loans from Others
| money received by the AU that the AU has an obligation to repay
^| E

| Loans to Others (Notes Receivable)
| monies loaned to persons outside the AU where a repayment agreement exists

Count the cash value.
^| I

| Low Income Home Energy Assistance Act (LIHEAA)
| money retained from payments for home energy provided to, or indirectly on behalf of, an AU
^| E

| Lump Sum
| money retained from a lump sum payment that is still available following the conclusion of a lump sum period of ineligibility.

Refer to 1650, Budgeting Lump Sum Income.
^| I

| Lump Sum/SSI Back Payment
| money retained from previous SSI benefits owed and paid to an individual who is currently receiving SSI.

money retained from payment for previous SSI benefits owed and paid to an individual who is no longer receiving SSI

*Exclude as a resource in the month of receipt and the month following the month of receipt.

*Count as a resource in the second month following the month of receipt.
^| E

*

| Nazi Victim Payment
| money retained from payment to an individual who was a victim of Nazi persecution

Per Public Law 103-286, such payment is disregarded in the determination of eligibility for needs-based assistance programs.
^| E

| Non-Homeplace Property
| buildings and land which are not considered part of the home place

Refer to Section 1520, Real Property.
^| I

| Pass Account (Plan to Achieve Self- Sufficiency)
| money deposited in a bank account to be used for an SSI individual in a plan for self- sufficiency approved by the SSA
^| E

| Pension Plan
| a retirement plan provided by an employer

Exclude if termination of employment or retirement is required for access to the funds and employment continues.
^| E

| Personal Property (equipment, tools, machinery, stock and inventory)
| property that produces income

The property retains its exclusion even during temporary periods of unemployment or inactivity.

The property retains its exclusion for one year from the date the owner terminates self- employment from farming.

Count the equity value of property that does not produce income.
^| E

I

| Real Property
| Home Place Property-includes the dwelling in which the AU lives and surrounding land and outbuildings

Non-Home Place-includes all land and buildings that are not home place property

Refer to Home Place and Non-Home Property in this chart and Section 1520,

Real Property.
^| E

I

| Rental Property
| real property rented to others that annually produces income consistent with fair market value

real property that does not annually produce income consistent with fair market value

Count the equity value.

Refer to Section 1520, Real Property.
^| E

I

| Resources of an SSI Recipient
a|
All resources of any source or type retained by an individual who:

* has been approved to receive benefits
* receives benefits
* is approved for/or receives benefits but the benefits are suspended, are being recouped because of an overpayment or are not being paid because the amount is less than the minimum issuance amount.
^| E

| Restitution for World War II Internment of Japanese Americans and Aleuts;
PL 100- 383
| Money retained from payments made to

U.S. citizens of Japanese ancestry or to lawful permanent resident Japanese aliens or their survivors, and to Aleutian Island residents, because of their evacuation, relocation, and internment during World War II
^| E

| Safe Deposit Box
| secure storage in a bank or other institution where money and other valuables may be deposited

Obtain from the AU a list of items that are in the box.

Count the cash value of all items unless an item is otherwise excluded.
^| I

| Savings Account
| monies held in a financial institution in an interest-bearing account

Refer to Section 1530, Treatment of Income by Income Type, for treatment of interest.
^| I

| Security Deposit on Rental Property or Utilities
| monies held by the provider and not accessible to the AU

monies returned to AU
^| E

I

| Spending Account
| funds which are held in an account to pay certain expenses such as childcare or medical expenses

Refer to Section 1530, Treatment of Income-by-Income Type, for treatment of reimbursements from spending accounts.
^| E

| Stimulus Payment

Coronavirus Aid, Relief and Economic Security (CARES) Act
| UNEARNED- The CARES Act provides fast and direct economic assistance for American workers and families, small businesses, and preserves jobs for American industries.
^| E

| Stocks
| a certificate representing ownership of shares in a company

Count cash value.
^| I

| TANF Corrective
| money retained from TANF corrective benefits received by an AU in a previous month
^| I

| Trusts
a|
funds in a trust or transferred to a trust, and the income produced by that trust +
Consider the value as follows:

* trust arrangement can be revoked by an AU member
* trustee administering the fund is a court, an institution, corporation or organization which is not under the direction or ownership of any AU member
* trustee appointed by the court has court-imposed limitations placed on the funds
* trust investments made on behalf of the trust do not directly involve or assist any business or corporation under the control, direction or influence of an AU member.

Refer to Section 1530, Treatment of Income by Income Type, for treatment of income from a trust.
^| I E

E

E

| Uniform Relocation Assistance and Real Property Acquisition
| reimbursements retained under Public Law 91- 646, Section 210
^| E

| Vehicles
| car, truck, motorcycle, etc.

*Refer to Section 1515, Vehicles.
^| *

| Women Infants and Children (WIC) special supplemental food program
| vouchers that are redeemable for food items by women and children considered to be nutritionally at high risk
^| E
|===
