= 1005 Domestic Violence
:chapter-number: 1000
:effective-date: August 2022
:mt: MT-68
:policy-number: 1005
:policy-title: Domestic Violence
:previous-policy-number: MT 49

include::ROOT:partial$policy-header.adoc[]

== Requirements

Because the U.S. Congress has determined that a reduction of domestic violence is in the national interest, standards and procedures must be established to identify domestic violence and reduce or eliminate its effects.

== Basic Considerations

Prior to the enactment of the Personal Responsibility and Work Opportunity Act (PRWORA) into law in 1996, the number of individuals receiving Aid to Families with Dependent Children (AFDC) had more than tripled since 1965.
Studies in many states throughout the nation revealed that current or recent domestic violence was prevalent among poor women in general and particularly among those receiving AFDC.

Studies consistently showed that well over half of the women receiving AFDC reported they had experienced physical abuse by an adult male partner at some point during their adult lives.
Some studies found that the rates were higher than those experienced by other low-income women from the same neighborhood who did not receive AFDC.
Most women receiving AFDC also reported physical and/or sexual abuse in childhood.

Studies revealed that women who had experienced physical violence by a partner were more likely to have remained on welfare for a combined total of five years or more.
Domestic violence has also been shown to be a significant factor in the cycling on and off AFDC.

Studies also indicated that many AFDC recipients reported physical health problems, with victims of domestic violence being more likely to report a current physical disability, handicap, or other serious physical, mental, or emotional problem.
Some findings showed that AFDC recipients who had experienced domestic violence in the previous twelve months were more likely to have a disability that limited their ability to work.

Women victimized by domestic violence have been more likely to be unemployed, to be in lower paying jobs when they were employed, to have lower personal income, and to have received AFDC, Food Stamps, and Medicaid.

Therefore, it was the sense of the Congress that reducing domestic violence was in the Government's interests, and that policies needed to be developed that addressed what was a significant contributing factor to the increase in the number of people receiving AFDC.

In conjunction with the PRWORA, states are required to operate a family assistance program, a child support enforcement program, a foster care and adoption assistance program and to establish standards and procedures to ensure that domestic violence will be identified.

=== Family Violence Option

The PRWORA encourages states to implement the Family Violence Option (FVO), a TANF State plan provision that provides a specific method for addressing the needs of domestic violence victims receiving welfare.
The FVO is intended to provide for the identification and screening of domestic violence victims, referral to services, and waiver of program requirements for good cause.

Georgia adopted the FVO, certifying that the State has established and is enforcing the following standards and procedures:

* to screen and identify individuals who receive TANF assistance and who have a history of domestic violence while maintaining the confidentiality of such individuals,
* to refer such individuals to counseling and supportive services, and
* to waive, pursuant to a determination of good cause, certain program requirements in cases where compliance with such requirements would make it more difficult for individuals receiving TANF assistance to escape domestic violence.

Program requirements can be waived if they would unfairly penalize individuals who are or have been victimized by domestic violence, or individuals who are at risk of further domestic violence.

Program requirements that may be waived include time limits, residency requirements, child support cooperation requirements, and the family cap provision.

=== Work Requirements and the Family Violence Option

Victims of domestic violence are not exempted from work requirements under the FVO.
It is believed that victims of domestic violence will be served most effectively if work requirements are maintained while appropriate services are provided.

=== Domestic Violence Waivers

Service providers who work closely with victims of domestic violence maintain that work is often a key factor in helping victims escape their violent circumstances.
It is recognized, however, that working or taking steps toward independence may aggravate tensions with a batterer and place the victim in further danger.
For that reason, temporary waivers of work requirements can be provided.

The PRWORA allows states to continue TANF after the lifetime limit is reached for families that suffer from domestic violence.

A state can claim reasonable cause if it fails to comply with the limit on the number of hardship extensions it can grant when its failure is attributable to its provision of federally recognized good cause domestic violence waivers.

A good cause domestic violence waiver refers to any waiver consistent with the FVO that is granted by Georgia.
Georgia has great flexibility in the granting of waivers, deciding which program requirements are to be waived, and for how long.
To be considered for this purpose, a good cause domestic violence waiver must:

* identify the specific program requirements that are waived;
* waive program requirements that are temporary in nature (not to exceed six months) and that are based on the recipient's need as determined by an individualized assessment by a person trained in domestic violence no less often than every six months;
and
* be accompanied by a services plan developed by a person trained in domestic violence, and that is reflective of an individualized assessment and which eventually leads to the TANF recipient's employment.

In order to administer these provisions and have effective and accountable programs, Georgia must maintain records that identify victims and recipients of good cause domestic violence waivers.
Because it is vital to keep this information, confidentiality standards must be enough to protect victims.

=== Consolidated Appropriations Act, 2022

On March 15, 2022, the Consolidated Appropriations Act, 2022 amended section 402(a) of the Social Security Act (42 U.S.C.602(a)), adding a new required certification for state TANF agencies related to providing information to victims of sexual harassment or survivors of domestic violence, sexual assault, or stalking.

States must establish and enforce standards and procedures to:

* ensure that applicants and potential applicants for assistance under the State program are notified of assistance made available by the State to victims of sexual harassment and survivors of domestic violence, sexual assault, or stalking
* ensure that case workers and other agency personnel responsible for administering the State program are trained in:
 ** the nature and dynamics of sexual harassment and domestic violence, sexual assault, and stalking;
 ** State standards and procedures relating to the prevention of, and assistance for, individuals who are victims of sexual harassment or survivors of domestic violence, sexual assault, or stalking;
and
 ** methods of ascertaining and ensuring the confidentiality of personal information and documentation related to applicants for assistance and their children who have provided notice about their experiences of sexual harassment, domestic violence, sexual assault, or stalking
* ensure that, if a State has elected to establish and enforce standards and procedures regarding the screening for, and identification of, sexual harassment, domestic violence, sexual assault, or stalking under the Family Violence Option (FVO)
 ** the State program provides information about the options to current and potential beneficiaries;
and
 ** case workers and other agency personnel responsible for administering the State program are provided with training regarding State standards and procedures.
