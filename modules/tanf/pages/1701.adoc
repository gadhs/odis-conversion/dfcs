= 1701 Case Management Overview
:chapter-number: 1700
:effective-date: June 2019
:mt: MT-48
:policy-number: 1701
:policy-title: Case Management Overview
:previous-policy-number: MT 6

include::ROOT:partial$policy-header.adoc[]

== Requirements

Case management is the activity that begins with application and continues as long as an assistance unit (AU) remains eligible for TANF.

== Basic Considerations

* Every AU is required to cooperate with periodic reviews of eligibility.

* Every AU is required to report all changes that may affect its eligibility and/or their benefit level.

* All reported changes must be processed in a timely manner.

* Every AU must receive proper notification of action taken on its case.

* Every AU has the right to request a hearing on any action taken on its case.

* In some cases, changes reported by the AU may result in the AU's receipt of incorrect benefits in prior months.
It may be necessary to recover these overpayments.
