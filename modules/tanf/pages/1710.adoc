= 1710 Reviews
:chapter-number: 1700
:effective-date: November 2023
:mt: MT-75
:policy-number: 1710
:policy-title: Reviews
:previous-policy-number: MT 72

include::ROOT:partial$policy-header.adoc[]

== Requirements

Renewals of continuing eligibility are required for all TANF cases every twelve months.

== Basic Considerations

=== Review Requirements

AUs must be notified that a renewal needs to be completed, allowing them sufficient time to comply with the renewal requirements.
The renewal process must be completed by the last day of the month in which the renewal is due. +
After completing a Renewal, the TANF case is renewed for a period of 12-months.

NOTE: If the last day of the month falls on the weekend or holiday, the renewal must be submitted by the last business day of the month.

Renewals may also be completed at other times if the case manager deems it necessary because of a change in the AUs circumstances.
To complete the renewal process, the AU must submit a completed renewal form and be interviewed.

NOTE: 90-day reviews are recommended for Individuals that are caring for a disabled household member.
This is not a standard TANF eligibility renewal.

The AU can complete the renewal form by:

* submitting a completed Form 508 – Food Stamps, Medicaid, TANF Renewal Form to the local county office or
* using the on-line Renewal Option via Georgia Gateway at: https://www.gateway.ga.gov./[www.gateway.ga.gov.]

The AU must submit the renewal form prior to the scheduled interview appointment time.

=== Electronic Signatures

Electronic signatures utilizing Adobe signature or other E-Signature software will be acceptable for all TANF forms.
A completed application consists of a signed application submitted with a name and address.
A typed name on the signature line of a paper application is not acceptable.

=== Who Must be Interviewed

A telephone or face-to-face interview must be conducted with the following individuals:

* the parent(s) or grantee relative,
* a pregnant woman (including a minor dependent child who is pregnant)
* a minor parent who is receiving cash assistance as a dependent child, including a non- custodial, non-supporting minor parent;
or
* an AU member who has a work requirement

If relevant to the AUs circumstances, a telephone or a face-to-face interview may also need to be conducted with one or more of the following individuals to obtain income/resource information:

* alien's sponsor
* ineligible parent who is not a payee
* employed minor child in the AU if the caretaker is uncertain of the child's income
* spouse of a married minor
* stepparent
* spouse of a caretaker
* parent of a minor caretaker

=== What Must be Reviewed

The following points of eligibility must be reviewed, and verification obtained, if applicable:

* deprivation
* living arrangements, including those of a pregnant minor or minor parent
* prenatal care
* personal responsibility requirements
* work requirements
* immunizations
* resources
* income
* dependent care expenses
* financial management
* any other point of eligibility subject to change
* GRG /MSP eligibility.

=== Voter Registration

It is the policy of the Department of Human Services (DHS) that all Office of Family Independence (OFI) programs must offer voter registration services to all applicants and recipients whenever they apply for services, renew services, or submit a change of address, whether in person, electronically, via the telephone, facsimile, or mail. +
Voter registration services include distribution of:

* Application for Voter Registration
* Assistance in completing the form if requested
* Acceptance and transmittal of completed Voter Registration Application and the Agency Recap Forms to the Secretary of State.

Include the Agency Voter Registration Declaration Statement and Application for Voter Registration with each renewal package. +
Document recipient's acceptance or declination of the voter registration service.

_For additional information about voter registration, refer to section 1008 of the policy manual._

=== Appointment Notice

A system-generated appointment notice or a manual Form 173A- Appointment Letter is used to schedule a face-to-face or telephone interview.
The letter must contain the following information:

* that a review is necessary to continue eligibility,
* the consequences of failing to comply with the renewal process,
* the date on which the AU must be available for a telephone interview or appear for a face-to-face interview with the case manager,
* the AUs responsibility for rescheduling a missed interview,
* the AUs responsibility to provide all required verification,
* the AUs right to a fair hearing,
* the address of the county office completing the renewal, and
* the name and direct telephone number of the case manager.

=== Missed Appointment

If the AU misses the telephone or in-office appointment and does not contact the agency within the 24-hour grace period, the case is closed in IES issuing a timely notice.
The TANF case is automatically closed following the expiration of timely notice.

If the AU contacts the agency within 14-days waiting period, the action must be stopped and the AUs circumstances reevaluated.
Any OI/OP resulting from this missed appointment will be considered a valid overpayment.

_Refer to section 1705-Notifications, for additional information regarding Timely Notice and possible overpayment._

== Procedures

Follow the steps below to complete a renewal:

. Mail the AU an appointment notice to schedule the interview.
The appointment notices/letter should be mailed to the AU to ensure receipt by the AU within seven calendar days prior to the appointment date.
+
[caption=Exception]
NOTE: An appointment scheduled by means other than in writing can be made fewer than seven days prior to the appointment date.
Ensure that sufficient time is allowed for processing the review by the due date.

[start=2]
. Conduct a telephone or face-to-face interview with the appropriate individuals.
. Offer voter registration services and record the outcome in IES and on the Declaration Statement.
. Review all points of eligibility using the completed Form 508, Food Stamps, Medicaid, TANF Renewal Form or Customer Portal Renewal Application.
Obtain verification when required.
. Document the information obtained during the interview process in IES.

=== Mandatory Forms

The following forms must be completed and/or provided to the AU when completing a TANF renewal *and document in IES:*

* *Form 508*, Food Stamps, Medicaid, and TANF Renewal form (if renewal is not submitted through customer portal).
+
NOTE: *Form 297*, Application for TANF, Food Stamps, and Medical Assistance can be accepted for a TANF renewal. +
However, *Form 528*, SNAP Periodic Report Form, cannot be accepted for a TANF renewal.

* *Form 522*, Domestic Violence brochure.
If the renewal is submitted through Customer portal or Form 508, the section for Domestic Violence will satisfy the Form 522 requirement.

=== Conditional Forms

The following forms are completed *as necessary* when completing a TANF renewal:

* *Form 138*, Notice of Requirement to Cooperate and Right to Claim Good Cause for Refusal to Cooperate in Child Support Enforcement and Third-Party Resources, if deprivation is based on continued absence and there has been a change since the most recent application or renewal.
(_Form is required if additional child is added)._
* *Form 173*, TANF/ Medicaid/ Food Stamp Verification Checklist
* *Form 173 A*, Appointment Letter
* *Form 194,* TANF Assessment for Domestic Violence, Sexual Assault, Sexual Harassment, or Stalking (_This form is required to be signed and returned if a waiver is requested for DCSS Cooperation or Work Requirements)._
* *Form 196,* TANF Family Service Plan (TFSP)-Personal Responsibility Requirements (PRP) if there has been a change in the AUs PRP requirements since application or last renewal.
_(Form must be signed and returned)._
* *Form 196A,* TANF Family Service Plan (TFSP)- Work Requirement, if there has been a change in the AUs TFSP work requirement since application or last renewal.
_(Form must be signed and returned)._
* *Form 351*, GRG MSP CRISP Work Sheet
* *Form 354*, Expense Statement, (_Form 354 is only for work eligible adults.
It should be completed if Form 508 is not completed in its entirety)._
* *Form 489*, TANF Work Requirement Exemption Form, if there is a child in the home less than 12 months of age.
_(Form must be signed and returned if the client is requesting an exemption_).
* *Form 785*, Family Planning Services Referral, if required _Refer to 1345-4 – Personal Responsibilities Requirements for details about requirements to complete Form 785._
* *ADA RM Form 101,* Americans with Disability Act Reasonable Modification Form
* _(This form may be used, but is not required, if reasonable modifications or accommodations are requested)._
* *ADA RM Form 102 Tracking Form,* Americans with Disability Act Reasonable Modification Tracking Form (_This form is required when the Reasonable Modification (RM) request cannot be entered and documented in Gateway, or the request for a RM may be potentially denied)._

NOTE: If Form 194, 196 – PRP, 196A – Work Plan, and/or 489 are required, they must be signed and returned.
Case Notes must be updated to indicate a required form was mailed if Form 508 is submitted as the renewal.

Whenever IES can be used to collect or transmit necessary information, it should be used.
If IES cannot be used, even if only temporarily, the hard copy form must be used.

=== Other Forms

Based on the client's circumstances, other forms may also be completed.

Other forms are completed to verify Points of Eligibility.
For example, income verification forms, exemption from work requirement forms etc.

. Complete Clearinghouse requirements:
 ** View related records
 ** Document IES: “Additional information verified by a third party or other sources.”

*Do not cut and paste Federal Tax Information (FTI) in WebCenter Enterprise Capture (WEC) or IES screens.*

NOTE: Refer to section 1410, IRS, and BEERS Information Security, of the policy manual to learn more about the security of FTI information.

Update information on the system to reflect changes and to indicate that the review is complete.

Use the following chart as a guide to timely completion of a TANF renewal:

.Chart 1710.1 - PROCEDURES FOR COMPLETION OF TANF RENEWAL
|===
| IF | THEN

| the AU complies with all requirements
| Continue eligibility.

| the AU misses the telephone or in- office appointment and does not contact the agency within the 24-hour grace period
| A timely notice is sent, after the 24-hour grace period and then close the case following expiration of the timely notice

| the AU misses the telephone or in- office appointment and contacts the agency within the 24-hour grace
| Appointment rescheduled;
client advised to submit renewal form *508 prior to the appointment.*

| the AU misses the telephone or in- office appointment and contacts the agency within the timely notice period
a|
Appointment rescheduled;
client advised to submit renewal form *508 prior to the appointment*.

NOTE: If this appointment is missed, the original timely notice period continues.

| the AU misses the telephone or in- office appointment and later contacts the agency for an interview and submits a renewal form before the end of the POE
| Reopen the TANF case and complete the interview.

If verification is requested and returned, complete TANF renewal.

If verification is not returned, close TANF case.

| the agency did not provide written notice of the appointment seven days prior to the appointment date and the appointment has been missed
a|
Reschedule the appointment.

NOTE: Timely notice does not begin until the second appointment is missed.

| the AU fails to provide requested verification that is required to re- determine eligibility, or return *Form 508*
| close the case following expiration of timely notice.

| Client fails to attend renewal appointment and does not submit a renewal form
| Close case

| the case is transferred from another county
| complete a review within 30 days of accepting transfer.
|===
