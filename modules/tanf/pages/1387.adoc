= 1387 Lawbreakers
:chapter-number: 1300
:effective-date: December 2019
:mt: MT-51
:policy-number: 1387
:policy-title: Lawbreakers
:previous-policy-number: MT 1

include::ROOT:partial$policy-header.adoc[]

== Requirements

Individuals who are convicted of certain crimes, who are probation or parole violators, or who are fleeing to avoid prosecution, custody or confinement for a felony are penalized and cannot be included in the assistance unit (AU).

== Basic Considerations

=== Violent Felons

Individuals convicted on or after January 1, 1997, of a serious violent felony are penalized for life and cannot be included in the AU.
A serious violent felony includes the following:

* murder
* felony murder
* armed robbery
* kidnapping
* rape
* aggravated child molestation
* aggravated sodomy
* aggravated sexual battery This list is all-inclusive.

=== Controlled Substance Abuse Felons

Individuals convicted on or after January 1, 1997, of an offense classified as a felony related to the possession, use or distribution of a controlled substance are penalized for life and cannot be included in the AU.

=== Fleeing Felons

Individuals who are fleeing to avoid custody or confinement for a felony conviction or fleeing to avoid prosecution for a felony are penalized and cannot be included in the AU.
The individuals are penalized until they are no longer fleeing.

=== Parole/Probation Violators

Individuals who are violating a condition of their parole or probation are penalized and cannot be included in the AU.
The individuals are penalized until they are no longer violating a condition of their parole or probation.

An individual who meets the definition of a lawbreaker but who is not a member of the standard filing unit (SFU) is not subject to a penalty but is excluded from the AU for the same period of time for which a penalty would be imposed.

Refer to Section 1205, Assistance Units, for additional information.
The following chart provides the penalties applied to lawbreakers:

.CHART 1387.1 - LAWBREAKERS
|===
| LAWBREAKERS | PENALTY

| Individuals convicted on or after January 1, 1997, of a serious violent felony
| penalized for life.

| Individuals convicted on or after January 1, 1997, of a felony related to possession, use or distribution of a controlled substance
| penalized for life.

| Individuals fleeing to avoid prosecution, custody or confinement for a felony
| penalized until no longer fleeing to avoid prosecution, custody or confinement.

| Individuals violating a condition of their probation or parole
| penalized until no longer a probation/parole violator.
|===

=== First Offender Status

An individual who meets the definition of a lawbreaker but who has been granted first offender status by the court is not subject to a penalty as a lawbreaker.

=== Minors

A minor adjudicated as a delinquent through the juvenile court system has not been convicted of a crime.
The minor is not subject to a penalty as a lawbreaker.

A minor who is prosecuted and convicted as an adult is subject to a penalty as a lawbreaker.

=== Disclosure/Exchange of Information

The address of an individual who meets the definition of a lawbreaker may be provided to federal, state or local law enforcement officials.

Refer to Section 1002, Confidentiality, for policy on providing information to law enforcement officials.

== Verification

The AU's verbal or written statement of whether any member of the AU meets the definition of a lawbreaker is acceptable, unless questionable.
The statement is obtained at application, review, and when adding an individual to an existing AU.

If questionable, the written statement from the appropriate law enforcement agency or court is acceptable.

== Procedures

Discuss the policy regarding lawbreakers with the AU at application and review.

Document the AUs statement of whether any AU member meets the definition of lawbreaker. +
Accept the AUs verbal or written statement, unless questionable.
If questionable, verify with the appropriate source.

Document the following information: AUs statement or date and source of verification, if +
questionable
