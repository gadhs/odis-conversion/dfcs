= 1905 Issuance
:chapter-number: 1900
:effective-date: November 2023
:mt: MT-75
:policy-number: 1905
:policy-title: Issuance
:previous-policy-number: MT 69

include::ROOT:partial$policy-header.adoc[]

== Requirements

Cash assistance is issued in the form of an electronic payment using a Way2Go Debit Mastercard.

== Basic Considerations

=== Types of Issuance

Cash assistance is issued in the following categories:

* initial payments
* ongoing payments
* corrective payments
* supplemental payments
* manually issued payments.

Cash assistance is issued in the name of the caretaker relative or payee unless the AU has a managed account.
Refer to Section 1915, Managed Accounts.

Banking regulations do not release TANF benefits to a child or minor parent head of household under 13 years old.
A parent or parent caretaker over 13 years of age must be named as the head of household for the benefits to be released.

=== Out-of-State Mailing

A Way2Go card may be mailed to an AU that has moved to another state if one of the following exists:

* the AU lives in Georgia but has a mailing address in another state,

[.text-center]
OR

* the AU moves out of the state, requests that the case be closed, but is entitled to an additional month of benefits.

=== Initial Issuance

Following approval of an application for cash assistance, the Integrated Eligibility System (IES) will do the following:

* issue all benefits due the AU through the month of approval, including any prorated benefits, and
* issue benefits in the nightly processing cycle in which the case is brought to final and transfer this information to the Electronic Benefit Transfer Application System (EBTAS).

Initial benefits are posted to the AU's account after this information is then sent from EBTAS in an ACH file to the State's bank, Truist.
Payments prepared and transmitted each day contain the Comerica Banking (Contractor's bank) routing and cardholder account information established in the State's EBT Service Provider's administrative terminal.
The bank receives Automated Clearinghouse (ACH) files through the Electronic Payment Network (EPN) from Truist.
Comerica Bank processes the ACH files in all six National Automated Clearing House Association (NACHA) processing windows and transmits the ACH file containing the cardholder account information to the State's EBT Service Provider for posting to the individual accounts.

=== Ongoing Issuance

Ongoing benefits are available in the EBT account on the first calendar day of every month.
If the first falls on a weekend or holiday, benefits are made available on the last business day of the prior month.

NOTE: If the TANF benefit amount prorates to $0.00 for the application month and ongoing months, the TANF case will deny or close.

=== Ongoing Issuance and the Window Period

The window period refers to the period that begins on the date on which IES authorizes benefits to be issued to an active case for the ongoing month and extends through the last calendar day of the current month.

The dates for each month's window period are established by the IES processing schedule.
A change to affect the benefit amount for the ongoing month must be processed on IES prior to the first day of the window period.

Benefits are issued for the ongoing month in the amount determined by the last case action that processed on IES prior to the beginning of the window period.

=== Supplemental Issuance

Supplemental benefits are issued when the following occur:

* the AU is eligible for an increase in its benefit level for the ongoing month, AND
* the change is made during the window period.

A supplemental issuance is posted to the AU's account following IES processing and after this information is then sent from EBTAS in an ACH file to the State's bank, Truist.
Payments prepared and transmitted each day contain the Comerica Banking (Contractor's bank) routing and cardholder account information established in the Way2GO administrative terminal.
The bank receives Automated Clearinghouse (ACH) files through the Electronic Payment Network (EPN) from Truist.
Comerica Bank processes the ACH files in all six National Automated Clearing House Association (NACHA) processing windows and transmits the ACH file containing the cardholder account information to the State's EBT Service Provider for posting to the individual accounts.

=== Corrective Issuance

Corrective benefits are issued when the following occur:

* an application is denied in error
* a case is closed in error
* an issuance is canceled in error
* an underpayment occurs because of untimely action by the county
* a change in an AU's circumstances that will result in an increase in benefits for the ongoing month cannot be made prior to the window period because of system limitations
* a recoupment error occurs
* an Administrative Law Judge directs the agency to issue a corrective
+
NOTE: A corrective benefit can be paid only to an AU with an active case.
If a corrective is authorized for an AU with a closed case, the corrective will pend until the case becomes active and the status of the corrective is manually changed.

Correct payments can be issued in the following ways:

* completion on IES of an historical change that causes an increase in the benefit level
* completion of an underpayment claim
* submission of a request for a manual issuance to the state office.

System-generated corrective payments are typically offset if an AU has an outstanding claim.
